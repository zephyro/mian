<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->


            <div class="first first-new">
                <div class="first__wrap">
                    <div class="container">
                        <h1 class="first__heading">Продажа новостроек в Москве</h1>

                        <div class="first__content">
                            <form class="form">

                                <ul class="first__filter">

                                    <li>
                                        <div class="mySelect first__select">
                                            <div class="mySelect__btn">
                                                <span class="mySelect__selected">Продажа</span>
                                            </div>
                                            <div class="mySelect__dropdown">
                                                <div class="mySelect__wrap">
                                                    <ul>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__deal" value="Продажа" checked>
                                                                <span>Продажа</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__deal" value="Аренда">
                                                                <span>Аренда</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__deal" value="Посуточно">
                                                                <span>Посуточно</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="mySelect first__select">
                                            <div class="mySelect__btn">
                                                <span class="mySelect__selected">Квартира</span>
                                            </div>
                                            <div class="mySelect__dropdown">
                                                <div class="mySelect__wrap">
                                                    <ul>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Квартира" checked>
                                                                <span>Квартира</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Комната">
                                                                <span>Комната</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Дом">
                                                                <span>Дом</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Коттедж">
                                                                <span>Коттедж</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Участок">
                                                                <span>Участок</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Офис">
                                                                <span>Офис</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Торговая площадь">
                                                                <span>Торговая площадь</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Склад">
                                                                <span>Склад</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="mySelect first__select">
                                            <div class="mySelect__btn" data-empty="Число комнат">
                                                <span class="mySelect__selected">Число комнат</span>
                                            </div>
                                            <div class="mySelect__dropdown">
                                                <div class="mySelect__wrap">
                                                    <ul>
                                                        <li>
                                                            <label class="mySelect__checkbox">
                                                                <input type="checkbox" name="filter__type" value="Студия">
                                                                <span>Студия</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__checkbox">
                                                                <input type="checkbox" name="filter__type" value="1к">
                                                                <span>1-комнатная</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__checkbox">
                                                                <input type="checkbox" name="filter__type" value="2к">
                                                                <span>2-комнатная</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__checkbox">
                                                                <input type="checkbox" name="filter__type" value="3к">
                                                                <span>3-комнатная</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__checkbox">
                                                                <input type="checkbox" name="filter__type" value="4к">
                                                                <span>4-комнатная</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="formRange">
                                            <div class="formRange__start">
                                                <input type="text" name="priceStart" placeholder="от">
                                            </div>
                                            <div class="formRange__end">
                                                <input type="text" name="priceEnd" placeholder="до">
                                                <span class="formRange__legend">₽</span>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="mySelect first__select">
                                            <div class="mySelect__btn">
                                                <span class="mySelect__selected">Город, район, метро, адрес</span>
                                            </div>
                                            <div class="mySelect__dropdown">
                                                <div class="mySelect__wrap">
                                                    <ul>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__place" value="Город, район, метро, адрес">
                                                                <span>Показать всё</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__place" value="Москва, м. Беляево">
                                                                <span>Москва, м. Беляево</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__place" value="Москва, Универстетский проспект">
                                                                <span>Москва, Универстетский проспект</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__place" value="Москва, Куркино">
                                                                <span>Москва, Куркино</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__place" value="Москва, Митино">
                                                                <span>Москва, Митино</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__place" value="Москва, м. Новослободская">
                                                                <span>Москва, м. Новослободская</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__place" value="Москва, м. Теплый Стан">
                                                                <span>Москва, м. Теплый Стан</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__place" value="Москва, ЮЗАО, Коньково">
                                                                <span>Москва, ЮЗАО, Коньково</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__place" value="Москва, ЮЗАО, м. Прсопект Вернадского">
                                                                <span>Москва, ЮЗАО, м. Прсопект Вернадского</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                    </li>

                                </ul>

                                <ul class="first__send">
                                    <li>
                                        <button type="submit" class="btn btn-orange">Найти</button>
                                    </li>
                                    <li>
                                        <button type="submit" class="btn btn-opacity"><i class="fas fa-map-marker-alt"></i> Показать на карте</button>
                                    </li>
                                </ul>

                            </form>
                        </div>

                    </div>

                </div>
            </div>

            <section class="main">

                <div class="container">

                    <div class="promo-container rows">

                        <div class="col-left">
                            <div class="promo">

                                <div class="promo__tile tile-new-01">
                                    <div class="promo__item item-new">
                                        <div class="promo__item_content">
                                            <div class="promo__item_wrap">
                                                <h4><span>Купить квартиру в новостройке</span></h4>
                                                <ul>
                                                    <li><a href="#">1-комнатные</a> <span>5 850</span></li>
                                                    <li><a href="#">2-комнатные</a> <span>9 098</span></li>
                                                    <li><a href="#">3-комнатные</a> <span>7 836</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="promo__tile tile-new-02">
                                    <div class="promo__item item-developer">
                                        <a href="#" class="promo__item_link">
                                            <div class="promo__item_wrap">
                                                <h4><span>Каталог застройщиков</span></h4>
                                                <p>7 202 предложения</p>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                                <div class="promo__tile tile-new-03">
                                    <div class="promo__item item-build">
                                        <a href="#" class="promo__item_link">
                                            <div class="promo__item_wrap">
                                                <h4><span>Строящиеся ЖК</span></h4>
                                                <p>3 726 предложений</p>
                                            </div>
                                        </a>
                                    </div>
                                </div>


                                <div class="promo__tile tile-new-04">
                                    <div class="promo__item item-handed">
                                        <a href="#" class="promo__item_link">
                                            <div class="promo__item_wrap">
                                                <h4><span>Сданные ЖК</span></h4>
                                                <p>3 726 предложений</p>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                                <div class="promo__tile tile-new-05">
                                    <div class="promo__item item-newRoom">
                                        <a href="#" class="promo__item_link">
                                            <div class="promo__item_wrap">
                                                <h4><span>Квартиры от</span><br><span>застройщиков</span></h4>
                                                <p>97 предложений</p>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                                <div class="promo__tile tile-new-06">
                                    <div class="promo__item item-leader">
                                        <a href="#" class="promo__item_link">
                                            <div class="promo__item_wrap">
                                                <h4><span>Лидеры продаж</span></h4>
                                                <p>За прошлый квартал</p>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                                <div class="promo__tile tile-new-07">
                                    <div class="promo__item item-map">
                                        <a href="#" class="promo__item_link">
                                            <div class="promo__item_wrap">
                                                <h4><span>Поиск на карте</span></h4>
                                                <p>Ищите новостройки рядом с работой, парком или родственниками</p>
                                            </div>
                                        </a>
                                    </div>
                                </div>


                            </div>
                        </div>

                        <div class="col-right">
                            <div class="requests">

                                <div class="requests__group box-hide">
                                    <h3>По году сдачи</h3>
                                    <ul class="requests__links">

                                        <li><a href="#"><span class="item">новостройки 2018 год</span> <span class="value">1 992</span></a></li>
                                        <li><a href="#"><span class="item">новостройки 2019 год</span> <span class="value">1 935</span></a></li>
                                        <li><a href="#"><span class="item">новостройки 2020 год</span> <span class="value">1 866</span></a></li>
                                        <li><a href="#"><span class="item">новостройки 2021</span> <span class="value">1 763</span></a></li>
                                    </ul>
                                </div>

                                <div class="requests__group box-hide">
                                    <h3>По цене</h3>
                                    <ul class="requests__links">
                                        <li><a href="#"></a></li>

                                        <li><a href="#"><span class="item">до 1 млн рублей</span> <span class="value">7 770</span></a></li>
                                        <li><a href="#"><span class="item">до 2 млн рублей</span> <span class="value">7 010</span></a></li>
                                        <li><a href="#"><span class="item">до 3 млн рублей</span> <span class="value">3 787</span></a></li>
                                        <li><a href="#"><span class="item">до 4 млн рублей</span> <span class="value">1 958</span></a></li>
                                        <li><a href="#"><span class="item">новостройки до 5 млн рублей</span> <span class="value">691</span></a></li>
                                    </ul>
                                </div>

                                <div class="requests__group box-hide">
                                    <h3>По типу</h3>
                                    <ul class="requests__links">

                                        <li><a href="#"><span class="item">в панельном доме</span> <span class="value">175</span></a></li>
                                        <li><a href="#"><span class="item">в монолитном доме</span> <span class="value">42</span></a></li>
                                        <li><a href="#"><span class="item">в кирпичном доме</span> <span class="value">24</span></a></li>
                                        <li><a href="#"><span class="item">эконом новостройки в Новой Москве</span> <span class="value">22</span></a></li>
                                        <li><a href="#"><span class="item">новостройки комфорт в Москве</span> <span class="value">16</span></a></li>
                                        <li><a href="#"><span class="item">новостройки бизнес</span> <span class="value">15</span></a></li>
                                        <li><a href="#"><span class="item">новостройки премиум</span> <span class="value">14</span></a></li>
                                        <li><a href="#"><span class="item">апартаменты</span> <span class="value">13</span></a></li>
                                    </ul>
                                    <a class="requests__views btn-view"><span>ещё</span> <i class="fas fa-angle-down"></i></a>
                                </div>

                                <div class="requests__group box-hide">
                                    <h3>Квартиры от застройщика</h3>
                                    <ul class="requests__links">

                                        <li><a href="#"><span class="item">от застройщика ПИК</span> <span class="value">175</span></a></li>
                                        <li><a href="#"><span class="item">от застройщика Лидер-Инвест</span> <span class="value">42</span></a></li>
                                        <li><a href="#"><span class="item">от застройщика ДОНСТРОЙ</span> <span class="value">24</span></a></li>
                                        <li><a href="#"><span class="item">от застройщика MR Group</span> <span class="value">22</span></a></li>
                                        <li><a href="#"><span class="item">от застройщика Управление Гражданского Строительства</span> <span class="value">16</span></a></li>
                                        <li><a href="#"><span class="item">от застройщика Capital Group</span> <span class="value">15</span></a></li>
                                        <li><a href="#"><span class="item">от застройщика Галс-Девелопмент</span> <span class="value">14</span></a></li>
                                        <li><a href="#"><span class="item">от застройщика Концерн КРОСТ</span> <span class="value">13</span></a></li>
                                        <li><a href="#"><span class="item">от застройщика Группа ПСН</span> <span class="value">13</span></a></li>
                                        <li><a href="#"><span class="item">от застройщика ИНГРАД</span> <span class="value">13</span></a></li>
                                        <li><a href="#"><span class="item">от застройщика Флэт и Ко</span> <span class="value">13</span></a></li>
                                        <li><a href="#"><span class="item">от застройщика ГК МИЦ</span> <span class="value">13</span></a></li>
                                        <li><a href="#"><span class="item">от застройщика ГК А101</span> <span class="value">13</span></a></li>
                                        <li><a href="#"><span class="item">от застройщика VESPER</span> <span class="value">13</span></a></li>
                                        <li><a href="#"><span class="item">от застройщика KR Properties</span> <span class="value">13</span></a></li>
                                        <li><a href="#"><span class="item">от застройщика Банк Российский Капитал</span> <span class="value">13</span></a></li>
                                        <li><a href="#"><span class="item">от застройщика Бэсткон</span> <span class="value">13</span></a></li>
                                        <li><a href="#"><span class="item">от застройщика ИНТЕКО</span> <span class="value">13</span></a></li>
                                        <li><a href="#"><span class="item">от застройщика Эталон-Инвест</span> <span class="value">13</span></a></li>
                                    </ul>
                                    <a class="requests__views btn-view"><span>ещё</span> <i class="fas fa-angle-down"></i></a>
                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="rows">

                        <div class="col-left">

                            <div class="popular">
                                <div class="popular__heading">
                                    <h2>Популярные ЖК в Москве</h2>
                                    <a href="#" class="popular__heading_link">Как сюда попасть?</a>
                                </div>
                                <div class="popular__content tabs">

                                    <div class="tabs-item tab1 active">
                                        <div class="popular__row">

                                            <div class="popular__col">
                                                <a href="#" class="popular__item">
                                                    <div class="popular__item_image" style="background-image: url('images/item__thumb_04.jpg')"></div>
                                                    <div class="popular__item_text">
                                                        <div class="popular__item_price">100 000<span>p</span></div>
                                                        <div class="popular__item_type">4-комн. кв., 126 м</div>
                                                        <div class="popular_item_place">
                                                            <div class="metro">
                                                                <span class="metro__line" style="background-color: #03238B"></span>
                                                                <span class="metro__station">Семеновская</span>
                                                                <span class="metro__time">7 минут пешком</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="popular__col">
                                                <a href="#" class="popular__item">
                                                    <div class="popular__item_image" style="background-image: url('images/item__thumb_05.jpg')"></div>
                                                    <div class="popular__item_text">
                                                        <div class="popular__item_price">90 000<span>p</span></div>
                                                        <div class="popular__item_type">3-комн. кв., 92 м</div>
                                                        <div class="popular_item_place">
                                                            <div class="metro">
                                                                <span class="metro__line" style="background-color: #00701A"></span>
                                                                <span class="metro__station">Октябрьская</span>
                                                                <span class="metro__time">6 минут пешком</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="popular__col">
                                                <a href="#" class="popular__item">
                                                    <div class="popular__item_image" style="background-image: url('images/item__thumb_06.jpg')"></div>
                                                    <div class="popular__item_text">
                                                        <div class="popular__item_price">45 000<span>p</span></div>
                                                        <div class="popular__item_type">1-комн. кв., 58 м</div>
                                                        <div class="popular_item_place">
                                                            <div class="metro">
                                                                <span class="metro__line" style="background-color: #FFDF00"></span>
                                                                <span class="metro__station">Беляево</span>
                                                                <span class="metro__time">15 минут пешком</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>

                                        <a href="#" class="popular__all">Все предложения</a>

                                    </div>
                                </div>

                            </div>

                            <div class="functionality">

                                <div class="functionality__item">
                                    <div class="functionality__wrap">
                                        <div class="functionality__icon">
                                            <i class="far fa-edit"></i>
                                        </div>
                                        <h3>Добавьте свой ЖК</h3>
                                        <p>Бесплатное размещение объявлений для застройщиков  и их официальных представителей</p>
                                        <div class="functionality__bottom">
                                            <a href="#" class="btn btn-sm btn-radius">Связаться с менеджером</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="functionality__item">
                                    <div class="functionality__wrap">
                                        <div class="functionality__icon">
                                            <i class="far fa-map"></i>
                                        </div>
                                        <h3>Ищите на карте</h3>
                                        <p>Ищите объявления рядом с работой, парком или родственниками</p>
                                        <div class="functionality__bottom">
                                            <a href="#" class="btn btn-sm btn-radius">Найти на карте</a>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="col-right">

                            <div class="specialist-block">
                                <h3>Риэлторы</h3>
                                <a class="specialist-block__item" href="#">
                                    <div class="specialist-block__image">
                                        <img src="images/user__01.jpg" class="img-fluid" alt="">
                                    </div>
                                    <span class="specialist-block__name">Мария Щетинина</span>
                                    <span class="specialist-block__summary">26 предложений</span>
                                </a>
                                <a class="specialist-block__item" href="#">
                                    <div class="specialist-block__image">
                                        <img src="images/user__02.jpg" class="img-fluid" alt="">
                                    </div>
                                    <span class="specialist-block__name">Евгений Мазай</span>
                                    <span class="specialist-block__summary">2 предложения</span>
                                </a>
                                <a class="specialist-block__item" href="#">
                                    <div class="specialist-block__image">
                                        <img src="images/user__03.jpg" class="img-fluid" alt="">
                                    </div>
                                    <span class="specialist-block__name">Зоя Андрусяк</span>
                                    <span class="specialist-block__summary">1 предложение</span>
                                </a>
                                <a class="specialist-block__all" href="#">Все риэлторы</a>
                            </div>

                            <div class="specialist-block">
                                <h3>Агентства</h3>
                                <a class="specialist-block__item" href="#">
                                    <div class="specialist-block__logo">
                                        <img src="images/company__01.jpg" class="img-fluid" alt="">
                                    </div>
                                    <span class="specialist-block__name">Century 21 Римарком</span>
                                    <span class="specialist-block__summary">47 предложений</span>
                                </a>
                                <a class="specialist-block__item" href="#">
                                    <div class="specialist-block__logo">
                                        <img src="images/company__02.jpg" class="img-fluid" alt="">
                                    </div>
                                    <span class="specialist-block__name">МГСН-Аренда</span>
                                    <span class="specialist-block__summary">83 предложения</span>
                                </a>
                                <a class="specialist-block__item" href="#">
                                    <div class="specialist-block__logo">
                                        <img src="images/company__03.jpg" class="img-fluid" alt="">
                                    </div>
                                    <span class="specialist-block__name">БЛАГОГРАД</span>
                                    <span class="specialist-block__summary">38 предложение</span>
                                </a>
                                <a class="specialist-block__all" href="#">Все агентства</a>
                            </div>
                        </div>

                    </div>

                    <div class="rows">

                        <div class="col-left">
                            <div class="contentText">
                                <h1>Новостройки в Москве и области</h1>
                                <p>На сайте ЦИАН в категории продажи жилой недвижимости представлено максимальное количество объявлений в Москве. Все квартиры, дома и другие объекты в Москве проверены нашей командой модераторов. Для удобства вы можете загрузить мобильное приложение на iPhone и Android, а также удобно находить жилье благодаря структурированному каталогу и наличию поиска на нашем сайте. Для облегчения поиска арендного жилья, у нас реализована система рекомендаций похожих объявлений. Все объекты, которые Вас заинтересовали легко найти в личном кабинете с помощью добавления в "Избранное".</p>
                            </div>
                        </div>

                        <div class="col-right">
                            <div class="sideNews">
                                <h4>Новое на сайте</h4>
                                <ul>
                                    <li><a href="#">Продажа 2-комнатных квартир на улице Центральная в поселке Верея</a></li>
                                    <li><a href="#">Продажа 3-комнатных квартир в Дигорском районе республики Северная Осетия - Алания</a></li>
                                    <li><a href="#">Продажа квартир на улице Калачинская в городе Элиста</a></li>
                                    <li><a href="#">Продажа квартир на улице 2-я Усадебная в поселке Отрадное</a></li>
                                    <li><a href="#">Продажа трехкомнатных квартир на улице Харьковская в городе Прохладный</a></li>
                                </ul>
                            </div>

                        </div>
                    </div>


                    <div class="geo">

                        <div class="geo__block box-hide">
                            <h5>Квартиры у метро</h5>
                            <ul class="geo__list ">
                                <li><a href="#">Авиамоторная</a></li>
                                <li><a href="#">Верхние котлы</a></li>
                                <li><a href="#">Коптево</a></li>
                                <li><a href="#">Мякинино</a></li>
                                <li><a href="#">Пражская</a></li>
                                <li><a href="#">Таганская</a></li>
                                <li><a href="#">Автозаводская</a></li>
                                <li><a href="#">Владыкино</a></li>
                                <li><a href="#">Котельники</a></li>
                                <li><a href="#">Нагатинская</a></li>
                                <li><a href="#">Преображенская площадь</a></li>
                                <li><a href="#">Тверская</a></li>
                                <li><a href="#">Академическая</a></li>
                                <li><a href="#">Водный стадион</a></li>
                                <li><a href="#">Красногвардейская</a></li>
                                <li><a href="#">Нагорная</a></li>
                                <li><a href="#">Пролетарская</a></li>
                                <li><a href="#">Театральная</a></li>
                                <li><a href="#">Александровский сад</a></li>
                                <li><a href="#">Войковская</a></li>
                                <li><a href="#">Краснопресненская</a></li>
                                <li><a href="#">Нахимовский проспект</a></li>
                                <li><a href="#">Проспект Вернадского</a></li>
                                <li><a href="#">Текстильщики</a></li>
                                <li><a href="#">Алексеевская</a></li>
                                <li><a href="#">Волгоградский проспект</a></li>
                                <li><a href="#">Красносельская</a></li>
                                <li><a href="#">Нижегородская</a></li>
                                <li><a href="#">Проспект Мира</a></li>
                                <li><a href="#">Телецентр</a></li>
                                <li><a href="#">Алма-Атинская</a></li>
                                <li><a href="#">Волжская</a></li>
                                <li><a href="#">Красные ворота</a></li>
                                <li><a href="#">Новогиреево</a></li>
                                <li><a href="#">Профсоюзная</a></li>
                                <li><a href="#">Теплый Стан</a></li>
                                <li><a href="#">Алтуфьево</a></li>
                                <li><a href="#">Волоколамская</a></li>
                                <li><a href="#">Крестьянская застава</a></li>
                                <li><a href="#">Новокосино</a></li>
                                <li><a href="#">Пушкинская</a></li>
                                <li><a href="#">Технопарк</a></li>
                                <li><a href="#">Андроновка</a></li>
                                <li><a href="#">Воробьевы горы</a></li>
                                <li><a href="#">Кропоткинская</a></li>
                                <li><a href="#">Новокузнецкая</a></li>
                                <li><a href="#">Пятницкое шоссе</a></li>
                                <li><a href="#">Тимирязевская</a></li>
                                <li><a href="#">Аннино</a></li>
                                <li><a href="#">Выставочная</a></li>
                                <li><a href="#">Крылатское</a></li>
                                <li><a href="#">Новослободская</a></li>
                                <li><a href="#">Раменки</a></li>
                                <li><a href="#">Третьяковская</a></li>
                                <li><a href="#">Арбатская</a></li>
                                <li><a href="#">Выставочный центр</a></li>
                                <li><a href="#">Крымская</a></li>
                                <li><a href="#">Новохохловская</a></li>
                                <li><a href="#">Речной вокзал</a></li>
                                <li><a href="#">Тропарево</a></li>
                                <li><a href="#">Аэропорт</a></li>
                                <li><a href="#">Выхино</a></li>
                                <li><a href="#">Кузнецкий мост</a></li>
                                <li><a href="#">Новоясеневская</a></li>
                                <li><a href="#">Рижская</a></li>
                                <li><a href="#">Трубная</a></li>
                                <li><a href="#">Бабушкинская</a></li>
                                <li><a href="#">Деловой центр</a></li>
                                <li><a href="#">Кузьминки</a></li>
                                <li><a href="#">Новые Черемушки</a></li>
                                <li><a href="#">Римская</a></li>
                                <li><a href="#">Тульская</a></li>
                                <li><a href="#">Багратионовская</a></li>
                                <li><a href="#">Динамо</a></li>
                                <li><a href="#">Кунцевская</a></li>
                                <li><a href="#">Окружная</a></li>
                                <li><a href="#">Ростокино</a></li>
                                <li><a href="#">Тургеневская</a></li>
                                <li><a href="#">Балтийская</a></li>
                                <li><a href="#">Дмитровская</a></li>
                                <li><a href="#">Курская</a></li>
                                <li><a href="#">Октябрьская</a></li>
                                <li><a href="#">Румянцево</a></li>
                                <li><a href="#">Тушинская</a></li>
                                <li><a href="#">Баррикадная</a></li>
                                <li><a href="#">Добрынинская</a></li>
                                <li><a href="#">Кутузовская</a></li>
                                <li><a href="#">Октябрьское поле</a></li>
                                <li><a href="#">Рязанский проспект</a></li>
                                <li><a href="#">Улица 1905 года</a></li>
                                <li><a href="#">Улица Сергея Эйзенштейна</a></li>
                                <li><a href="#">Бибирево</a></li>
                                <li><a href="#">Зорге</a></li>
                                <li><a href="#">Ломоносовский проспект</a></li>
                                <li><a href="#">Парк Культуры</a></li>
                                <li><a href="#">Серпуховская</a></li>
                                <li><a href="#">Улица Скобелевская</a></li>
                                <li><a href="#">Зябликово</a></li>
                                <li><a href="#">Лубянка</a></li>
                                <li><a href="#">Парк Победы</a></li>
                                <li><a href="#">Славянский бульвар</a></li>
                                <li><a href="#">Улица Старокачаловская</a></li>
                                <li><a href="#">Битцевский парк</a></li>
                                <li><a href="#">Измайлово</a></li>
                                <li><a href="#">Лужники</a></li>
                                <li><a href="#">Партизанская</a></li>
                                <li><a href="#">Смоленская</a></li>
                                <li><a href="#">Университет</a></li>
                                <li><a href="#">Борисово</a></li>
                                <li><a href="#">Измайловская</a></li>
                                <li><a href="#">Люблино</a></li>
                                <li><a href="#">Первомайская</a></li>
                                <li><a href="#">Сокол</a></li>
                                <li><a href="#">Боровицкая</a></li>
                                <li><a href="#">Калужская</a></li>
                                <li><a href="#">Марксистская</a></li>
                                <li><a href="#">Перово</a></li>
                                <li><a href="#">Соколиная гора</a></li>
                                <li><a href="#">Фили</a></li>
                                <li><a href="#">Ботанический сад</a></li>
                                <li><a href="#">Кантемировская</a></li>
                                <li><a href="#">Марьина роща</a></li>
                                <li><a href="#">Петровско-Разумовская</a></li>
                                <li><a href="#">Сокольники</a></li>
                                <li><a href="#">Фонвизинская</a></li>
                                <li><a href="#">Братиславская</a></li>
                                <li><a href="#">Каховская</a></li>
                                <li><a href="#">Марьино</a></li>
                                <li><a href="#">Печатники</a></li>
                                <li><a href="#">Спартак</a></li>
                                <li><a href="#">Фрунзенская</a></li>
                                <li><a href="#">Бульвар Адмирала Ушакова</a></li>
                                <li><a href="#">Каширская</a></li>
                                <li><a href="#">Маяковская</a></li>
                                <li><a href="#">Пионерская</a></li>
                                <li><a href="#">Спортивная</a></li>
                                <li><a href="#">Ховрино</a></li>
                                <li><a href="#">Бульвар Дмитрия Донского</a></li>
                                <li><a href="#">Киевская</a></li>
                                <li><a href="#">Медведково</a></li>
                                <li><a href="#">Планерная</a></li>
                                <li><a href="#">Сретенский бульвар</a></li>
                                <li><a href="#">Хорошёво</a></li>
                                <li><a href="#">Бульвар Рокоссовского</a></li>
                                <li><a href="#">Китай-город</a></li>
                                <li><a href="#">Международная</a></li>
                                <li><a href="#">Площадь Гагарина</a></li>
                                <li><a href="#">Стрешнево</a></li>
                                <li><a href="#">Царицыно</a></li>
                                <li><a href="#">Бунинская аллея</a></li>
                                <li><a href="#">Кожуховская</a></li>
                                <li><a href="#">Менделеевская</a></li>
                                <li><a href="#">Площадь Ильича</a></li>
                                <li><a href="#">Строгино</a></li>
                                <li><a href="#">Бутырская</a></li>
                                <li><a href="#">Коломенская</a></li>
                                <li><a href="#">Минская</a></li>
                                <li><a href="#">Площадь Революции</a></li>
                                <li><a href="#">Студенческая</a></li>
                                <li><a href="#">ВДНХ</a></li>
                                <li><a href="#">Комсомольская</a></li>
                                <li><a href="#">Митино</a></li>
                                <li><a href="#">Полежаевская</a></li>
                                <li><a href="#">Сухаревская</a></li>
                                <li><a href="#">Варшавская</a></li>
                                <li><a href="#">Коньково</a></li>
                                <li><a href="#">Молодежная</a></li>
                                <li><a href="#">Полянка</a></li>
                                <li><a href="#">Сходненская</a></li>
                            </ul>
                            <a href="#" class="geo__views btn-view"><span>Еще</span> <i class="fas fa-angle-down"></i></a>
                        </div>

                        <div class="geo__block box-hide">
                            <h5>Квартиры в районе</h5>
                            <ul class="geo__list ">
                                <li><a href="#">Авиамоторная</a></li>
                                <li><a href="#">Верхние котлы</a></li>
                                <li><a href="#">Коптево</a></li>
                                <li><a href="#">Мякинино</a></li>
                                <li><a href="#">Пражская</a></li>
                                <li><a href="#">Таганская</a></li>
                                <li><a href="#">Автозаводская</a></li>
                                <li><a href="#">Владыкино</a></li>
                                <li><a href="#">Котельники</a></li>
                                <li><a href="#">Нагатинская</a></li>
                                <li><a href="#">Преображенская площадь</a></li>
                                <li><a href="#">Тверская</a></li>
                                <li><a href="#">Академическая</a></li>
                                <li><a href="#">Водный стадион</a></li>
                                <li><a href="#">Красногвардейская</a></li>
                                <li><a href="#">Нагорная</a></li>
                                <li><a href="#">Пролетарская</a></li>
                                <li><a href="#">Театральная</a></li>
                                <li><a href="#">Александровский сад</a></li>
                                <li><a href="#">Войковская</a></li>
                                <li><a href="#">Краснопресненская</a></li>
                                <li><a href="#">Нахимовский проспект</a></li>
                                <li><a href="#">Проспект Вернадского</a></li>
                                <li><a href="#">Текстильщики</a></li>
                                <li><a href="#">Алексеевская</a></li>
                                <li><a href="#">Волгоградский проспект</a></li>
                                <li><a href="#">Красносельская</a></li>
                                <li><a href="#">Нижегородская</a></li>
                                <li><a href="#">Проспект Мира</a></li>
                                <li><a href="#">Телецентр</a></li>
                                <li><a href="#">Алма-Атинская</a></li>
                                <li><a href="#">Волжская</a></li>
                                <li><a href="#">Красные ворота</a></li>
                                <li><a href="#">Новогиреево</a></li>
                                <li><a href="#">Профсоюзная</a></li>
                                <li><a href="#">Теплый Стан</a></li>
                                <li><a href="#">Алтуфьево</a></li>
                                <li><a href="#">Волоколамская</a></li>
                                <li><a href="#">Крестьянская застава</a></li>
                                <li><a href="#">Новокосино</a></li>
                                <li><a href="#">Пушкинская</a></li>
                                <li><a href="#">Технопарк</a></li>
                                <li><a href="#">Андроновка</a></li>
                                <li><a href="#">Воробьевы горы</a></li>
                                <li><a href="#">Кропоткинская</a></li>
                                <li><a href="#">Новокузнецкая</a></li>
                                <li><a href="#">Пятницкое шоссе</a></li>
                                <li><a href="#">Тимирязевская</a></li>
                                <li><a href="#">Аннино</a></li>
                                <li><a href="#">Выставочная</a></li>
                                <li><a href="#">Крылатское</a></li>
                                <li><a href="#">Новослободская</a></li>
                                <li><a href="#">Раменки</a></li>
                                <li><a href="#">Третьяковская</a></li>
                                <li><a href="#">Арбатская</a></li>
                                <li><a href="#">Выставочный центр</a></li>
                                <li><a href="#">Крымская</a></li>
                                <li><a href="#">Новохохловская</a></li>
                                <li><a href="#">Речной вокзал</a></li>
                                <li><a href="#">Тропарево</a></li>
                                <li><a href="#">Аэропорт</a></li>
                                <li><a href="#">Выхино</a></li>
                                <li><a href="#">Кузнецкий мост</a></li>
                                <li><a href="#">Новоясеневская</a></li>
                                <li><a href="#">Рижская</a></li>
                                <li><a href="#">Трубная</a></li>
                                <li><a href="#">Бабушкинская</a></li>
                                <li><a href="#">Деловой центр</a></li>
                                <li><a href="#">Кузьминки</a></li>
                                <li><a href="#">Новые Черемушки</a></li>
                                <li><a href="#">Римская</a></li>
                                <li><a href="#">Тульская</a></li>
                                <li><a href="#">Багратионовская</a></li>
                                <li><a href="#">Динамо</a></li>
                                <li><a href="#">Кунцевская</a></li>
                                <li><a href="#">Окружная</a></li>
                                <li><a href="#">Ростокино</a></li>
                                <li><a href="#">Тургеневская</a></li>
                                <li><a href="#">Балтийская</a></li>
                                <li><a href="#">Дмитровская</a></li>
                                <li><a href="#">Курская</a></li>
                                <li><a href="#">Октябрьская</a></li>
                                <li><a href="#">Румянцево</a></li>
                                <li><a href="#">Тушинская</a></li>
                                <li><a href="#">Баррикадная</a></li>
                                <li><a href="#">Добрынинская</a></li>
                                <li><a href="#">Кутузовская</a></li>
                                <li><a href="#">Октябрьское поле</a></li>
                                <li><a href="#">Рязанский проспект</a></li>
                                <li><a href="#">Улица 1905 года</a></li>
                                <li><a href="#">Улица Сергея Эйзенштейна</a></li>
                                <li><a href="#">Бибирево</a></li>
                                <li><a href="#">Зорге</a></li>
                                <li><a href="#">Ломоносовский проспект</a></li>
                                <li><a href="#">Парк Культуры</a></li>
                                <li><a href="#">Серпуховская</a></li>
                                <li><a href="#">Улица Скобелевская</a></li>
                                <li><a href="#">Зябликово</a></li>
                                <li><a href="#">Лубянка</a></li>
                                <li><a href="#">Парк Победы</a></li>
                                <li><a href="#">Славянский бульвар</a></li>
                                <li><a href="#">Улица Старокачаловская</a></li>
                                <li><a href="#">Битцевский парк</a></li>
                                <li><a href="#">Измайлово</a></li>
                                <li><a href="#">Лужники</a></li>
                                <li><a href="#">Партизанская</a></li>
                                <li><a href="#">Смоленская</a></li>
                                <li><a href="#">Университет</a></li>
                                <li><a href="#">Борисово</a></li>
                                <li><a href="#">Измайловская</a></li>
                                <li><a href="#">Люблино</a></li>
                                <li><a href="#">Первомайская</a></li>
                                <li><a href="#">Сокол</a></li>
                                <li><a href="#">Боровицкая</a></li>
                                <li><a href="#">Калужская</a></li>
                                <li><a href="#">Марксистская</a></li>
                                <li><a href="#">Перово</a></li>
                                <li><a href="#">Соколиная гора</a></li>
                                <li><a href="#">Фили</a></li>
                                <li><a href="#">Ботанический сад</a></li>
                                <li><a href="#">Кантемировская</a></li>
                                <li><a href="#">Марьина роща</a></li>
                                <li><a href="#">Петровско-Разумовская</a></li>
                                <li><a href="#">Сокольники</a></li>
                                <li><a href="#">Фонвизинская</a></li>
                                <li><a href="#">Братиславская</a></li>
                                <li><a href="#">Каховская</a></li>
                                <li><a href="#">Марьино</a></li>
                                <li><a href="#">Печатники</a></li>
                                <li><a href="#">Спартак</a></li>
                                <li><a href="#">Фрунзенская</a></li>
                                <li><a href="#">Бульвар Адмирала Ушакова</a></li>
                                <li><a href="#">Каширская</a></li>
                                <li><a href="#">Маяковская</a></li>
                                <li><a href="#">Пионерская</a></li>
                                <li><a href="#">Спортивная</a></li>
                                <li><a href="#">Ховрино</a></li>
                                <li><a href="#">Бульвар Дмитрия Донского</a></li>
                                <li><a href="#">Киевская</a></li>
                                <li><a href="#">Медведково</a></li>
                                <li><a href="#">Планерная</a></li>
                                <li><a href="#">Сретенский бульвар</a></li>
                                <li><a href="#">Хорошёво</a></li>
                                <li><a href="#">Бульвар Рокоссовского</a></li>
                                <li><a href="#">Китай-город</a></li>
                                <li><a href="#">Международная</a></li>
                                <li><a href="#">Площадь Гагарина</a></li>
                                <li><a href="#">Стрешнево</a></li>
                                <li><a href="#">Царицыно</a></li>
                                <li><a href="#">Бунинская аллея</a></li>
                                <li><a href="#">Кожуховская</a></li>
                                <li><a href="#">Менделеевская</a></li>
                                <li><a href="#">Площадь Ильича</a></li>
                                <li><a href="#">Строгино</a></li>
                                <li><a href="#">Бутырская</a></li>
                                <li><a href="#">Коломенская</a></li>
                                <li><a href="#">Минская</a></li>
                                <li><a href="#">Площадь Революции</a></li>
                                <li><a href="#">Студенческая</a></li>
                                <li><a href="#">ВДНХ</a></li>
                                <li><a href="#">Комсомольская</a></li>
                                <li><a href="#">Митино</a></li>
                                <li><a href="#">Полежаевская</a></li>
                                <li><a href="#">Сухаревская</a></li>
                                <li><a href="#">Варшавская</a></li>
                                <li><a href="#">Коньково</a></li>
                                <li><a href="#">Молодежная</a></li>
                                <li><a href="#">Полянка</a></li>
                                <li><a href="#">Сходненская</a></li>
                            </ul>
                            <a href="#" class="geo__views btn-view"><span>Еще</span> <i class="fas fa-angle-down"></i></a>
                        </div>

                        <div class="geo__block box-hide">
                            <h5>Офисы у метро</h5>
                            <ul class="geo__list ">
                                <li><a href="#">Авиамоторная</a></li>
                                <li><a href="#">Верхние котлы</a></li>
                                <li><a href="#">Коптево</a></li>
                                <li><a href="#">Мякинино</a></li>
                                <li><a href="#">Пражская</a></li>
                                <li><a href="#">Таганская</a></li>
                                <li><a href="#">Автозаводская</a></li>
                                <li><a href="#">Владыкино</a></li>
                                <li><a href="#">Котельники</a></li>
                                <li><a href="#">Нагатинская</a></li>
                                <li><a href="#">Преображенская площадь</a></li>
                                <li><a href="#">Тверская</a></li>
                                <li><a href="#">Академическая</a></li>
                                <li><a href="#">Водный стадион</a></li>
                                <li><a href="#">Красногвардейская</a></li>
                                <li><a href="#">Нагорная</a></li>
                                <li><a href="#">Пролетарская</a></li>
                                <li><a href="#">Театральная</a></li>
                                <li><a href="#">Александровский сад</a></li>
                                <li><a href="#">Войковская</a></li>
                                <li><a href="#">Краснопресненская</a></li>
                                <li><a href="#">Нахимовский проспект</a></li>
                                <li><a href="#">Проспект Вернадского</a></li>
                                <li><a href="#">Текстильщики</a></li>
                                <li><a href="#">Алексеевская</a></li>
                                <li><a href="#">Волгоградский проспект</a></li>
                                <li><a href="#">Красносельская</a></li>
                                <li><a href="#">Нижегородская</a></li>
                                <li><a href="#">Проспект Мира</a></li>
                                <li><a href="#">Телецентр</a></li>
                                <li><a href="#">Алма-Атинская</a></li>
                                <li><a href="#">Волжская</a></li>
                                <li><a href="#">Красные ворота</a></li>
                                <li><a href="#">Новогиреево</a></li>
                                <li><a href="#">Профсоюзная</a></li>
                                <li><a href="#">Теплый Стан</a></li>
                                <li><a href="#">Алтуфьево</a></li>
                                <li><a href="#">Волоколамская</a></li>
                                <li><a href="#">Крестьянская застава</a></li>
                                <li><a href="#">Новокосино</a></li>
                                <li><a href="#">Пушкинская</a></li>
                                <li><a href="#">Технопарк</a></li>
                                <li><a href="#">Андроновка</a></li>
                                <li><a href="#">Воробьевы горы</a></li>
                                <li><a href="#">Кропоткинская</a></li>
                                <li><a href="#">Новокузнецкая</a></li>
                                <li><a href="#">Пятницкое шоссе</a></li>
                                <li><a href="#">Тимирязевская</a></li>
                                <li><a href="#">Аннино</a></li>
                                <li><a href="#">Выставочная</a></li>
                                <li><a href="#">Крылатское</a></li>
                                <li><a href="#">Новослободская</a></li>
                                <li><a href="#">Раменки</a></li>
                                <li><a href="#">Третьяковская</a></li>
                                <li><a href="#">Арбатская</a></li>
                                <li><a href="#">Выставочный центр</a></li>
                                <li><a href="#">Крымская</a></li>
                                <li><a href="#">Новохохловская</a></li>
                                <li><a href="#">Речной вокзал</a></li>
                                <li><a href="#">Тропарево</a></li>
                                <li><a href="#">Аэропорт</a></li>
                                <li><a href="#">Выхино</a></li>
                                <li><a href="#">Кузнецкий мост</a></li>
                                <li><a href="#">Новоясеневская</a></li>
                                <li><a href="#">Рижская</a></li>
                                <li><a href="#">Трубная</a></li>
                                <li><a href="#">Бабушкинская</a></li>
                                <li><a href="#">Деловой центр</a></li>
                                <li><a href="#">Кузьминки</a></li>
                                <li><a href="#">Новые Черемушки</a></li>
                                <li><a href="#">Римская</a></li>
                                <li><a href="#">Тульская</a></li>
                                <li><a href="#">Багратионовская</a></li>
                                <li><a href="#">Динамо</a></li>
                                <li><a href="#">Кунцевская</a></li>
                                <li><a href="#">Окружная</a></li>
                                <li><a href="#">Ростокино</a></li>
                                <li><a href="#">Тургеневская</a></li>
                                <li><a href="#">Балтийская</a></li>
                                <li><a href="#">Дмитровская</a></li>
                                <li><a href="#">Курская</a></li>
                                <li><a href="#">Октябрьская</a></li>
                                <li><a href="#">Румянцево</a></li>
                                <li><a href="#">Тушинская</a></li>
                                <li><a href="#">Баррикадная</a></li>
                                <li><a href="#">Добрынинская</a></li>
                                <li><a href="#">Кутузовская</a></li>
                                <li><a href="#">Октябрьское поле</a></li>
                                <li><a href="#">Рязанский проспект</a></li>
                                <li><a href="#">Улица 1905 года</a></li>
                                <li><a href="#">Улица Сергея Эйзенштейна</a></li>
                                <li><a href="#">Бибирево</a></li>
                                <li><a href="#">Зорге</a></li>
                                <li><a href="#">Ломоносовский проспект</a></li>
                                <li><a href="#">Парк Культуры</a></li>
                                <li><a href="#">Серпуховская</a></li>
                                <li><a href="#">Улица Скобелевская</a></li>
                                <li><a href="#">Зябликово</a></li>
                                <li><a href="#">Лубянка</a></li>
                                <li><a href="#">Парк Победы</a></li>
                                <li><a href="#">Славянский бульвар</a></li>
                                <li><a href="#">Улица Старокачаловская</a></li>
                                <li><a href="#">Битцевский парк</a></li>
                                <li><a href="#">Измайлово</a></li>
                                <li><a href="#">Лужники</a></li>
                                <li><a href="#">Партизанская</a></li>
                                <li><a href="#">Смоленская</a></li>
                                <li><a href="#">Университет</a></li>
                                <li><a href="#">Борисово</a></li>
                                <li><a href="#">Измайловская</a></li>
                                <li><a href="#">Люблино</a></li>
                                <li><a href="#">Первомайская</a></li>
                                <li><a href="#">Сокол</a></li>
                                <li><a href="#">Боровицкая</a></li>
                                <li><a href="#">Калужская</a></li>
                                <li><a href="#">Марксистская</a></li>
                                <li><a href="#">Перово</a></li>
                                <li><a href="#">Соколиная гора</a></li>
                                <li><a href="#">Фили</a></li>
                                <li><a href="#">Ботанический сад</a></li>
                                <li><a href="#">Кантемировская</a></li>
                                <li><a href="#">Марьина роща</a></li>
                                <li><a href="#">Петровско-Разумовская</a></li>
                                <li><a href="#">Сокольники</a></li>
                                <li><a href="#">Фонвизинская</a></li>
                                <li><a href="#">Братиславская</a></li>
                                <li><a href="#">Каховская</a></li>
                                <li><a href="#">Марьино</a></li>
                                <li><a href="#">Печатники</a></li>
                                <li><a href="#">Спартак</a></li>
                                <li><a href="#">Фрунзенская</a></li>
                                <li><a href="#">Бульвар Адмирала Ушакова</a></li>
                                <li><a href="#">Каширская</a></li>
                                <li><a href="#">Маяковская</a></li>
                                <li><a href="#">Пионерская</a></li>
                                <li><a href="#">Спортивная</a></li>
                                <li><a href="#">Ховрино</a></li>
                                <li><a href="#">Бульвар Дмитрия Донского</a></li>
                                <li><a href="#">Киевская</a></li>
                                <li><a href="#">Медведково</a></li>
                                <li><a href="#">Планерная</a></li>
                                <li><a href="#">Сретенский бульвар</a></li>
                                <li><a href="#">Хорошёво</a></li>
                                <li><a href="#">Бульвар Рокоссовского</a></li>
                                <li><a href="#">Китай-город</a></li>
                                <li><a href="#">Международная</a></li>
                                <li><a href="#">Площадь Гагарина</a></li>
                                <li><a href="#">Стрешнево</a></li>
                                <li><a href="#">Царицыно</a></li>
                                <li><a href="#">Бунинская аллея</a></li>
                                <li><a href="#">Кожуховская</a></li>
                                <li><a href="#">Менделеевская</a></li>
                                <li><a href="#">Площадь Ильича</a></li>
                                <li><a href="#">Строгино</a></li>
                                <li><a href="#">Бутырская</a></li>
                                <li><a href="#">Коломенская</a></li>
                                <li><a href="#">Минская</a></li>
                                <li><a href="#">Площадь Революции</a></li>
                                <li><a href="#">Студенческая</a></li>
                                <li><a href="#">ВДНХ</a></li>
                                <li><a href="#">Комсомольская</a></li>
                                <li><a href="#">Митино</a></li>
                                <li><a href="#">Полежаевская</a></li>
                                <li><a href="#">Сухаревская</a></li>
                                <li><a href="#">Варшавская</a></li>
                                <li><a href="#">Коньково</a></li>
                                <li><a href="#">Молодежная</a></li>
                                <li><a href="#">Полянка</a></li>
                                <li><a href="#">Сходненская</a></li>
                            </ul>
                            <a href="#" class="geo__views btn-view"><span>Еще</span> <i class="fas fa-angle-down"></i></a>
                        </div>

                        <div class="geo__block box-hide">
                            <h5>Офисы в районе</h5>
                            <ul class="geo__list ">
                                <li><a href="#">Авиамоторная</a></li>
                                <li><a href="#">Верхние котлы</a></li>
                                <li><a href="#">Коптево</a></li>
                                <li><a href="#">Мякинино</a></li>
                                <li><a href="#">Пражская</a></li>
                                <li><a href="#">Таганская</a></li>
                                <li><a href="#">Автозаводская</a></li>
                                <li><a href="#">Владыкино</a></li>
                                <li><a href="#">Котельники</a></li>
                                <li><a href="#">Нагатинская</a></li>
                                <li><a href="#">Преображенская площадь</a></li>
                                <li><a href="#">Тверская</a></li>
                                <li><a href="#">Академическая</a></li>
                                <li><a href="#">Водный стадион</a></li>
                                <li><a href="#">Красногвардейская</a></li>
                                <li><a href="#">Нагорная</a></li>
                                <li><a href="#">Пролетарская</a></li>
                                <li><a href="#">Театральная</a></li>
                                <li><a href="#">Александровский сад</a></li>
                                <li><a href="#">Войковская</a></li>
                                <li><a href="#">Краснопресненская</a></li>
                                <li><a href="#">Нахимовский проспект</a></li>
                                <li><a href="#">Проспект Вернадского</a></li>
                                <li><a href="#">Текстильщики</a></li>
                                <li><a href="#">Алексеевская</a></li>
                                <li><a href="#">Волгоградский проспект</a></li>
                                <li><a href="#">Красносельская</a></li>
                                <li><a href="#">Нижегородская</a></li>
                                <li><a href="#">Проспект Мира</a></li>
                                <li><a href="#">Телецентр</a></li>
                                <li><a href="#">Алма-Атинская</a></li>
                                <li><a href="#">Волжская</a></li>
                                <li><a href="#">Красные ворота</a></li>
                                <li><a href="#">Новогиреево</a></li>
                                <li><a href="#">Профсоюзная</a></li>
                                <li><a href="#">Теплый Стан</a></li>
                                <li><a href="#">Алтуфьево</a></li>
                                <li><a href="#">Волоколамская</a></li>
                                <li><a href="#">Крестьянская застава</a></li>
                                <li><a href="#">Новокосино</a></li>
                                <li><a href="#">Пушкинская</a></li>
                                <li><a href="#">Технопарк</a></li>
                                <li><a href="#">Андроновка</a></li>
                                <li><a href="#">Воробьевы горы</a></li>
                                <li><a href="#">Кропоткинская</a></li>
                                <li><a href="#">Новокузнецкая</a></li>
                                <li><a href="#">Пятницкое шоссе</a></li>
                                <li><a href="#">Тимирязевская</a></li>
                                <li><a href="#">Аннино</a></li>
                                <li><a href="#">Выставочная</a></li>
                                <li><a href="#">Крылатское</a></li>
                                <li><a href="#">Новослободская</a></li>
                                <li><a href="#">Раменки</a></li>
                                <li><a href="#">Третьяковская</a></li>
                                <li><a href="#">Арбатская</a></li>
                                <li><a href="#">Выставочный центр</a></li>
                                <li><a href="#">Крымская</a></li>
                                <li><a href="#">Новохохловская</a></li>
                                <li><a href="#">Речной вокзал</a></li>
                                <li><a href="#">Тропарево</a></li>
                                <li><a href="#">Аэропорт</a></li>
                                <li><a href="#">Выхино</a></li>
                                <li><a href="#">Кузнецкий мост</a></li>
                                <li><a href="#">Новоясеневская</a></li>
                                <li><a href="#">Рижская</a></li>
                                <li><a href="#">Трубная</a></li>
                                <li><a href="#">Бабушкинская</a></li>
                                <li><a href="#">Деловой центр</a></li>
                                <li><a href="#">Кузьминки</a></li>
                                <li><a href="#">Новые Черемушки</a></li>
                                <li><a href="#">Римская</a></li>
                                <li><a href="#">Тульская</a></li>
                                <li><a href="#">Багратионовская</a></li>
                                <li><a href="#">Динамо</a></li>
                                <li><a href="#">Кунцевская</a></li>
                                <li><a href="#">Окружная</a></li>
                                <li><a href="#">Ростокино</a></li>
                                <li><a href="#">Тургеневская</a></li>
                                <li><a href="#">Балтийская</a></li>
                                <li><a href="#">Дмитровская</a></li>
                                <li><a href="#">Курская</a></li>
                                <li><a href="#">Октябрьская</a></li>
                                <li><a href="#">Румянцево</a></li>
                                <li><a href="#">Тушинская</a></li>
                                <li><a href="#">Баррикадная</a></li>
                                <li><a href="#">Добрынинская</a></li>
                                <li><a href="#">Кутузовская</a></li>
                                <li><a href="#">Октябрьское поле</a></li>
                                <li><a href="#">Рязанский проспект</a></li>
                                <li><a href="#">Улица 1905 года</a></li>
                                <li><a href="#">Улица Сергея Эйзенштейна</a></li>
                                <li><a href="#">Бибирево</a></li>
                                <li><a href="#">Зорге</a></li>
                                <li><a href="#">Ломоносовский проспект</a></li>
                                <li><a href="#">Парк Культуры</a></li>
                                <li><a href="#">Серпуховская</a></li>
                                <li><a href="#">Улица Скобелевская</a></li>
                                <li><a href="#">Зябликово</a></li>
                                <li><a href="#">Лубянка</a></li>
                                <li><a href="#">Парк Победы</a></li>
                                <li><a href="#">Славянский бульвар</a></li>
                                <li><a href="#">Улица Старокачаловская</a></li>
                                <li><a href="#">Битцевский парк</a></li>
                                <li><a href="#">Измайлово</a></li>
                                <li><a href="#">Лужники</a></li>
                                <li><a href="#">Партизанская</a></li>
                                <li><a href="#">Смоленская</a></li>
                                <li><a href="#">Университет</a></li>
                                <li><a href="#">Борисово</a></li>
                                <li><a href="#">Измайловская</a></li>
                                <li><a href="#">Люблино</a></li>
                                <li><a href="#">Первомайская</a></li>
                                <li><a href="#">Сокол</a></li>
                                <li><a href="#">Боровицкая</a></li>
                                <li><a href="#">Калужская</a></li>
                                <li><a href="#">Марксистская</a></li>
                                <li><a href="#">Перово</a></li>
                                <li><a href="#">Соколиная гора</a></li>
                                <li><a href="#">Фили</a></li>
                                <li><a href="#">Ботанический сад</a></li>
                                <li><a href="#">Кантемировская</a></li>
                                <li><a href="#">Марьина роща</a></li>
                                <li><a href="#">Петровско-Разумовская</a></li>
                                <li><a href="#">Сокольники</a></li>
                                <li><a href="#">Фонвизинская</a></li>
                                <li><a href="#">Братиславская</a></li>
                                <li><a href="#">Каховская</a></li>
                                <li><a href="#">Марьино</a></li>
                                <li><a href="#">Печатники</a></li>
                                <li><a href="#">Спартак</a></li>
                                <li><a href="#">Фрунзенская</a></li>
                                <li><a href="#">Бульвар Адмирала Ушакова</a></li>
                                <li><a href="#">Каширская</a></li>
                                <li><a href="#">Маяковская</a></li>
                                <li><a href="#">Пионерская</a></li>
                                <li><a href="#">Спортивная</a></li>
                                <li><a href="#">Ховрино</a></li>
                                <li><a href="#">Бульвар Дмитрия Донского</a></li>
                                <li><a href="#">Киевская</a></li>
                                <li><a href="#">Медведково</a></li>
                                <li><a href="#">Планерная</a></li>
                                <li><a href="#">Сретенский бульвар</a></li>
                                <li><a href="#">Хорошёво</a></li>
                                <li><a href="#">Бульвар Рокоссовского</a></li>
                                <li><a href="#">Китай-город</a></li>
                                <li><a href="#">Международная</a></li>
                                <li><a href="#">Площадь Гагарина</a></li>
                                <li><a href="#">Стрешнево</a></li>
                                <li><a href="#">Царицыно</a></li>
                                <li><a href="#">Бунинская аллея</a></li>
                                <li><a href="#">Кожуховская</a></li>
                                <li><a href="#">Менделеевская</a></li>
                                <li><a href="#">Площадь Ильича</a></li>
                                <li><a href="#">Строгино</a></li>
                                <li><a href="#">Бутырская</a></li>
                                <li><a href="#">Коломенская</a></li>
                                <li><a href="#">Минская</a></li>
                                <li><a href="#">Площадь Революции</a></li>
                                <li><a href="#">Студенческая</a></li>
                                <li><a href="#">ВДНХ</a></li>
                                <li><a href="#">Комсомольская</a></li>
                                <li><a href="#">Митино</a></li>
                                <li><a href="#">Полежаевская</a></li>
                                <li><a href="#">Сухаревская</a></li>
                                <li><a href="#">Варшавская</a></li>
                                <li><a href="#">Коньково</a></li>
                                <li><a href="#">Молодежная</a></li>
                                <li><a href="#">Полянка</a></li>
                                <li><a href="#">Сходненская</a></li>
                            </ul>
                            <a href="#" class="geo__views btn-view"><span>Еще</span> <i class="fas fa-angle-down"></i></a>
                        </div>

                    </div>


                    <div class="main-block">

                    </div>

                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->



        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->



    </body>
</html>
