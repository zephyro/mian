<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Filter -->
            <div class="filter">
                <div class="container">

                    <div class="filter__heading">
                        <span class="filter__heading_name">Фильтр</span>
                        <span class="filter__heading_toggle"><i class="fas fa-bars"></i></span>
                    </div>

                    <div class="filter__content">
                        <form class="filter-form">

                            <div class="filter__row">

                                <div class="filter__elem">
                                    <div class="filter__group">
                                        <div class="filter__group_col">
                                            <div class="mySelect filter__select">
                                                <div class="mySelect__btn">
                                                    <span class="mySelect__selected">Купить</span>
                                                </div>
                                                <div class="mySelect__dropdown">
                                                    <div class="mySelect__wrap">
                                                        <ul>
                                                            <li>
                                                                <label class="mySelect__radio">
                                                                    <input type="radio" name="filter__deal" value="Продажа" checked>
                                                                    <span>Купить</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="mySelect__radio">
                                                                    <input type="radio" name="filter__deal" value="Аренда">
                                                                    <span>Снять</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="mySelect__radio">
                                                                    <input type="radio" name="filter__deal" value="Посуточно">
                                                                    <span>Посуточно</span>
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="filter__group_col">
                                            <div class="mySelect filter__select">
                                                <div class="mySelect__btn">
                                                    <span class="mySelect__selected">Квартиру</span>
                                                </div>
                                                <div class="mySelect__dropdown">
                                                    <div class="mySelect__wrap">
                                                        <ul>
                                                            <li>
                                                                <label class="mySelect__radio">
                                                                    <input type="radio" name="filter__object" value="Квартиру"checked>
                                                                    <span>Квартира</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="mySelect__radio">
                                                                    <input type="radio" name="filter__object" value="Комнату">
                                                                    <span>Комната</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="mySelect__radio">
                                                                    <input type="radio" name="filter__object" value="Дом">
                                                                    <span>Дом</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="mySelect__radio">
                                                                    <input type="radio" name="filter__object" value="Коттедж">
                                                                    <span>Коттедж</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="mySelect__radio">
                                                                    <input type="radio" name="filter__object" value="Участок">
                                                                    <span>Участок</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="mySelect__radio">
                                                                    <input type="radio" name="filter__object" value="Офис">
                                                                    <span>Офис</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="mySelect__radio">
                                                                    <input type="radio" name="filter__object" value="Торговая площадь">
                                                                    <span>Торговая площадь</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="mySelect__radio">
                                                                    <input type="radio" name="filter__object" value="Склад">
                                                                    <span>Склад</span>
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="filter__group_col">
                                            <div class="mySelect filter__select">
                                                <div class="mySelect__btn" data-empty="Планировка">
                                                    <span class="mySelect__selected">Число комнат</span>
                                                </div>
                                                <div class="mySelect__dropdown">
                                                    <div class="mySelect__wrap">
                                                        <ul>
                                                            <li>
                                                                <label class="mySelect__checkbox">
                                                                    <input type="checkbox" name="filter__type" value="Студия">
                                                                    <span>Студия</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="mySelect__checkbox">
                                                                    <input type="checkbox" name="filter__type" value="1к">
                                                                    <span>1-комнатная</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="mySelect__checkbox">
                                                                    <input type="checkbox" name="filter__type" value="2к">
                                                                    <span>2-комнатная</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="mySelect__checkbox">
                                                                    <input type="checkbox" name="filter__type" value="3к">
                                                                    <span>3-комнатная</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="mySelect__checkbox">
                                                                    <input type="checkbox" name="filter__type" value="4к">
                                                                    <span>4-комнатная</span>
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter__elem">
                                    <div class="filter__price">
                                        <label class="filter__price_label">Цена</label>
                                        <div class="filter__price_control">
                                            <div class="filter__price_start">
                                                <input type="text" class="filter__price_input" name="price-start" placeholder="до">
                                            </div>
                                            <div class="filter__price_end">
                                                <input type="text" class="filter__price_input" name="price-end" placeholder="после">
                                            </div>
                                        </div>
                                        <div class="filter__price_type">
                                            <div class="mySelect">
                                                <div class="mySelect__btn">
                                                    <span class="mySelect__selected">За всё</span>
                                                </div>
                                                <div class="mySelect__dropdown">
                                                    <div class="mySelect__wrap">
                                                        <ul>
                                                            <li>
                                                                <label class="mySelect__radio">
                                                                    <input type="radio" name="filter__deal" value="за всё" checked>
                                                                    <span>За всё</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="mySelect__radio">
                                                                    <input type="radio" name="filter__deal" value="за кв м">
                                                                    <span>За кв. м.</span>
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter__elem">
                                    <label class="form-checkbox">
                                        <input type="checkbox" name="filter-photo">
                                        <span>С фото</span>
                                    </label>
                                </div>

                                <div class="filter__elem filter__elem-place">
                                    <input type="text" class="filter__input" name="filter-place" placeholder="Город, адрес, метро, район или шоссе">
                                </div>

                                <div class="filter__elem">
                                    <a href="#filterModal" class="filter__button btn-modal">Ещё фильтры</a>
                                </div>

                                <div class="filter__elem">
                                    <a href="#filterCity" class="filter__button btn-filter btn-modal">Москва</a>
                                </div>

                                <div class="filter__elem">
                                    <a href="#filterDistrict" class="filter__button btn-filter btn-modal">Район</a>
                                </div>

                                <div class="filter__elem">
                                    <a href="#" class="filter__button"><i class="fas fa-search-plus"></i> Сохранить поиск</a>
                                </div>

                                <div class="filter__elem">
                                    <button type="submit" class="filter__button btn-blue">Показать объекты</button>
                                </div>

                            </div>

                            <div type="filter__row">
                           <span class="filter__params">
                               <span type="filter__params_name">ул. Профсоюзная</span>
                               <span class="filter__params_remove"></span>
                           </span>
                                <span class="filter__params">
                               <span type="filter__params_name">ул. Гончарова</span>
                               <span class="filter__params_remove"></span>
                           </span>
                                <span class="filter__params">
                               <span type="filter__params_name">Центральный р-н</span>
                               <span class="filter__params_remove"></span>
                           </span>
                                <span class="filter__params">
                               <span type="filter__params_name">Южный р-н</span>
                               <span class="filter__params_remove"></span>
                           </span>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <!-- -->

            <!-- Выбор города -->
            <div class="hide">
                <div class="placeModal" id="filterCity">
                    <div class="placeModal__heading">Выбор города</div>
                    <div class="placeModal__body">
                        <form class="form">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Укажите регион, город">
                            </div>
                            <ul class="placeModal__list">
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Москва">
                                        <span>Москва</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Волгоград">
                                        <span>Волгоград</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Тверь">
                                        <span>Тверь</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Новосибирск">
                                        <span>Новосибирск</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Санкт-Петербург">
                                        <span>Санкт-Петербург</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Омск">
                                        <span>Омск</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Томск">
                                        <span>Томск</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Новгород">
                                        <span>Новгород</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Нижний Новгород">
                                        <span>Нижний Новгород</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Екатеринбург">
                                        <span>Екатеринбург</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Волгоград">
                                        <span>Волгоград</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Краснодар">
                                        <span>Краснодар</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Астрахань">
                                        <span>Астрахань</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Саратов">
                                        <span>Саратов</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Ростов на Дону">
                                        <span>Ростов на Дону</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Рязань">
                                        <span>Рязань</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Воронеж">
                                        <span>Воронеж</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Сочи">
                                        <span>Сочи</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Мурманск">
                                        <span>Мурманск</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Псков">
                                        <span>Псков</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Ярославль">
                                        <span>Ярославль</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Владимир">
                                        <span>Владимир</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Самара">
                                        <span>Самара</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Казань">
                                        <span>Казань</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Калуга">
                                        <span>Калуга</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Элиста">
                                        <span>Элиста</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="city" value="Брянск">
                                        <span>Брянск</span>
                                    </label>
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
            <!-- -->

            <!-- Выбор города -->
            <div class="hide">
                <div class="placeModal" id="filterDistrict">
                    <div class="placeModal__heading">Выбор района</div>
                    <div class="placeModal__body">
                        <form class="form">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Укажите округ, район">
                            </div>
                            <ul class="placeModal__list">
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="district" value="Центральный">
                                        <span>Центральный</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="district" value="Северный">
                                        <span>Северный</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="district" value="Северо-Восточный">
                                        <span>Северо-Восточный</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="district" value="Восточный">
                                        <span>Восточный</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="district" value="Юго-Восточный">
                                        <span>Юго-Восточный</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="district" value="Южный">
                                        <span>Южный</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="district" value="Юго-Западный">
                                        <span>Юго-Западный</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="district" value="Западный">
                                        <span>Западный</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="district" value="Северо-Западный">
                                        <span>Северо-Западный</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="district" value="Зеленоградский">
                                        <span>Зеленоградский</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="district" value="Троицкий">
                                        <span>Троицкий</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="form-radio">
                                        <input type="radio" name="district" value="Новомосковский">
                                        <span>Новомосковский</span>
                                    </label>
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
            <!-- -->

            <!-- Расширенный фильтр -->
            <div class="hide">
                <div class="filterModal" id="filterModal">
                    <div class="filterModal__body">

                        <ul class="filterModal__row">
                            <li>Дата публикации объявлений</li>
                            <li>
                                <div class="filterModal__content">
                                    <div class="filterModal__elem">
                                        <div class="mySelect filter__select">
                                            <div class="mySelect__btn">
                                                <span class="mySelect__selected">Сегодня</span>
                                            </div>
                                            <div class="mySelect__dropdown">
                                                <div class="mySelect__wrap">
                                                    <ul>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__deal" value="Сегодня" checked>
                                                                <span>Сегодня</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__deal" value="Неделя">
                                                                <span>Неделя</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__deal" value="Две недели">
                                                                <span>Две недели</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__deal" value="Месяц">
                                                                <span>Месяц</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <ul class="filterModal__row">
                            <li>Видео</li>
                            <li>
                                <div class="filterModal__content">
                                    <div class="filterModal__elem">
                                        <label class="form-checkbox">
                                            <input type="checkbox" name="filter-photo" value="С видео">
                                            <span>С видео</span>
                                        </label>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <ul class="filterModal__row">
                            <li>Площадь, м²</li>
                            <li>
                                <div class="filterModal__content">

                                    <div class="filterModal__elem">
                                        <div class="filterModal__amount">
                                            <label class="filterModal__amount_label">общая</label>
                                            <div class="filterModal__amount_control">
                                                <div class="filterModal__amount_start">
                                                    <input type="text" class="filterModal__amount_input" name="area-start" placeholder="от">
                                                </div>
                                                <div class="filterModal__amount_end">
                                                    <input type="text" class="filterModal__amount_input" name="area-end" placeholder="до">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="filterModal__elem">
                                        <div class="filterModal__amount">
                                            <label class="filterModal__amount_label">кухня</label>
                                            <div class="filterModal__amount_control">
                                                <div class="filterModal__amount_start">
                                                    <input type="text" class="filterModal__amount_input" name="area-one-start" placeholder="от">
                                                </div>
                                                <div class="filterModal__amount_end">
                                                    <input type="text" class="filterModal__amount_input" name="area-one-end" placeholder="до">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="filterModal__elem">
                                        <div class="filterModal__amount">
                                            <label class="filterModal__amount_label">жилая</label>
                                            <div class="filterModal__amount_control">
                                                <div class="filterModal__amount_start">
                                                    <input type="text" class="filterModal__amount_input" name="area-two-start" placeholder="от">
                                                </div>
                                                <div class="filterModal__amount_end">
                                                    <input type="text" class="filterModal__amount_input" name="area-two-end" placeholder="до">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </li>
                        </ul>

                        <ul class="filterModal__row">
                            <li>Этаж</li>
                            <li>
                                <div class="filterModal__content">

                                    <div class="filterModal__elem">
                                        <div class="filterModal__amount">
                                            <div class="filterModal__amount_control">
                                                <div class="filterModal__amount_start">
                                                    <input type="text" class="filterModal__amount_input" name="floor-start" placeholder="от">
                                                </div>
                                                <div class="filterModal__amount_end">
                                                    <input type="text" class="filterModal__amount_input" name="floor-end" placeholder="до">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="filterModal__elem">
                                        <label class="button-checkbox">
                                            <input type="radio" name="filter-floor" value="не первый">
                                            <span>не первый</span>
                                        </label>
                                    </div>

                                    <div class="filterModal__elem">
                                        <label class="button-checkbox">
                                            <input type="radio" name="filter-floor" value="не последний">
                                            <span>не последний</span>
                                        </label>
                                    </div>

                                    <div class="filterModal__elem">
                                        <label class="button-checkbox">
                                            <input type="radio" name="filter-floor" value="только последний">
                                            <span>только последний</span>
                                        </label>
                                    </div>

                                </div>
                            </li>
                        </ul>

                        <ul class="filterModal__row">
                            <li>Этажей в доме</li>
                            <li>
                                <div class="filterModal__content">
                                    <div class="filterModal__elem">
                                        <div class="filterModal__amount">
                                            <div class="filterModal__amount_control">
                                                <div class="filterModal__amount_start">
                                                    <input type="text" class="filterModal__amount_input" name="floor-start" placeholder="от">
                                                </div>
                                                <div class="filterModal__amount_end">
                                                    <input type="text" class="filterModal__amount_input" name="floor-end" placeholder="до">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>


                        <ul class="filterModal__row">
                            <li>Тип дома</li>
                            <li>
                                <div class="filterModal__content">
                                    <div class="filterModal__elem">
                                        <label class="button-checkbox">
                                            <input type="radio" name="filter-floor" value="Кирпичный">
                                            <span>Кирпичный</span>
                                        </label>
                                    </div>
                                    <div class="filterModal__elem">
                                        <label class="button-checkbox">
                                            <input type="radio" name="filter-floor" value="Деревянный">
                                            <span>Деревянный</span>
                                        </label>
                                    </div>
                                    <div class="filterModal__elem">
                                        <label class="button-checkbox">
                                            <input type="radio" name="filter-floor" value="Монолитный">
                                            <span>Монолитный</span>
                                        </label>
                                    </div>
                                    <div class="filterModal__elem">
                                        <label class="button-checkbox">
                                            <input type="radio" name="filter-floor" value="Панельный">
                                            <span>Панельный</span>
                                        </label>
                                    </div>
                                    <div class="filterModal__elem">
                                        <label class="button-checkbox">
                                            <input type="radio" name="filter-floor" value="Блочный">
                                            <span>Блочный</span>
                                        </label>
                                    </div>
                                    <div class="filterModal__elem">
                                        <label class="button-checkbox">
                                            <input type="radio" name="filter-floor" value="Кирпично-монолитный">
                                            <span>Кирпично-монолитный</span>
                                        </label>
                                    </div>
                                    <div class="filterModal__elem">
                                        <label class="button-checkbox">
                                            <input type="radio" name="filter-floor" value="Сталинский">
                                            <span>Сталинский</span>
                                        </label>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <ul class="filterModal__row">
                            <li>Год постройки</li>
                            <li>
                                <div class="filterModal__content">
                                    <div class="filterModal__elem">
                                        <div class="filterModal__amount">
                                            <div class="filterModal__amount_control">
                                                <div class="filterModal__amount_start">
                                                    <input type="text" class="filterModal__amount_input" name="year-start" placeholder="от">
                                                </div>
                                                <div class="filterModal__amount_end">
                                                    <input type="text" class="filterModal__amount_input" name="year-end" placeholder="до">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <ul class="filterModal__row">
                            <li>Высота потолков</li>
                            <li>
                                <div class="filterModal__content">
                                    <div class="filterModal__elem">
                                        <div class="filterModal__amount">
                                            <div class="filterModal__amount_control">
                                                <div class="filterModal__amount_start">
                                                    <input type="text" class="filterModal__amount_input" name="height-start" placeholder="от">
                                                </div>
                                                <div class="filterModal__amount_end">
                                                    <input type="text" class="filterModal__amount_input" name="height-end" placeholder="до">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <ul class="filterModal__row">
                            <li>Балкон</li>
                            <li>
                                <div class="filterModal__content">
                                    <div class="filterModal__elem">

                                        <div class="filterModal__elem">
                                            <label class="button-checkbox">
                                                <input type="checkbox" name="param-one" value="Есть балкон">
                                                <span>Есть балкон</span>
                                            </label>
                                        </div>

                                        <div class="filterModal__elem">
                                            <label class="button-checkbox">
                                                <input type="checkbox" name="param-one" value="Есть лоджия">
                                                <span>Есть лоджия</span>
                                            </label>
                                        </div>

                                    </div>
                                </div>
                            </li>
                        </ul>

                        <ul class="filterModal__row">
                            <li>Ремонт</li>
                            <li>
                                <div class="filterModal__content">

                                    <div class="filterModal__elem">
                                        <label class="button-checkbox">
                                            <input type="checkbox" name="param-two" value="Косметический">
                                            <span>Косметический</span>
                                        </label>
                                    </div>

                                    <div class="filterModal__elem">
                                        <label class="button-checkbox">
                                            <input type="checkbox" name="param-two" value="Евроремонт">
                                            <span>Евроремонт</span>
                                        </label>
                                    </div>

                                    <div class="filterModal__elem">
                                        <label class="button-checkbox">
                                            <input type="checkbox" name="param-two" value="Дизайнерский">
                                            <span>Дизайнерский</span>
                                        </label>
                                    </div>

                                    <div class="filterModal__elem">
                                        <label class="button-checkbox">
                                            <input type="checkbox" name="param-two" value="Без ремонта">
                                            <span>Без ремонта</span>
                                        </label>
                                    </div>

                                </div>
                            </li>
                        </ul>

                        <ul class="filterModal__row">
                            <li>Парковка</li>
                            <li>
                                <div class="filterModal__content">

                                    <div class="filterModal__elem">
                                        <label class="button-checkbox">
                                            <input type="checkbox" name="param-parking" value="Наземная">
                                            <span>Наземная</span>
                                        </label>
                                    </div>

                                    <div class="filterModal__elem">
                                        <label class="button-checkbox">
                                            <input type="checkbox" name="param-parking" value="Многоуровневая">
                                            <span>Многоуровневая</span>
                                        </label>
                                    </div>

                                    <div class="filterModal__elem">
                                        <label class="button-checkbox">
                                            <input type="checkbox" name="param-parking" value="Подземная">
                                            <span>Подземная</span>
                                        </label>
                                    </div>

                                    <div class="filterModal__elem">
                                        <label class="button-checkbox">
                                            <input type="checkbox" name="param-parking" value="На крыше">
                                            <span>На крыше</span>
                                        </label>
                                    </div>

                                </div>
                            </li>
                        </ul>

                    </div>
                    <div class="filterModal__bottom">
                        <button type="submit" class="btn btn-blue btn-circle">Показать результаты</button>
                    </div>
                </div>
            </div>
            <!-- -->

            <section class="main">

                <div class="container">

                    <ul class="breadcrumb">
                        <li><a href="#">Недвижимость в Перми</a></li>
                        <li><span>Продажа</span></li>
                    </ul>

                    <h1>Купить 2-комнатную квартиру в Перми</h1>

                    <div class="totalOffers">
                        <span class="totalOffers__text">8 863 предложения отсортированы</span>
                        <div class="totalOffers__sort">
                            <div class="mySelect">
                                <div class="mySelect__btn">
                                    <span class="mySelect__selected">по умолчанию</span>
                                </div>
                                <div class="mySelect__dropdown">
                                    <div class="mySelect__wrap">
                                        <ul>
                                            <li>
                                                <label class="mySelect__radio">
                                                    <input type="radio" name="sort" value="по умолчанию" checked="">
                                                    <span>по умолчанию</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="mySelect__radio">
                                                    <input type="radio" name="sort" value="По цене">
                                                    <span>По цене</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="mySelect__radio">
                                                    <input type="radio" name="sort" value="По прощади">
                                                    <span>По прощади</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="mySelect__radio">
                                                    <input type="radio" name="sort" value="По улице">
                                                    <span>По улице</span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="wrapper">

                        <div class="offer offer-premium">
                            <div class="offer__media">
                                <div class="offer__media_wrap">
                                    <div class="offer__slider">
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_01.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_02.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_03.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_04.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="offer__content">

                                <div class="offer__info">

                                    <div class="offer__base">
                                        <div class="offer__adv">
                                            <div class="offer__adv_id">ID 13014860</div>
                                            <div class="offer__adv_author"><a href="#">Риэлт Сибирь</a></div>
                                        </div>
                                        <div class="offer__heading">
                                            <div class="offer__heading_name"><a href="#">2-комн. кв., 74 м², 2/10 этаж</a></div>
                                            <div class="offer__heading_house"><a href="#">ЖК «Созвездие»</a></div>
                                        </div>
                                        <div class="offer__cost">
                                            <div class="offer__cost_primary">2 200 000 ₽</div>
                                            <div class="offer__cost_second">41 353 ₽/м²</div>
                                        </div>
                                    </div>

                                    <div class="offer__data">
                                        <div class="offer__data_address"><a href="#">Пермский край</a>, <a href="#">Пермь</a>, <a href="#">р-н Мотовилихинский</a>, <a href="#">улица Грибоедова</a>, 72</div>
                                        <div class="offer__data_intro">Уютная, чистая, светлая квартира. Комнаты изолированные, санузел раздельный, застекленный балкон, окна ПВХ на южную сторону. Входная дверь металлическая, есть телефон, интернет, домофон. Встроенные шкафы в коридоре. На этаже тамбурная дверь (решетка) на 2 квартиры. Лифт. Парковка возле дома.</div>
                                    </div>

                                </div>

                                <div class="offer__bottom">
                                    <a class="offer__phone" href="tel:+73422563059">+7 342 256-30-59</a>
                                    <ul class="offer__action">
                                        <li>
                                            <a href="#" class="offer__view">Подробнее</a>
                                        </li>
                                        <li>
                                            <label class="btn-favorite">
                                                <input type="checkbox">
                                                <i class="fas fa-heart"></i>
                                            </label>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Перезвоните мне">
                                                <i class="fas fa-phone"></i><span>Перезвоните мне</span>
                                            </button>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Посмотреть на карте">
                                                <i class="far fa-map"></i><span>На карте</span>
                                            </button>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Пожаловаться">
                                                <i class="far fa-bell"></i><span>Пожаловаться</span>
                                            </button>
                                        </li>
                                        <li><span class="offer__time">9 часов назад</span></li>
                                    </ul>
                                </div>

                            </div>
                        </div>

                        <div class="offer offer-premium">
                            <div class="offer__media">
                                <div class="offer__media_wrap">
                                    <div class="offer__slider">
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_05.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_06.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_07.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_08.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="offer__content">

                                <div class="offer__info">

                                    <div class="offer__base">
                                        <div class="offer__adv">
                                            <div class="offer__adv_id">ID 13014860</div>
                                            <div class="offer__adv_author"><a href="#">Риэлт Сибирь</a></div>
                                        </div>
                                        <div class="offer__heading">
                                            <div class="offer__heading_name"><a href="#">2-комн. кв., 74 м², 2/10 этаж</a></div>
                                            <div class="offer__heading_house"><a href="#">ЖК «Созвездие»</a></div>
                                        </div>
                                        <div class="offer__cost">
                                            <div class="offer__cost_primary">2 200 000 ₽</div>
                                            <div class="offer__cost_second">41 353 ₽/м²</div>
                                        </div>
                                    </div>

                                    <div class="offer__data">
                                        <div class="offer__data_address"><a href="#">Пермский край</a>, <a href="#">Пермь</a>, <a href="#">р-н Мотовилихинский</a>, <a href="#">улица Грибоедова</a>, 72</div>
                                        <div class="offer__data_intro">Уютная, чистая, светлая квартира. Комнаты изолированные, санузел раздельный, застекленный балкон, окна ПВХ на южную сторону. Входная дверь металлическая, есть телефон, интернет, домофон. Встроенные шкафы в коридоре. На этаже тамбурная дверь (решетка) на 2 квартиры. Лифт. Парковка возле дома.</div>
                                    </div>

                                </div>
                                <div class="offer__bottom">
                                    <a class="offer__phone" href="tel:+73422563059">+7 342 256-30-59</a>
                                    <ul class="offer__action">
                                        <li>
                                            <a href="#" class="offer__view">Подробнее</a>
                                        </li>
                                        <li>
                                            <label class="btn-favorite">
                                                <input type="checkbox">
                                                <i class="fas fa-heart"></i>
                                            </label>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Перезвоните мне">
                                                <i class="fas fa-phone"></i><span>Перезвоните мне</span>
                                            </button>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Посмотреть на карте">
                                                <i class="far fa-map"></i><span>На карте</span>
                                            </button>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Пожаловаться">
                                                <i class="far fa-bell"></i><span>Пожаловаться</span>
                                            </button>
                                        </li>
                                        <li><span class="offer__time">9 часов назад</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="offer">
                            <div class="offer__media">
                                <div class="offer__media_wrap">
                                    <div class="offer__slider">
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_01.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_02.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_03.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_04.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="offer__content">

                                <div class="offer__info">

                                    <div class="offer__base">
                                        <div class="offer__adv">
                                            <div class="offer__adv_id">ID 13014860</div>
                                            <div class="offer__adv_author"><a href="#">Риэлт Сибирь</a></div>
                                        </div>
                                        <div class="offer__heading">
                                            <div class="offer__heading_name"><a href="#">2-комн. кв., 74 м², 2/10 этаж</a></div>
                                            <div class="offer__heading_house"><a href="#">ЖК «Созвездие»</a></div>
                                        </div>
                                        <div class="offer__cost">
                                            <div class="offer__cost_primary">2 200 000 ₽</div>
                                            <div class="offer__cost_second">41 353 ₽/м²</div>
                                        </div>
                                    </div>

                                    <div class="offer__data">
                                        <div class="offer__data_address"><a href="#">Пермский край</a>, <a href="#">Пермь</a>, <a href="#">р-н Мотовилихинский</a>, <a href="#">улица Грибоедова</a>, 72</div>
                                        <div class="offer__data_intro">Уютная, чистая, светлая квартира. Комнаты изолированные, санузел раздельный, застекленный балкон, окна ПВХ на южную сторону. Входная дверь металлическая, есть телефон, интернет, домофон. Встроенные шкафы в коридоре. На этаже тамбурная дверь (решетка) на 2 квартиры. Лифт. Парковка возле дома.</div>
                                    </div>

                                </div>

                                <div class="offer__bottom">
                                    <a class="offer__phone" href="tel:+73422563059">+7 342 256-30-59</a>
                                    <ul class="offer__action">
                                        <li>
                                            <a href="#" class="offer__view">Подробнее</a>
                                        </li>
                                        <li>
                                            <label class="btn-favorite">
                                                <input type="checkbox">
                                                <i class="fas fa-heart"></i>
                                            </label>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Перезвоните мне">
                                                <i class="fas fa-phone"></i><span>Перезвоните мне</span>
                                            </button>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Посмотреть на карте">
                                                <i class="far fa-map"></i><span>На карте</span>
                                            </button>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Пожаловаться">
                                                <i class="far fa-bell"></i><span>Пожаловаться</span>
                                            </button>
                                        </li>
                                        <li><span class="offer__time">9 часов назад</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="offer">
                            <div class="offer__media">
                                <div class="offer__media_wrap">
                                    <div class="offer__slider">
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_01.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_02.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_03.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_04.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="offer__content">

                                <div class="offer__info">

                                    <div class="offer__base">
                                        <div class="offer__adv">
                                            <div class="offer__adv_id">ID 13014860</div>
                                            <div class="offer__adv_author"><a href="#">Риэлт Сибирь</a></div>
                                        </div>
                                        <div class="offer__heading">
                                            <div class="offer__heading_name"><a href="#">2-комн. кв., 74 м², 2/10 этаж</a></div>
                                            <div class="offer__heading_house"><a href="#">ЖК «Созвездие»</a></div>
                                        </div>
                                        <div class="offer__cost">
                                            <div class="offer__cost_primary">2 200 000 ₽</div>
                                            <div class="offer__cost_second">41 353 ₽/м²</div>
                                        </div>
                                    </div>

                                    <div class="offer__data">
                                        <div class="offer__data_address"><a href="#">Пермский край</a>, <a href="#">Пермь</a>, <a href="#">р-н Мотовилихинский</a>, <a href="#">улица Грибоедова</a>, 72</div>
                                        <div class="offer__data_intro">Уютная, чистая, светлая квартира. Комнаты изолированные, санузел раздельный, застекленный балкон, окна ПВХ на южную сторону. Входная дверь металлическая, есть телефон, интернет, домофон. Встроенные шкафы в коридоре. На этаже тамбурная дверь (решетка) на 2 квартиры. Лифт. Парковка возле дома.</div>
                                    </div>

                                </div>

                                <div class="offer__bottom">
                                    <a class="offer__phone" href="tel:+73422563059">+7 342 256-30-59</a>
                                    <ul class="offer__action">
                                        <li>
                                            <a href="#" class="offer__view">Подробнее</a>
                                        </li>
                                        <li>
                                            <label class="btn-favorite">
                                                <input type="checkbox">
                                                <i class="fas fa-heart"></i>
                                            </label>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Перезвоните мне">
                                                <i class="fas fa-phone"></i><span>Перезвоните мне</span>
                                            </button>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Посмотреть на карте">
                                                <i class="far fa-map"></i><span>На карте</span>
                                            </button>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Пожаловаться">
                                                <i class="far fa-bell"></i><span>Пожаловаться</span>
                                            </button>
                                        </li>
                                        <li><span class="offer__time">9 часов назад</span></li>
                                    </ul>
                                </div>

                            </div>
                        </div>

                        <div class="offer">
                            <div class="offer__media">
                                <div class="offer__media_wrap">
                                    <div class="offer__slider">
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_05.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_06.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_07.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_08.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="offer__content">

                                <div class="offer__info">

                                    <div class="offer__base">
                                        <div class="offer__adv">
                                            <div class="offer__adv_id">ID 13014860</div>
                                            <div class="offer__adv_author"><a href="#">Риэлт Сибирь</a></div>
                                        </div>
                                        <div class="offer__heading">
                                            <div class="offer__heading_name"><a href="#">2-комн. кв., 74 м², 2/10 этаж</a></div>
                                            <div class="offer__heading_house"><a href="#">ЖК «Созвездие»</a></div>
                                        </div>
                                        <div class="offer__cost">
                                            <div class="offer__cost_primary">2 200 000 ₽</div>
                                            <div class="offer__cost_second">41 353 ₽/м²</div>
                                        </div>
                                    </div>

                                    <div class="offer__data">
                                        <div class="offer__data_address"><a href="#">Пермский край</a>, <a href="#">Пермь</a>, <a href="#">р-н Мотовилихинский</a>, <a href="#">улица Грибоедова</a>, 72</div>
                                        <div class="offer__data_intro">Уютная, чистая, светлая квартира. Комнаты изолированные, санузел раздельный, застекленный балкон, окна ПВХ на южную сторону. Входная дверь металлическая, есть телефон, интернет, домофон. Встроенные шкафы в коридоре. На этаже тамбурная дверь (решетка) на 2 квартиры. Лифт. Парковка возле дома.</div>
                                    </div>

                                </div>
                                <div class="offer__bottom">
                                    <a class="offer__phone" href="tel:+73422563059">+7 342 256-30-59</a>
                                    <ul class="offer__action">
                                        <li>
                                            <a href="#" class="offer__view">Подробнее</a>
                                        </li>
                                        <li>
                                            <label class="btn-favorite">
                                                <input type="checkbox">
                                                <i class="fas fa-heart"></i>
                                            </label>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Перезвоните мне">
                                                <i class="fas fa-phone"></i><span>Перезвоните мне</span>
                                            </button>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Посмотреть на карте">
                                                <i class="far fa-map"></i><span>На карте</span>
                                            </button>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Пожаловаться">
                                                <i class="far fa-bell"></i><span>Пожаловаться</span>
                                            </button>
                                        </li>
                                        <li><span class="offer__time">9 часов назад</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="offer">
                            <div class="offer__media">
                                <div class="offer__media_wrap">
                                    <div class="offer__slider">
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_01.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_02.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_03.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_04.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="offer__content">

                                <div class="offer__info">

                                    <div class="offer__base">
                                        <div class="offer__adv">
                                            <div class="offer__adv_id">ID 13014860</div>
                                            <div class="offer__adv_author"><a href="#">Риэлт Сибирь</a></div>
                                        </div>
                                        <div class="offer__heading">
                                            <div class="offer__heading_name"><a href="#">2-комн. кв., 74 м², 2/10 этаж</a></div>
                                            <div class="offer__heading_house"><a href="#">ЖК «Созвездие»</a></div>
                                        </div>
                                        <div class="offer__cost">
                                            <div class="offer__cost_primary">2 200 000 ₽</div>
                                            <div class="offer__cost_second">41 353 ₽/м²</div>
                                        </div>
                                    </div>

                                    <div class="offer__data">
                                        <div class="offer__data_address"><a href="#">Пермский край</a>, <a href="#">Пермь</a>, <a href="#">р-н Мотовилихинский</a>, <a href="#">улица Грибоедова</a>, 72</div>
                                        <div class="offer__data_intro">Уютная, чистая, светлая квартира. Комнаты изолированные, санузел раздельный, застекленный балкон, окна ПВХ на южную сторону. Входная дверь металлическая, есть телефон, интернет, домофон. Встроенные шкафы в коридоре. На этаже тамбурная дверь (решетка) на 2 квартиры. Лифт. Парковка возле дома.</div>
                                    </div>

                                </div>

                                <div class="offer__bottom">
                                    <a class="offer__phone" href="tel:+73422563059">+7 342 256-30-59</a>
                                    <ul class="offer__action">
                                        <li>
                                            <a href="#" class="offer__view">Подробнее</a>
                                        </li>
                                        <li>
                                            <label class="btn-favorite">
                                                <input type="checkbox">
                                                <i class="fas fa-heart"></i>
                                            </label>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Перезвоните мне">
                                                <i class="fas fa-phone"></i><span>Перезвоните мне</span>
                                            </button>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Посмотреть на карте">
                                                <i class="far fa-map"></i><span>На карте</span>
                                            </button>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Пожаловаться">
                                                <i class="far fa-bell"></i><span>Пожаловаться</span>
                                            </button>
                                        </li>
                                        <li><span class="offer__time">9 часов назад</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="offer">
                            <div class="offer__media">
                                <div class="offer__media_wrap">
                                    <div class="offer__slider">
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_01.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_02.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_03.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_04.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="offer__content">

                                <div class="offer__info">

                                    <div class="offer__base">
                                        <div class="offer__adv">
                                            <div class="offer__adv_id">ID 13014860</div>
                                            <div class="offer__adv_author"><a href="#">Риэлт Сибирь</a></div>
                                        </div>
                                        <div class="offer__heading">
                                            <div class="offer__heading_name"><a href="#">2-комн. кв., 74 м², 2/10 этаж</a></div>
                                            <div class="offer__heading_house"><a href="#">ЖК «Созвездие»</a></div>
                                        </div>
                                        <div class="offer__cost">
                                            <div class="offer__cost_primary">2 200 000 ₽</div>
                                            <div class="offer__cost_second">41 353 ₽/м²</div>
                                        </div>
                                    </div>

                                    <div class="offer__data">
                                        <div class="offer__data_address"><a href="#">Пермский край</a>, <a href="#">Пермь</a>, <a href="#">р-н Мотовилихинский</a>, <a href="#">улица Грибоедова</a>, 72</div>
                                        <div class="offer__data_intro">Уютная, чистая, светлая квартира. Комнаты изолированные, санузел раздельный, застекленный балкон, окна ПВХ на южную сторону. Входная дверь металлическая, есть телефон, интернет, домофон. Встроенные шкафы в коридоре. На этаже тамбурная дверь (решетка) на 2 квартиры. Лифт. Парковка возле дома.</div>
                                    </div>

                                </div>

                                <div class="offer__bottom">
                                    <a class="offer__phone" href="tel:+73422563059">+7 342 256-30-59</a>
                                    <ul class="offer__action">
                                        <li>
                                            <a href="#" class="offer__view">Подробнее</a>
                                        </li>
                                        <li>
                                            <label class="btn-favorite">
                                                <input type="checkbox">
                                                <i class="fas fa-heart"></i>
                                            </label>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Перезвоните мне">
                                                <i class="fas fa-phone"></i><span>Перезвоните мне</span>
                                            </button>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Посмотреть на карте">
                                                <i class="far fa-map"></i><span>На карте</span>
                                            </button>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Пожаловаться">
                                                <i class="far fa-bell"></i><span>Пожаловаться</span>
                                            </button>
                                        </li>
                                        <li><span class="offer__time">9 часов назад</span></li>
                                    </ul>
                                </div>

                            </div>
                        </div>

                        <div class="offer">
                            <div class="offer__media">
                                <div class="offer__media_wrap">
                                    <div class="offer__slider">
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_05.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_06.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_07.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_08.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="offer__content">

                                <div class="offer__info">

                                    <div class="offer__base">
                                        <div class="offer__adv">
                                            <div class="offer__adv_id">ID 13014860</div>
                                            <div class="offer__adv_author"><a href="#">Риэлт Сибирь</a></div>
                                        </div>
                                        <div class="offer__heading">
                                            <div class="offer__heading_name"><a href="#">2-комн. кв., 74 м², 2/10 этаж</a></div>
                                            <div class="offer__heading_house"><a href="#">ЖК «Созвездие»</a></div>
                                        </div>
                                        <div class="offer__cost">
                                            <div class="offer__cost_primary">2 200 000 ₽</div>
                                            <div class="offer__cost_second">41 353 ₽/м²</div>
                                        </div>
                                    </div>

                                    <div class="offer__data">
                                        <div class="offer__data_address"><a href="#">Пермский край</a>, <a href="#">Пермь</a>, <a href="#">р-н Мотовилихинский</a>, <a href="#">улица Грибоедова</a>, 72</div>
                                        <div class="offer__data_intro">Уютная, чистая, светлая квартира. Комнаты изолированные, санузел раздельный, застекленный балкон, окна ПВХ на южную сторону. Входная дверь металлическая, есть телефон, интернет, домофон. Встроенные шкафы в коридоре. На этаже тамбурная дверь (решетка) на 2 квартиры. Лифт. Парковка возле дома.</div>
                                    </div>

                                </div>
                                <div class="offer__bottom">
                                    <a class="offer__phone" href="tel:+73422563059">+7 342 256-30-59</a>
                                    <ul class="offer__action">
                                        <li>
                                            <a href="#" class="offer__view">Подробнее</a>
                                        </li>
                                        <li>
                                            <label class="btn-favorite">
                                                <input type="checkbox">
                                                <i class="fas fa-heart"></i>
                                            </label>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Перезвоните мне">
                                                <i class="fas fa-phone"></i><span>Перезвоните мне</span>
                                            </button>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Посмотреть на карте">
                                                <i class="far fa-map"></i><span>На карте</span>
                                            </button>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Пожаловаться">
                                                <i class="far fa-bell"></i><span>Пожаловаться</span>
                                            </button>
                                        </li>
                                        <li><span class="offer__time">9 часов назад</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="offer">
                            <div class="offer__media">
                                <div class="offer__media_wrap">
                                    <div class="offer__slider">
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_01.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_02.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_03.jpg" alt="">
                                        </div>
                                        <div class="offer__slider_item">
                                            <img src="images/apartment_04.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="offer__content">

                                <div class="offer__info">

                                    <div class="offer__base">
                                        <div class="offer__adv">
                                            <div class="offer__adv_id">ID 13014860</div>
                                            <div class="offer__adv_author"><a href="#">Риэлт Сибирь</a></div>
                                        </div>
                                        <div class="offer__heading">
                                            <div class="offer__heading_name"><a href="#">2-комн. кв., 74 м², 2/10 этаж</a></div>
                                            <div class="offer__heading_house"><a href="#">ЖК «Созвездие»</a></div>
                                        </div>
                                        <div class="offer__cost">
                                            <div class="offer__cost_primary">2 200 000 ₽</div>
                                            <div class="offer__cost_second">41 353 ₽/м²</div>
                                        </div>
                                    </div>

                                    <div class="offer__data">
                                        <div class="offer__data_address"><a href="#">Пермский край</a>, <a href="#">Пермь</a>, <a href="#">р-н Мотовилихинский</a>, <a href="#">улица Грибоедова</a>, 72</div>
                                        <div class="offer__data_intro">Уютная, чистая, светлая квартира. Комнаты изолированные, санузел раздельный, застекленный балкон, окна ПВХ на южную сторону. Входная дверь металлическая, есть телефон, интернет, домофон. Встроенные шкафы в коридоре. На этаже тамбурная дверь (решетка) на 2 квартиры. Лифт. Парковка возле дома.</div>
                                    </div>

                                </div>

                                <div class="offer__bottom">
                                    <a class="offer__phone" href="tel:+73422563059">+7 342 256-30-59</a>
                                    <ul class="offer__action">
                                        <li>
                                            <a href="#" class="offer__view">Подробнее</a>
                                        </li>
                                        <li>
                                            <label class="btn-favorite">
                                                <input type="checkbox">
                                                <i class="fas fa-heart"></i>
                                            </label>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Перезвоните мне">
                                                <i class="fas fa-phone"></i><span>Перезвоните мне</span>
                                            </button>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Посмотреть на карте">
                                                <i class="far fa-map"></i><span>На карте</span>
                                            </button>
                                        </li>
                                        <li>
                                            <button class="btn-action" title="Пожаловаться">
                                                <i class="far fa-bell"></i><span>Пожаловаться</span>
                                            </button>
                                        </li>
                                        <li><span class="offer__time">9 часов назад</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <ul class="pagination">
                        <li><span>1</span></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">6</a></li>
                        <li><a href="#">7</a></li>
                        <li><a href="#" class="next">...</a></li>
                    </ul>

                </div>

            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->



        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->



    </body>
</html>
