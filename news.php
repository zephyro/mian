<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<head>
    <?php include('inc/head.inc.php') ?>
</head>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header.inc.php') ?>
    <!-- -->

    <section class="main main-white">

        <div class="container">

            <ul class="breadcrumb">
                <li><a href="#">Недвижимость в Перми</a></li>
                <li><span>Новости</span></li>
            </ul>

            <div class="main-row">
                <div class="main-left">
                    <div class="newsList">

                        <div class="newsTitle">
                            <h1>Новости</h1>
                            <ul class="newsTitle__link">
                                <li class="active"><a href="#">Новое</a></li>
                                <li><a href="#">Популярное</a></li>
                                <li><a href="#">Обсуждаемое</a></li>
                            </ul>
                        </div>

                        <article class="newsItem">
                            <a href="#" class="newsItem__image">
                                <img src="images/news__01.jpg" class="img-fluid" alt="">
                            </a>
                            <div class="newsItem__content">
                                <a href="#" class="newsItem__title">В сегменте ипотеки выросло число «хороших» заемщиков</a>
                                <a href="#" class="newsItem__introtext">Благодаря снижению ставок в прошлом году реализовался отложенный спрос на ипотечные займы</a>
                                <div class="newsItem__tags">
                                    <span>22 марта 2018</span>
                                    <span><i class="fas fa-eye"></i> 181</span>
                                </div>
                            </div>
                        </article>

                        <article class="newsItem">
                            <a href="#" class="newsItem__image">
                                <img src="images/news__02.jpg" class="img-fluid" alt="">
                            </a>
                            <div class="newsItem__content">
                                <a href="#" class="newsItem__title">Герман Греф анонсировал новые ипотечные рекорды</a>
                                <a href="#" class="newsItem__introtext">тавка, по его мнению, продолжит снижаться</a>
                                <div class="newsItem__tags">
                                    <span>22 марта 2018</span>
                                    <span><i class="fas fa-eye"></i> 181</span>
                                </div>
                            </div>
                        </article>

                        <article class="newsItem">
                            <a href="#" class="newsItem__image">
                                <img src="images/news_03.jpg" class="img-fluid" alt="">
                            </a>
                            <div class="newsItem__content">
                                <a href="#" class="newsItem__title">Жильцы смогут заработать на аренде крыш и чердаков многоквартирных домов</a>
                                <a href="#" class="newsItem__introtext">Часто собственники даже не знают о том, что провайдеры вносят оплату за пользование общедомовым имуществом</a>
                                <div class="newsItem__tags">
                                    <span>22 марта 2018</span>
                                    <span><i class="fas fa-eye"></i> 181</span>
                                </div>
                            </div>
                        </article>

                        <article class="newsItem">
                            <a href="#" class="newsItem__image">
                                <img src="images/news__01.jpg" class="img-fluid" alt="">
                            </a>
                            <div class="newsItem__content">
                                <a href="#" class="newsItem__title">В сегменте ипотеки выросло число «хороших» заемщиков</a>
                                <a href="#" class="newsItem__introtext">Благодаря снижению ставок в прошлом году реализовался отложенный спрос на ипотечные займы</a>
                                <div class="newsItem__tags">
                                    <span>22 марта 2018</span>
                                    <span><i class="fas fa-eye"></i> 181</span>
                                </div>
                            </div>
                        </article>

                        <article class="newsItem">
                            <a href="#" class="newsItem__image">
                                <img src="images/news__02.jpg" class="img-fluid" alt="">
                            </a>
                            <div class="newsItem__content">
                                <a href="#" class="newsItem__title">Герман Греф анонсировал новые ипотечные рекорды</a>
                                <a href="#" class="newsItem__introtext">тавка, по его мнению, продолжит снижаться</a>
                                <div class="newsItem__tags">
                                    <span>22 марта 2018</span>
                                    <span><i class="fas fa-eye"></i> 181</span>
                                </div>
                            </div>
                        </article>

                        <article class="newsItem">
                            <a href="#" class="newsItem__image">
                                <img src="images/news_03.jpg" class="img-fluid" alt="">
                            </a>
                            <div class="newsItem__content">
                                <a href="#" class="newsItem__title">Жильцы смогут заработать на аренде крыш и чердаков многоквартирных домов</a>
                                <a href="#" class="newsItem__introtext">Часто собственники даже не знают о том, что провайдеры вносят оплату за пользование общедомовым имуществом</a>
                                <div class="newsItem__tags">
                                    <span>22 марта 2018</span>
                                    <span><i class="fas fa-eye"></i> 181</span>
                                </div>
                            </div>
                        </article>

                        <article class="newsItem">
                            <a href="#" class="newsItem__image">
                                <img src="images/news__01.jpg" class="img-fluid" alt="">
                            </a>
                            <div class="newsItem__content">
                                <a href="#" class="newsItem__title">В сегменте ипотеки выросло число «хороших» заемщиков</a>
                                <a href="#" class="newsItem__introtext">Благодаря снижению ставок в прошлом году реализовался отложенный спрос на ипотечные займы</a>
                                <div class="newsItem__tags">
                                    <span>22 марта 2018</span>
                                    <span><i class="fas fa-eye"></i> 181</span>
                                </div>
                            </div>
                        </article>

                        <article class="newsItem">
                            <a href="#" class="newsItem__image">
                                <img src="images/news__02.jpg" class="img-fluid" alt="">
                            </a>
                            <div class="newsItem__content">
                                <a href="#" class="newsItem__title">Герман Греф анонсировал новые ипотечные рекорды</a>
                                <a href="#" class="newsItem__introtext">тавка, по его мнению, продолжит снижаться</a>
                                <div class="newsItem__tags">
                                    <span>22 марта 2018</span>
                                    <span><i class="fas fa-eye"></i> 181</span>
                                </div>
                            </div>
                        </article>

                        <article class="newsItem">
                            <a href="#" class="newsItem__image">
                                <img src="images/news_03.jpg" class="img-fluid" alt="">
                            </a>
                            <div class="newsItem__content">
                                <a href="#" class="newsItem__title">Жильцы смогут заработать на аренде крыш и чердаков многоквартирных домов</a>
                                <a href="#" class="newsItem__introtext">Часто собственники даже не знают о том, что провайдеры вносят оплату за пользование общедомовым имуществом</a>
                                <div class="newsItem__tags">
                                    <span>22 марта 2018</span>
                                    <span><i class="fas fa-eye"></i> 181</span>
                                </div>
                            </div>
                        </article>

                    </div>

                    <ul class="pagination">
                        <li><span>1</span></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                    </ul>
                </div>

                <div class="main-right">

                    <aside class="sidebar">
                        <div class="sidebar__title">Новое на сайте</div>

                        <div class="sidebar__elem">
                            <a href="#" class="sidebar__elem_title">Когда начинает течь минимальный срок владения ИЖдомом в случае его реконструкции?</a>
                            <div class="sidebar__elem_text">Дарение и продажа в одном периоде Итак, при получении квартиры в подарок и ее последующей продаже двойного налогообложения не происходит....</div>
                            <div class="sidebar__elem_time">сегодня в 1:24</div>
                        </div>

                        <div class="sidebar__elem">
                            <a href="#" class="sidebar__elem_title">Когда начинает течь минимальный срок владения ИЖдомом в случае его реконструкции?</a>
                            <div class="sidebar__elem_text">Дарение и продажа в одном периоде Итак, при получении квартиры в подарок и ее последующей продаже двойного налогообложения не происходит....</div>
                            <div class="sidebar__elem_time">сегодня в 1:24</div>
                        </div>

                        <div class="sidebar__elem">
                            <a href="#" class="sidebar__elem_title">Когда начинает течь минимальный срок владения ИЖдомом в случае его реконструкции?</a>
                            <div class="sidebar__elem_text">Дарение и продажа в одном периоде Итак, при получении квартиры в подарок и ее последующей продаже двойного налогообложения не происходит....</div>
                            <div class="sidebar__elem_time">сегодня в 1:24</div>
                        </div>

                        <div class="sidebar__elem">
                            <a href="#" class="sidebar__elem_title">Когда начинает течь минимальный срок владения ИЖдомом в случае его реконструкции?</a>
                            <div class="sidebar__elem_text">Дарение и продажа в одном периоде Итак, при получении квартиры в подарок и ее последующей продаже двойного налогообложения не происходит....</div>
                            <div class="sidebar__elem_time">сегодня в 1:24</div>
                        </div>

                    </aside>

                    <div class="sideSubscribe">
                        <div class="sideSubscribe__title">Подпишитесь на рассылку</div>
                        <div class="sideSubscribe__form">
                            <input type="text" class="sideSubscribe__input" name="email" placeholder="Ваш email">
                            <button type="submit" class="sideSubscribe__btn"><i class="far fa-envelope"></i></button>
                        </div>
                    </div>

                </div>
            </div>



        </div>

    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Modal -->
<?php include('inc/modal.inc.php') ?>
<!-- -->



<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->



</body>
</html>
