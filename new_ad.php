]<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <!-- -->

    <body>

    <div class="page">

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <section class="main">

            <div class="container">

                <div class="new__heading">
                    <h1>Новое объявление</h1>
                    <p>После публикации, ваше объявление будет доступно на сайте</p>
                </div>


                <div class="new">
                    <div class="new__body">

                        <div class="segment" id="segment1">
                            <div class="segment__title">Тип объявления</div>

                            <!-- Тип аккаунта -->
                            <div class="segment__row">
                                <div class="segment__label">Тип аккаунта</div>
                                <div class="segment__elem">
                                    <div class="selectBox">
                                        <label class="selectBox__item">
                                            <input type="radio" name="newAccount" value="Собственник">
                                            <span>Собственник</span>
                                        </label>
                                        <label class="selectBox__item">
                                            <input type="radio" name="newAccount" value="Агент">
                                            <span>Агент</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <!-- Тип сделки -->
                            <div class="segment__row">
                                <div class="segment__label">Тип сделки</div>
                                <div class="segment__elem">
                                    <div class="selectBox">
                                        <label class="selectBox__item">
                                            <input type="radio" name="newAccount" value="Продажа">
                                            <span>Продажа</span>
                                        </label>
                                        <label class="selectBox__item">
                                            <input type="radio" name="newAccount" value="Аренда">
                                            <span>Аренда</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <!-- Тип аренды -->
                            <div class="segment__row">
                                <div class="segment__label">Тип аренды</div>
                                <div class="segment__elem">
                                    <div class="selectBox">
                                        <label class="selectBox__item">
                                            <input type="radio" name="newRentType" value="Длительно">
                                            <span>Длительно</span>
                                        </label>
                                        <label class="selectBox__item">
                                            <input type="radio" name="newRentType" value="Посуточно">
                                            <span>Посуточно</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <!-- Тип недвижимости -->
                            <div class="segment__row">
                                <div class="segment__label">Тип недвижимости</div>
                                <div class="segment__elem">
                                    <div class="selectBox">
                                        <label class="selectBox__item">
                                            <input type="radio" name="newRealtyType" value="Жилая">
                                            <span>Жилая</span>
                                        </label>
                                        <label class="selectBox__item">
                                            <input type="radio" name="newRealtyType" value="Коммерческая">
                                            <span>Коммерческая</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <!-- Объект -->
                            <div class="segment__row">
                                <div class="segment__label">Объект</div>
                                <div class="segment__elem">

                                    <ul class="segment__list">
                                        <li>
                                            <label class="form-radio">
                                                <input type="radio" name="newObject" value="Квартира">
                                                <span>Квартира</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="form-radio">
                                                <input type="radio" name="newObject" value="Дом\Дача">
                                                <span>Дом\Дача</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="form-radio">
                                                <input type="radio" name="newObject" value="Комната">
                                                <span>Комната</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="form-radio">
                                                <input type="radio" name="newObject" value="Коттедж">
                                                <span>Коттедж</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="form-radio">
                                                <input type="radio" name="newObject" value="Койко-место">
                                                <span>Койко-место</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="form-radio">
                                                <input type="radio" name="newObject" value="Таунхаус">
                                                <span>Таунхаус</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="form-radio">
                                                <input type="radio" name="newObject" value="Часть дома">
                                                <span>Часть дома</span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="segment__help">Для добавления объявления необходимо выбрать тип сделки (Аренда или Продажа), тип недвижимости (Жилая или Коммерческая), а также соответствующий тип объекта (квартира, гараж, дом, участок и т.д.). После выбора каждого из типов на форме появятся поля для заполнения.</div>
                        </div>

                        <div class="segment" id="segment2">
                            <div class="segment__title">Адрес</div>
                            <div class="form-group">
                                <div class="inputBox">
                                    <input type="text" class="form-control" id="suggest" name="newAddress" placeholder="Введите адрес: Город, улицу, дом">
                                    <button type="submit" class="inputBox__button" id="button"><i class="fas fa-search"></i></button>
                                </div>
                                <br/>
                                <p id="notice">Адрес не найден</p>
                            </div>

                            <div class="new__map">
                                <div id="map"></div>

                            </div>
                            <div id="footer">
                                <div id="message"></div>
                            </div>

                        </div>

                        <div class="segment" id="segment3">
                            <div class="segment__title">Об объекте</div>

                            <div class="segment__row">
                                <div class="segment__label">Число комнат</div>
                                <div class="segment__elem">
                                    <div class="mySelect">
                                        <div class="mySelect__btn">
                                            <span class="mySelect__selected">Не выбрано</span>
                                        </div>
                                        <div class="mySelect__dropdown">
                                            <div class="mySelect__wrap">
                                                <ul>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newAddress" value="1">
                                                            <span>1</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newAddress" value="2">
                                                            <span>2</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newAddress" value="3">
                                                            <span>3</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newAddress" value="4">
                                                            <span>4</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newAddress" value="5 и более">
                                                            <span>5 и более</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newAddress" value="Студия">
                                                            <span>Студия</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newAddress" value="Свободная планировка">
                                                            <span>Свободная планировка</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="segment__row">
                                <div class="segment__label">Общая площадь</div>
                                <div class="segment__elem">
                                    <input type="text" class="form-control form-control-sm" name="newAreaFull" value="" placeholder="">
                                </div>
                            </div>

                            <div class="segment__row">
                                <div class="segment__label">Зтаж</div>
                                <div class="segment__elem">
                                    <input type="text" class="form-control form-control-xs" name="newFloor" value="" placeholder="">
                                </div>
                            </div>

                            <div class="segment__row">
                                <div class="segment__label">Площадь комнат</div>
                                <div class="segment__elem">
                                    <input type="text" class="form-control form-control-sm" name="newRoomArea" value="" placeholder="Пример: 18+14-10">
                                </div>
                            </div>

                            <div class="segment__row">
                                <div class="segment__label">Жилая площадь</div>
                                <div class="segment__elem">
                                    <input type="text" class="form-control form-control-sm" name="newLivingRoom" value="" placeholder="">
                                </div>
                            </div>

                            <div class="segment__row">
                                <div class="segment__label">Кухня</div>
                                <div class="segment__elem">
                                    <input type="text" class="form-control form-control-sm" name="newКitchenКщщь" value="" placeholder="">
                                </div>
                            </div>

                            <div class="segment__row">
                                <div class="segment__label">Лоджия</div>
                                <div class="segment__elem">
                                    <div class="mySelect">
                                        <div class="mySelect__btn">
                                            <span class="mySelect__selected">Нет</span>
                                        </div>
                                        <div class="mySelect__dropdown">
                                            <div class="mySelect__wrap">
                                                <ul>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newLoggia" value="нет" checked>
                                                            <span>нет</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newLoggia" value="1">
                                                            <span>1</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newLoggia" value="2">
                                                            <span>2</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newLoggia" value="3">
                                                            <span>3</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newLoggia" value="4">
                                                            <span>4</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="segment__row">
                                <div class="segment__label">Балкон</div>
                                <div class="segment__elem">
                                    <div class="mySelect">
                                        <div class="mySelect__btn">
                                            <span class="mySelect__selected">Нет</span>
                                        </div>
                                        <div class="mySelect__dropdown">
                                            <div class="mySelect__wrap">
                                                <ul>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newBalcony" value="нет" checked>
                                                            <span>нет</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newBalcony" value="1">
                                                            <span>1</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newBalcony" value="2">
                                                            <span>2</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newBalcony" value="3">
                                                            <span>3</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newBalcony" value="4">
                                                            <span>4</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="segment__row">
                                <div class="segment__label">Раздельные санузлы</div>
                                <div class="segment__elem">
                                    <div class="segment__elem">
                                        <div class="mySelect">
                                            <div class="mySelect__btn">
                                                <span class="mySelect__selected">Нет</span>
                                            </div>
                                            <div class="mySelect__dropdown">
                                                <div class="mySelect__wrap">
                                                    <ul>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="newBalcony" value="нет" checked>
                                                                <span>нет</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="newBalcony" value="1">
                                                                <span>1</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="newBalcony" value="2">
                                                                <span>2</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="newBalcony" value="3">
                                                                <span>3</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="newBalcony" value="4">
                                                                <span>4</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="segment__row">
                                <div class="segment__label">Совмещенные санузлы</div>
                                <div class="segment__elem">
                                    <div class="segment__elem">
                                        <div class="mySelect">
                                            <div class="mySelect__btn">
                                                <span class="mySelect__selected">Нет</span>
                                            </div>
                                            <div class="mySelect__dropdown">
                                                <div class="mySelect__wrap">
                                                    <ul>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="newCombinedBathroom" value="нет" checked>
                                                                <span>нет</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="newCombinedBathroom" value="1">
                                                                <span>1</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="newCombinedBathroom" value="2">
                                                                <span>2</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="newCombinedBathroom" value="3">
                                                                <span>3</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="newCombinedBathroom" value="4">
                                                                <span>4</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="segment__row">
                                <div class="segment__label">Телефон</div>
                                <div class="segment__elem">
                                    <div class="segment__elem">
                                        <div class="selectBox">
                                            <label class="selectBox__item">
                                                <input type="radio" name="newPhoneCommunication" value="Нет">
                                                <span>Нет</span>
                                            </label>
                                            <label class="selectBox__item">
                                                <input type="radio" name="newPhoneCommunication" value="Да">
                                                <span>Да</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="segment__row">
                                <div class="segment__label"></div>
                                <div class="segment__elem"></div>
                            </div>

                        </div>

                        <div class="segment">
                            <div class="segment__title">О здании</div>

                            <div class="segment__row">
                                <div class="segment__label">Год постройки</div>
                                <div class="segment__elem">
                                    <input type="text" class="form-control form-control-sm" name="newBuildingYear" value="" placeholder="">
                                </div>
                            </div>

                            <div class="segment__row">
                                <div class="segment__label">Высота потолков</div>
                                <div class="segment__elem">
                                    <input type="text" class="form-control form-control-sm" name="newBuildingHeight" value="" placeholder="">
                                </div>
                            </div>

                            <div class="segment__row">
                                <div class="segment__label">Тип или серия дома</div>
                                <div class="segment__elem">
                                    <div class="mySelect">
                                        <div class="mySelect__btn">
                                            <span class="mySelect__selected">Не выбрано</span>
                                        </div>
                                        <div class="mySelect__dropdown">
                                            <div class="mySelect__wrap">
                                                <ul>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newBuildingType" value="Кирпичный">
                                                            <span>Кирпичный</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newBuildingType" value="Монолитный">
                                                            <span>Монолитный</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newBuildingType" value="Панельный">
                                                            <span>Панельный</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newBuildingType" value="Блочный">
                                                            <span>Блочный</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newBuildingType" value="Деревянный">
                                                            <span>Деревянный</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newBuildingType" value="Монолитно-кирпичный">
                                                            <span>Монолитно-кирпичный</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newBuildingType" value="Сталинский">
                                                            <span>Сталинский</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="segment__row">
                                <div class="segment__label">Парковка</div>
                                <div class="segment__elem">
                                    <div class="mySelect">
                                        <div class="mySelect__btn">
                                            <span class="mySelect__selected">Нет</span>
                                        </div>
                                        <div class="mySelect__dropdown">
                                            <div class="mySelect__wrap">
                                                <ul>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newParking" value="Нет">
                                                            <span>Нет</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newParking" value="Наземная">
                                                            <span>Наземная</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newParking" value="Многоуровневая">
                                                            <span>Многоуровневая</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newParking" value="Подземная">
                                                            <span>Подземная</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label class="mySelect__radio">
                                                            <input type="radio" name="newParking" value="На крыше">
                                                            <span>На крыше</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="segment__row">
                                <div class="segment__label">Пассажирских лифтов</div>
                                <div class="segment__elem">
                                    <div class="segment__elem">
                                        <div class="mySelect">
                                            <div class="mySelect__btn">
                                                <span class="mySelect__selected">Нет</span>
                                            </div>
                                            <div class="mySelect__dropdown">
                                                <div class="mySelect__wrap">
                                                    <ul>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="newPassengerElevator" value="нет" checked>
                                                                <span>нет</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="newPassengerElevator" value="1">
                                                                <span>1</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="newPassengerElevator" value="2">
                                                                <span>2</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="newPassengerElevator" value="3">
                                                                <span>3</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="newPassengerElevator" value="4">
                                                                <span>4</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="segment__row">
                                <div class="segment__label">Грузовых лифтов</div>
                                <div class="segment__elem">
                                    <div class="segment__elem">
                                        <div class="mySelect">
                                            <div class="mySelect__btn">
                                                <span class="mySelect__selected">Нет</span>
                                            </div>
                                            <div class="mySelect__dropdown">
                                                <div class="mySelect__wrap">
                                                    <ul>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="newFreightElevator" value="нет" checked>
                                                                <span>нет</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="newFreightElevator" value="1">
                                                                <span>1</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="newFreightElevator" value="2">
                                                                <span>2</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="newFreightElevator" value="3">
                                                                <span>3</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="newFreightElevator" value="4">
                                                                <span>4</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="segment" id="segment4">
                            <div class="segment__title">Фотографии</div>
                            <div class="segment__info">Не допускаются к размещению фотографии с водяными знаками, чужих объектов и рекламные баннеры. JPG, PNG или GIF. Максимальный размер файла 10 мб</div>
                            <label class="fileUpload">
                                <input type="file" name="fileUpload">
                                <span class="fileUpload__icon">
                                    <i class="fas fa-download"></i>
                                </span>
                                <span class="fileUpload__text">Выберете файлы</span>
                            </label>

                            <div class="segment__title">Видео</div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="newVideo" placeholder="Ссылка на Youtube">
                            </div>

                            <div class="segment__title">Описание</div>
                            <div class="form-group">
                                <textarea class="form-control" name="newDescription" rows="8"></textarea>
                            </div>
                        </div>

                        <div class="segment" id="segment5">
                            <div class="segment__title">Цена и условия сделки</div>

                            <div class="segment__row">
                                <div class="segment__label">Цена</div>
                                <div class="segment__elem">
                                    <input type="text" class="form-control form-control-sm" name="newPrice" value="" placeholder="">
                                </div>
                            </div>

                            <div class="segment__row">
                                <div class="segment__label">Коммунальные платежи</div>
                                <div class="segment__elem">
                                    <div class="selectBox">
                                        <label class="selectBox__item">
                                            <input type="radio" name="newRentCommunal" value="Включены">
                                            <span>Включены</span>
                                        </label>
                                        <label class="selectBox__item">
                                            <input type="radio" name="newRentCommunal" value="Не включены">
                                            <span>Не включены</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="segment__row">
                                <div class="segment__label">Срок аренды</div>
                                <div class="segment__elem">
                                    <div class="selectBox">
                                        <label class="selectBox__item">
                                            <input type="radio" name="newRentTime" value="Длительный">
                                            <span>Длительный</span>
                                        </label>
                                        <label class="selectBox__item">
                                            <input type="radio" name="newRentTime" value="Краткосрочный">
                                            <span>Краткосрочный</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="segment__row">
                                <div class="segment__label">Предоплата</div>
                                <div class="segment__elem">
                                    <div class="selectBox">
                                        <label class="selectBox__item">
                                            <input type="radio" name="newRentPrepayment" value="Есть">
                                            <span>Есть</span>
                                        </label>
                                        <label class="selectBox__item">
                                            <input type="radio" name="newRentPrepayment" value="Нет">
                                            <span>Нет</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="segment__row">
                                <div class="segment__label">Залог собственнику</div>
                                <div class="segment__elem">
                                    <div class="selectBox">
                                        <label class="selectBox__item">
                                            <input type="radio" name="newRentPledge" value="Есть">
                                            <span>Есть</span>
                                        </label>
                                        <label class="selectBox__item">
                                            <input type="radio" name="newRentPledge" value="Нет">
                                            <span>Нет</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="segment" id="segment6">
                            <div class="segment__title">Для публикации объявления подтвердите номер мобильного телефона</div>
                            <div class="segment__phone">
                                <div class="segment__phone_input">
                                    <input type="text" class="form-control" name="newMobilePhone" value="" placeholder="+7 (927) 222-22-22">
                                </div>
                                <div class="segment__phone_button">
                                    <button type="submit" class="btn btn-sm btn-radius disable">Отправить код</button>
                                </div>
                                <div class="segment__phone_text">Мобильный телефон необходимо подтвердить кодом из СМС</div>
                            </div>
                        </div>

                        <div class="segment">
                            <br/>
                            <button type="submit" class="btn btn-sm btn-blue-border btn-circle">Опубликовать</button>
                            <br/>  <br/>
                        </div>

                    </div>

                    <div class="new__side">
                        <div class="new__nav landingMenu">
                            <ul>
                                <li><a href="#segment1">Тип объявления</a></li>
                                <li><a href="#segment2">Адрес</a></li>
                                <li><a href="#segment3">Об объекте</a></li>
                                <li><a href="#segment4">Описание и фото</a></li>
                                <li><a href="#segment5">Цена и условия</a></li>
                                <li><a href="#segment6">Контакты</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>

        </section>

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?>
        <!-- -->


    </div>

    <!-- Modal -->
    <?php include('inc/modal.inc.php') ?>
    <!-- -->

    <!-- Scripts -->
    <?php include('inc/scripts.inc.php') ?>
    <!-- -->

    <!-- Map -->
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

    <script>

        ymaps.ready(init);

        function init() {
            // Подключаем поисковые подсказки к полю ввода.
            var suggestView = new ymaps.SuggestView('suggest'),
                map,
                placemark;

            // При клике по кнопке запускаем верификацию введёных данных.
            $('#button').bind('click', function (e) {
                geocode();
            });

            $('#suggest').bind('change', function (e) {
                geocode();
            });

            function geocode() {
                // Забираем запрос из поля ввода.
                var request = $('#suggest').val();
                // Геокодируем введённые данные.
                ymaps.geocode(request).then(function (res) {
                    var obj = res.geoObjects.get(0),
                        error, hint;

                    if (obj) {
                        // Об оценке точности ответа геокодера можно прочитать тут: https://tech.yandex.ru/maps/doc/geocoder/desc/reference/precision-docpage/
                        switch (obj.properties.get('metaDataProperty.GeocoderMetaData.precision')) {
                            case 'exact':
                                break;
                            case 'number':
                            case 'near':
                            case 'range':
                                error = 'Неточный адрес, требуется уточнение';
                                hint = 'Уточните номер дома';
                                break;
                            case 'street':
                                error = 'Неполный адрес, требуется уточнение';
                                hint = 'Уточните номер дома';
                                break;
                            case 'other':
                            default:
                                error = 'Неточный адрес, требуется уточнение';
                                hint = 'Уточните адрес';
                        }
                    } else {
                        error = 'Адрес не найден';
                        hint = 'Уточните адрес';
                    }

                    // Если геокодер возвращает пустой массив или неточный результат, то показываем ошибку.
                    if (error) {
                        showError(error);
                        showMessage(hint);
                    } else {
                        showResult(obj);
                    }
                }, function (e) {
                    console.log(e)
                })

            }
            function showResult(obj) {
                // Удаляем сообщение об ошибке, если найденный адрес совпадает с поисковым запросом.
                $('#suggest').removeClass('input_error');
                $('#notice').css('display', 'none');

                var mapContainer = $('#map'),
                    bounds = obj.properties.get('boundedBy'),
                    // Рассчитываем видимую область для текущего положения пользователя.
                    mapState = ymaps.util.bounds.getCenterAndZoom(
                        bounds,
                        [mapContainer.width(), mapContainer.height()]
                    ),
                    // Сохраняем полный адрес для сообщения под картой.
                    address = [obj.getCountry(), obj.getAddressLine()].join(', '),
                    // Сохраняем укороченный адрес для подписи метки.
                    shortAddress = [obj.getThoroughfare(), obj.getPremiseNumber(), obj.getPremise()].join(' ');
                // Убираем контролы с карты.
                mapState.controls = [];
                // Создаём карту.
                createMap(mapState, shortAddress);
                // Выводим сообщение под картой.
                showMessage(address);
            }

            function showError(message) {
                $('#notice').text(message);
                $('#suggest').addClass('input_error');
                $('#notice').css('display', 'block');
                // Удаляем карту.
                if (map) {
                    map.destroy();
                    map = null;
                }
            }

            function createMap(state, caption) {
                // Если карта еще не была создана, то создадим ее и добавим метку с адресом.
                if (!map) {
                    map = new ymaps.Map('map', state);
                    placemark = new ymaps.Placemark(
                        map.getCenter(), {
                            iconCaption: caption,
                            balloonContent: caption
                        }, {
                            preset: 'islands#redDotIconWithCaption'
                        });
                    map.geoObjects.add(placemark);
                    // Если карта есть, то выставляем новый центр карты и меняем данные и позицию метки в соответствии с найденным адресом.
                } else {
                    map.setCenter(state.center, state.zoom);
                    placemark.geometry.setCoordinates(state.center);
                    placemark.properties.set({iconCaption: caption, balloonContent: caption});
                }
            }

            function showMessage(message) {
                $('#message').text(message);
            }
        }



    </script>
    <!-- -->


    <script>
        $(function($){
            $h = $('.new').offset().top;

            $(window).scroll(function(){

                if ( $(window).scrollTop() > $h) {
                    $('.new__nav').addClass('fixed');
                }else{

                    $('.new__nav').removeClass('fixed');
                }
            });
        });

    </script>


    </body>
</html>
