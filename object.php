]<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<head>
    <?php include('inc/head.inc.php') ?>
</head>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header.inc.php') ?>
    <!-- -->

    <div class="objectTop hide">
        <div class="container"></div>
    </div>

    <section class="main">

        <div class="container">

            <ul class="breadcrumb">
                <li><a href="#">Недвижимость в Перми</a></li>
                <li><a href="#">Продажа 2-комнатных квартир</a></li>
                <li><a href="#">24-я Северная</a></li>
            </ul>

            <div class="object__heading">
                <span class="object__date">сегодня, 01:41</span>, <span class="object__views"><i class="fas fa-eye"></i> 198 (15)</span>
                <h1>2-комн. квартира, 53,2 м²</h1>
                <div class="object__heading_address"><span>Омск, Центральный, ул. 24-я Северная, 196</span> <a href="#" class="object__heading_map"></a></div>
            </div>

            <div class="object">
                <div class="object__row">
                    <div class="object__content">
                        <div class="object__gallery">
                            <div class="fotorama" data-nav="thumbs" data-allowfullscreen="true">
                                <a href="images/object_01.jpg"><img src="images/object_01.jpg"  alt=""></a>
                                <a href="images/object_02.jpg"><img src="images/object_02.jpg"  alt=""></a>
                                <a href="images/object_03.jpg"><img src="images/object_03.jpg"  alt=""></a>
                                <a href="images/object_04.jpg"><img src="images/object_04.jpg"  alt=""></a>
                                <a href="images/object_05.jpg"><img src="images/object_05.jpg"  alt=""></a>
                                <a href="images/object_06.jpg"><img src="images/object_06.jpg"  alt=""></a>
                                <a href="images/object_07.jpg"><img src="images/object_07.jpg"  alt=""></a>
                                <a href="images/object_08.jpg"><img src="images/object_08.jpg"  alt=""></a>
                                <a href="images/object_09.jpg"><img src="images/object_09.jpg"  alt=""></a>
                                <a href="images/object_10.jpg"><img src="images/object_10.jpg"  alt=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="object__sidebar">
                        <div class="object__data">
                            <div class="object__data_first">
                                <div class="object__price">2 200 000 ₽</div>
                                <div class="object__price_area">41 353 ₽/м²</div>
                                <a href="#" class="object__subscribe"><i class="far fa-envelope"></i> <span>Следить за изменением цены</span></a>
                                <div class="object__status">Свободная продажа, возможна ипотека</div>
                                <a class="object__phone" href="tel:+79609952934">+7 960 995-29-34</a>
                                <div class="object__data_text">Пожалуйста, скажите, что нашли это объявление на МИАН</div>
                                <a href="#message" class="btn-small btn-modal">Написать сообщение</a>
                            </div>
                            <div class="object__data_second">
                                <span class="object__id">ID 13014860</span>  <span class="object__author">Собственник</span>
                                <ul class="object__action">
                                    <li>
                                        <label class="btn-favorite">
                                            <input type="checkbox">
                                            <i class="fas fa-heart"></i>
                                        </label>
                                    </li>
                                    <li>
                                        <a href="#callback" class="btn-action btn-modal" title="Перезвоните мне">
                                            <i class="fas fa-phone"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="btn-action" title="Посмотреть на карте">
                                            <i class="far fa-map"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="btn-action" title="Пожаловаться">
                                            <i class="far fa-bell"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="object__row">
                    <div class="object__content">

                        <div class="object__box">
                            <div id="objectMap">

                            </div>
                        </div>

                        <div class="object__box">
                            <h5>Описание</h5>
                            <div class="object__text box-hide">
                                <div class="object__text_wrap">
                                    Продаётся отличная квартира в Старгороде. Я собственник. Состоит из гостиной совмещенной с кухней (здесь площадь увеличена за счёт выноса балконных перегородок), детской комнаты, кабинета, спальни и прачечной. Фактически квартира четырехкомнатная! В спальне продуманы санузел и гардероб. На балконах поменяны окна на более качественные и залит в бетонную стяжку тёплый пол от отопления. В подъезде при входе сделан тамбур для того, чтобы не занимать площадь квартиры под гардероб для верхних вещей и обуви. А также между этажами имеется кладовая (для велосипеда, коляски и т. П. ). Дизайн-Проект делался и согласовывался полгода все было продумано до мелочей. Сейчас ремонт дошёл до чистовой отделки. Стены стоят заштукатуренные под покраску и обои, вся электропроводка раскинута, осталось поставить выключатели и розетки, эл. Щиток собран. Потолок тоже готов. Смежные с соседями стены заложены блоками и шумоизоляционным материалом. Полы выровнены. Стоят приборы учета. Возможен торг! Обмен на Lexus LX570 2017-18 года
                                </div>
                                <a href="#" class="object__text_toggle btn-view"><span>Подробнее</span> <i class="fas fa-angle-down"></i></a>
                            </div>
                        </div>

                        <div class="object__box">
                            <h5>Общая информация</h5>
                            <table class="object__table">
                                <tr>
                                    <td>Тип жилья</td>
                                    <td>Вторичка</td>
                                </tr>
                                <tr>
                                    <td>Количество комнат</td>
                                    <td>3</td>
                                </tr>
                                <tr>
                                    <td>Этаж</td>
                                    <td>2/6</td>
                                </tr>
                                <tr>
                                    <td>Высота потолков</td>
                                    <td>3,2 м</td>
                                </tr>
                                <tr>
                                    <td>Санузел</td>
                                    <td>1 раздельный, 1 совмещенный</td>
                                </tr>
                                <tr>
                                    <td>Ремонт</td>
                                    <td>Без ремонта</td>
                                </tr>
                                <tr>
                                    <td>Вид из окон</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>На улицу и двор</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Тип дома</td>
                                    <td>Кирпичный</td>
                                </tr>
                                <tr>
                                    <td>Лифт</td>
                                    <td>Пассажиский, грузовой</td>
                                </tr>
                                <tr>
                                    <td>Лоджия</td>
                                    <td>есть</td>
                                </tr>
                                <tr>
                                    <td>Балкон</td>
                                    <td>Есть</td>
                                </tr>
                                <tr>
                                    <td>Паркинг</td>
                                    <td>Подземный платный, во дворе</td>
                                </tr>
                                <tr>
                                    <td>Год посторойки</td>
                                    <td>2016</td>
                                </tr>
                                <tr>
                                    <td>Средняя цена за м<sup>2</sup></td>
                                    <td>52 299 ₽</td>
                                </tr>
                            </table>
                        </div>

                        <div class="object__box">
                            <div class="object__contact">
                                <div class="object__contact_row">
                                    <span class="object__contact_id">ID 15520745</span>
                                    <span class="object__contact_tag">Собственник</span>
                                </div>
                                <div class="object__contact_time">Менее 1 месяца на МИАН</div>
                                <div class="object__contact_row">
                                    <a class="object__contact_phone" href="tel:+79609952934">+7 960 995-29-34</a>
                                    <a href="#message" class="btn-small btn-modal">Написать сообщение</a>
                                </div>
                                <div class="object__contact_alert">Пожалуйста, скажите, что нашли это объявление на ЦИАН</div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <div class="goods">
                <div class="goods__heading">Похожие предложения</div>
                <div class="goods__slider">
                    <div class="goods__item">
                        <a class="goods__image" href="#">
                            <img src="images/apartment_01.jpg" alt="">
                        </a>
                        <a class="goods__title" href="#">3-комн.кв, 63/44/9 м², этаж 8/9</a>
                        <div class="goods__address">Омская область, Омск, Кемеровская улица, 20...</div>
                        <div class="goods__price">2 995 000 ₽</div>
                    </div>
                    <div class="goods__item">
                        <a class="goods__image" href="#">
                            <img src="images/apartment_02.jpg" alt="">
                        </a>
                        <a class="goods__title" href="#">3-комн.кв, 63/44/9 м², этаж 8/9</a>
                        <div class="goods__address">Омская область, Омск, Кемеровская улица, 20...</div>
                        <div class="goods__price">2 995 000 ₽</div>
                    </div>
                    <div class="goods__item">
                        <a class="goods__image" href="#">
                            <img src="images/apartment_03.jpg" alt="">
                        </a>
                        <a class="goods__title" href="#">3-комн.кв, 63/44/9 м², этаж 8/9</a>
                        <div class="goods__address">Омская область, Омск, Кемеровская улица, 20...</div>
                        <div class="goods__price">2 995 000 ₽</div>
                    </div>
                    <div class="goods__item">
                        <a class="goods__image" href="#">
                            <img src="images/apartment_04.jpg" alt="">
                        </a>
                        <a class="goods__title" href="#">3-комн.кв, 63/44/9 м², этаж 8/9</a>
                        <div class="goods__address">Омская область, Омск, Кемеровская улица, 20...</div>
                        <div class="goods__price">2 995 000 ₽</div>
                    </div>
                </div>
            </div>


        </div>

    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Modal -->
<?php include('inc/modal.inc.php') ?>
<!-- -->

<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->

<!-- Map -->
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

<script>
    var myMap;

    ymaps.ready(function () {
        myMap = new ymaps.Map('objectMap', {
            zoom: 16,
            center: [55.654137069072966,37.555875999999955],
            controls: ['zoomControl']
        }, {
            searchControlProvider: 'yandex#search'
        });
        myMap.geoObjects
            .add(new ymaps.Placemark([55.654137069072966,37.555875999999955], {
                balloonContent: '2-комн. квартира, 53,2 м²'
            }, {
                preset: 'islands#icon',
                iconColor: '#2b87db'
            }));
    });
</script>
<!-- -->

</body>
</html>
