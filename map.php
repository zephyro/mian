<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <!-- -->

    <body>

    <div class="page page-map">

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <section class="map">

            <div id="map"></div>

            <div class="map__content open">
                <div class="map__header">
                    <div class="map__header_wrap">
                        <a href="#" class="map__header_toggle filter-toggle">Фильтр</a>
                        <a href="#" class="map__header_list">Списком</a>
                    </div>
                </div>
                <div class="map__filter">

                    <div class="map__filter_scroll">
                        <div class="mapFilter">
                            <form class="form">
                                <ul class="mapFilter__first">

                                    <li>
                                        <div class="mySelect">
                                            <div class="mySelect__btn">
                                                <span class="mySelect__selected">Продажа</span>
                                            </div>
                                            <div class="mySelect__dropdown">
                                                <div class="mySelect__wrap">
                                                    <ul>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__deal" value="Продажа" checked>
                                                                <span>Продажа</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__deal" value="Аренда">
                                                                <span>Аренда</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__deal" value="Посуточно">
                                                                <span>Посуточно</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="mySelect">
                                            <div class="mySelect__btn">
                                                <span class="mySelect__selected">Квартира</span>
                                            </div>
                                            <div class="mySelect__dropdown">
                                                <div class="mySelect__wrap">
                                                    <ul>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Квартира" checked>
                                                                <span>Квартира</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Комната">
                                                                <span>Комната</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Дом">
                                                                <span>Дом</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Коттедж">
                                                                <span>Коттедж</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Участок">
                                                                <span>Участок</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Офис">
                                                                <span>Офис</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Торговая площадь">
                                                                <span>Торговая площадь</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Склад">
                                                                <span>Склад</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="mySelect">
                                            <div class="mySelect__btn" data-empty="Число комнат">
                                                <span class="mySelect__selected">Число комнат</span>
                                            </div>
                                            <div class="mySelect__dropdown">
                                                <div class="mySelect__wrap">
                                                    <ul>
                                                        <li>
                                                            <label class="mySelect__checkbox">
                                                                <input type="checkbox" name="filter__type" value="Студия">
                                                                <span>Студия</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__checkbox">
                                                                <input type="checkbox" name="filter__type" value="1к">
                                                                <span>1-комнатная</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__checkbox">
                                                                <input type="checkbox" name="filter__type" value="2к">
                                                                <span>2-комнатная</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__checkbox">
                                                                <input type="checkbox" name="filter__type" value="3к">
                                                                <span>3-комнатная</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__checkbox">
                                                                <input type="checkbox" name="filter__type" value="4к">
                                                                <span>4-комнатная</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="formRange">
                                            <div class="formRange__start">
                                                <input type="text" name="priceStart" placeholder="от">
                                            </div>
                                            <div class="formRange__end">
                                                <input type="text" name="priceEnd" placeholder="до">
                                                <span class="formRange__legend">₽</span>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="item-address">
                                        <input type="text" class="form-control" name="searchAddress" placeholder="Город, район, метро, адрес">
                                    </li>

                                    <li>
                                        <a href="#filterModal" class="mapFilter__button btn-modal"><i class="fas fa-sliders-h"></i>Фильтры<span class="mapFilter__button_value"></span></a>
                                    </li>

                                    <li><button type="submit" class="btn btn-sm">Показать</button></li>

                                </ul>

                            </form>
                        </div>
                    </div>

                    <span class="filter-button filter-toggle" title="Показать\скрыть фильтр"><i class="fas fa-angle-down"></i></span>

                    <!-- Расширенный фильтр -->
                    <div class="hide">
                        <div class="filterModal" id="filterModal">
                            <div class="filterModal__body">

                                <ul class="filterModal__row">
                                    <li>Дата публикации объявлений</li>
                                    <li>
                                        <div class="filterModal__content">
                                            <div class="filterModal__elem">
                                                <div class="mySelect filter__select">
                                                    <div class="mySelect__btn">
                                                        <span class="mySelect__selected">Сегодня</span>
                                                    </div>
                                                    <div class="mySelect__dropdown">
                                                        <div class="mySelect__wrap">
                                                            <ul>
                                                                <li>
                                                                    <label class="mySelect__radio">
                                                                        <input type="radio" name="filter__deal" value="Сегодня" checked>
                                                                        <span>Сегодня</span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label class="mySelect__radio">
                                                                        <input type="radio" name="filter__deal" value="Неделя">
                                                                        <span>Неделя</span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label class="mySelect__radio">
                                                                        <input type="radio" name="filter__deal" value="Две недели">
                                                                        <span>Две недели</span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label class="mySelect__radio">
                                                                        <input type="radio" name="filter__deal" value="Месяц">
                                                                        <span>Месяц</span>
                                                                    </label>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>

                                <ul class="filterModal__row">
                                    <li>Видео</li>
                                    <li>
                                        <div class="filterModal__content">
                                            <div class="filterModal__elem">
                                                <label class="form-checkbox">
                                                    <input type="checkbox" name="filter-photo" value="С видео">
                                                    <span>С видео</span>
                                                </label>
                                            </div>
                                        </div>
                                    </li>
                                </ul>

                                <ul class="filterModal__row">
                                    <li>Площадь, м²</li>
                                    <li>
                                        <div class="filterModal__content">

                                            <div class="filterModal__elem">
                                                <div class="filterModal__amount">
                                                    <label class="filterModal__amount_label">общая</label>
                                                    <div class="filterModal__amount_control">
                                                        <div class="filterModal__amount_start">
                                                            <input type="text" class="filterModal__amount_input" name="area-start" placeholder="от">
                                                        </div>
                                                        <div class="filterModal__amount_end">
                                                            <input type="text" class="filterModal__amount_input" name="area-end" placeholder="до">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="filterModal__elem">
                                                <div class="filterModal__amount">
                                                    <label class="filterModal__amount_label">кухня</label>
                                                    <div class="filterModal__amount_control">
                                                        <div class="filterModal__amount_start">
                                                            <input type="text" class="filterModal__amount_input" name="area-one-start" placeholder="от">
                                                        </div>
                                                        <div class="filterModal__amount_end">
                                                            <input type="text" class="filterModal__amount_input" name="area-one-end" placeholder="до">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="filterModal__elem">
                                                <div class="filterModal__amount">
                                                    <label class="filterModal__amount_label">жилая</label>
                                                    <div class="filterModal__amount_control">
                                                        <div class="filterModal__amount_start">
                                                            <input type="text" class="filterModal__amount_input" name="area-two-start" placeholder="от">
                                                        </div>
                                                        <div class="filterModal__amount_end">
                                                            <input type="text" class="filterModal__amount_input" name="area-two-end" placeholder="до">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </li>
                                </ul>

                                <ul class="filterModal__row">
                                    <li>Этаж</li>
                                    <li>
                                        <div class="filterModal__content">

                                            <div class="filterModal__elem">
                                                <div class="filterModal__amount">
                                                    <div class="filterModal__amount_control">
                                                        <div class="filterModal__amount_start">
                                                            <input type="text" class="filterModal__amount_input" name="floor-start" placeholder="от">
                                                        </div>
                                                        <div class="filterModal__amount_end">
                                                            <input type="text" class="filterModal__amount_input" name="floor-end" placeholder="до">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="filterModal__elem">
                                                <label class="button-checkbox">
                                                    <input type="radio" name="filter-floor" value="не первый">
                                                    <span>не первый</span>
                                                </label>
                                            </div>

                                            <div class="filterModal__elem">
                                                <label class="button-checkbox">
                                                    <input type="radio" name="filter-floor" value="не последний">
                                                    <span>не последний</span>
                                                </label>
                                            </div>

                                            <div class="filterModal__elem">
                                                <label class="button-checkbox">
                                                    <input type="radio" name="filter-floor" value="только последний">
                                                    <span>только последний</span>
                                                </label>
                                            </div>

                                        </div>
                                    </li>
                                </ul>

                                <ul class="filterModal__row">
                                    <li>Этажей в доме</li>
                                    <li>
                                        <div class="filterModal__content">
                                            <div class="filterModal__elem">
                                                <div class="filterModal__amount">
                                                    <div class="filterModal__amount_control">
                                                        <div class="filterModal__amount_start">
                                                            <input type="text" class="filterModal__amount_input" name="floor-start" placeholder="от">
                                                        </div>
                                                        <div class="filterModal__amount_end">
                                                            <input type="text" class="filterModal__amount_input" name="floor-end" placeholder="до">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>


                                <ul class="filterModal__row">
                                    <li>Тип дома</li>
                                    <li>
                                        <div class="filterModal__content">
                                            <div class="filterModal__elem">
                                                <label class="button-checkbox">
                                                    <input type="radio" name="filter-floor" value="Кирпичный">
                                                    <span>Кирпичный</span>
                                                </label>
                                            </div>
                                            <div class="filterModal__elem">
                                                <label class="button-checkbox">
                                                    <input type="radio" name="filter-floor" value="Деревянный">
                                                    <span>Деревянный</span>
                                                </label>
                                            </div>
                                            <div class="filterModal__elem">
                                                <label class="button-checkbox">
                                                    <input type="radio" name="filter-floor" value="Монолитный">
                                                    <span>Монолитный</span>
                                                </label>
                                            </div>
                                            <div class="filterModal__elem">
                                                <label class="button-checkbox">
                                                    <input type="radio" name="filter-floor" value="Панельный">
                                                    <span>Панельный</span>
                                                </label>
                                            </div>
                                            <div class="filterModal__elem">
                                                <label class="button-checkbox">
                                                    <input type="radio" name="filter-floor" value="Блочный">
                                                    <span>Блочный</span>
                                                </label>
                                            </div>
                                            <div class="filterModal__elem">
                                                <label class="button-checkbox">
                                                    <input type="radio" name="filter-floor" value="Кирпично-монолитный">
                                                    <span>Кирпично-монолитный</span>
                                                </label>
                                            </div>
                                            <div class="filterModal__elem">
                                                <label class="button-checkbox">
                                                    <input type="radio" name="filter-floor" value="Сталинский">
                                                    <span>Сталинский</span>
                                                </label>
                                            </div>
                                        </div>
                                    </li>
                                </ul>

                                <ul class="filterModal__row">
                                    <li>Год постройки</li>
                                    <li>
                                        <div class="filterModal__content">
                                            <div class="filterModal__elem">
                                                <div class="filterModal__amount">
                                                    <div class="filterModal__amount_control">
                                                        <div class="filterModal__amount_start">
                                                            <input type="text" class="filterModal__amount_input" name="year-start" placeholder="от">
                                                        </div>
                                                        <div class="filterModal__amount_end">
                                                            <input type="text" class="filterModal__amount_input" name="year-end" placeholder="до">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>

                                <ul class="filterModal__row">
                                    <li>Высота потолков</li>
                                    <li>
                                        <div class="filterModal__content">
                                            <div class="filterModal__elem">
                                                <div class="filterModal__amount">
                                                    <div class="filterModal__amount_control">
                                                        <div class="filterModal__amount_start">
                                                            <input type="text" class="filterModal__amount_input" name="height-start" placeholder="от">
                                                        </div>
                                                        <div class="filterModal__amount_end">
                                                            <input type="text" class="filterModal__amount_input" name="height-end" placeholder="до">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>

                                <ul class="filterModal__row">
                                    <li>Балкон</li>
                                    <li>
                                        <div class="filterModal__content">
                                            <div class="filterModal__elem">

                                                <div class="filterModal__elem">
                                                    <label class="button-checkbox">
                                                        <input type="checkbox" name="param-one" value="Есть балкон">
                                                        <span>Есть балкон</span>
                                                    </label>
                                                </div>

                                                <div class="filterModal__elem">
                                                    <label class="button-checkbox">
                                                        <input type="checkbox" name="param-one" value="Есть лоджия">
                                                        <span>Есть лоджия</span>
                                                    </label>
                                                </div>

                                            </div>
                                        </div>
                                    </li>
                                </ul>

                                <ul class="filterModal__row">
                                    <li>Ремонт</li>
                                    <li>
                                        <div class="filterModal__content">

                                            <div class="filterModal__elem">
                                                <label class="button-checkbox">
                                                    <input type="checkbox" name="param-two" value="Косметический">
                                                    <span>Косметический</span>
                                                </label>
                                            </div>

                                            <div class="filterModal__elem">
                                                <label class="button-checkbox">
                                                    <input type="checkbox" name="param-two" value="Евроремонт">
                                                    <span>Евроремонт</span>
                                                </label>
                                            </div>

                                            <div class="filterModal__elem">
                                                <label class="button-checkbox">
                                                    <input type="checkbox" name="param-two" value="Дизайнерский">
                                                    <span>Дизайнерский</span>
                                                </label>
                                            </div>

                                            <div class="filterModal__elem">
                                                <label class="button-checkbox">
                                                    <input type="checkbox" name="param-two" value="Без ремонта">
                                                    <span>Без ремонта</span>
                                                </label>
                                            </div>

                                        </div>
                                    </li>
                                </ul>

                                <ul class="filterModal__row">
                                    <li>Парковка</li>
                                    <li>
                                        <div class="filterModal__content">

                                            <div class="filterModal__elem">
                                                <label class="button-checkbox">
                                                    <input type="checkbox" name="param-parking" value="Наземная">
                                                    <span>Наземная</span>
                                                </label>
                                            </div>

                                            <div class="filterModal__elem">
                                                <label class="button-checkbox">
                                                    <input type="checkbox" name="param-parking" value="Многоуровневая">
                                                    <span>Многоуровневая</span>
                                                </label>
                                            </div>

                                            <div class="filterModal__elem">
                                                <label class="button-checkbox">
                                                    <input type="checkbox" name="param-parking" value="Подземная">
                                                    <span>Подземная</span>
                                                </label>
                                            </div>

                                            <div class="filterModal__elem">
                                                <label class="button-checkbox">
                                                    <input type="checkbox" name="param-parking" value="На крыше">
                                                    <span>На крыше</span>
                                                </label>
                                            </div>

                                        </div>
                                    </li>
                                </ul>

                            </div>
                            <div class="filterModal__bottom">
                                <button type="submit" class="btn btn-blue btn-circle">Показать результаты</button>
                            </div>
                        </div>
                    </div>
                    <!-- -->

                </div>

            </div>

            <div class="result">
                <div class="result__header">
                    <div class="result__header_text"><span>Омская область, Омск, улица 3-я Молодежная, 77</span></div>
                    <span class="result__close"></span>
                </div>
                <div class="result__body">
                    <div class="result__scroll">
                        <div class="result__wrap">

                            <div class="result__total">
                                <span class="result__total_value">4 объявления</span>
                                <span class="result__total_image"><span class="on">показать</span><span class="off">скрыть</span> фото</span>
                            </div>

                            <div class="result__item">
                                <a href="#" class="result__media">
                                    <div class="result__slider">
                                        <div class="result__slider_item">
                                            <img src="images/apartment_01.jpg" alt="">
                                        </div>
                                        <div class="result__slider_item">
                                            <img src="images/apartment_02.jpg" alt="">
                                        </div>
                                        <div class="result__slider_item">
                                            <img src="images/apartment_03.jpg" alt="">
                                        </div>
                                        <div class="result__slider_item">
                                            <img src="images/apartment_04.jpg" alt="">
                                        </div>
                                        <div class="result__slider_item">
                                            <img src="images/apartment_05.jpg" alt="">
                                        </div>

                                    </div>
                                </a>
                                <a href="#" class="result__content">
                                    <div class="result__price">2 890 000 ₽</div>

                                    <!-- favorite -->
                                    <?php include('inc/like.inc.php') ?>
                                    <!-- -->


                                    <div class="result__name">
                                        <span>1-комн</span>
                                        <span>41,7 м²</span>
                                        <span>1/8 этаж</span>
                                    </div>

                                    <div class="result__address">Московская область, Мытищи, Осташковское шоссе, 22к4</div>
                                    <div class="result__info">
                                        <div class="result__phone">+7 (925) 911-45-20</div>
                                        <div class="result__author">Ирина Александрова</div>
                                    </div>
                                </a>
                            </div>

                            <div class="result__item">
                                <a href="#" class="result__media">
                                    <div class="result__slider">
                                        <div class="result__slider_item">
                                            <img src="images/apartment_07.jpg" alt="">
                                        </div>
                                        <div class="result__slider_item">
                                            <img src="images/apartment_02.jpg" alt="">
                                        </div>
                                        <div class="result__slider_item">
                                            <img src="images/apartment_05.jpg" alt="">
                                        </div>
                                        <div class="result__slider_item">
                                            <img src="images/apartment_04.jpg" alt="">
                                        </div>
                                        <div class="result__slider_item">
                                            <img src="images/apartment_01.jpg" alt="">
                                        </div>

                                    </div>
                                </a>
                                <a href="#" class="result__content">
                                    <div class="result__price">2 890 000 ₽</div>

                                    <!-- favorite -->
                                    <?php include('inc/like.inc.php') ?>
                                    <!-- -->


                                    <div class="result__name">
                                        <span>1-комн</span>
                                        <span>41,7 м²</span>
                                        <span>1/8 этаж</span>
                                    </div>

                                    <div class="result__place">м Медведково, <i class="far fa-clock"></i> 30 минут</div>
                                    <div class="result__address">Московская область, Мытищи, Осташковское шоссе, 22к4</div>

                                    <div class="result__info">
                                        <div class="result__phone">+7 (925) 911-45-20</div>
                                        <div class="result__author">Ирина Александрова</div>
                                    </div>

                                </a>
                            </div>

                            <div class="result__item">
                                <a href="#" class="result__media">
                                    <div class="result__slider">
                                        <div class="result__slider_item">
                                            <img src="images/apartment_01.jpg" alt="">
                                        </div>
                                        <div class="result__slider_item">
                                            <img src="images/apartment_02.jpg" alt="">
                                        </div>
                                        <div class="result__slider_item">
                                            <img src="images/apartment_03.jpg" alt="">
                                        </div>
                                        <div class="result__slider_item">
                                            <img src="images/apartment_04.jpg" alt="">
                                        </div>
                                        <div class="result__slider_item">
                                            <img src="images/apartment_05.jpg" alt="">
                                        </div>

                                    </div>
                                </a>
                                <a href="#" class="result__content">
                                    <div class="result__price">2 890 000 ₽</div>

                                    <!-- favorite -->
                                    <?php include('inc/like.inc.php') ?>
                                    <!-- -->


                                    <div class="result__name">
                                        <span>1-комн</span>
                                        <span>41,7 м²</span>
                                        <span>1/8 этаж</span>
                                    </div>

                                    <div class="result__address">Московская область, Мытищи, Осташковское шоссе, 22к4</div>
                                    <div class="result__info">
                                        <div class="result__phone">+7 (925) 911-45-20</div>
                                        <div class="result__author">Ирина Александрова</div>
                                    </div>
                                </a>
                            </div>

                            <div class="result__item">
                                <a href="#" class="result__media">
                                    <div class="result__slider">
                                        <div class="result__slider_item">
                                            <img src="images/apartment_07.jpg" alt="">
                                        </div>
                                        <div class="result__slider_item">
                                            <img src="images/apartment_02.jpg" alt="">
                                        </div>
                                        <div class="result__slider_item">
                                            <img src="images/apartment_05.jpg" alt="">
                                        </div>
                                        <div class="result__slider_item">
                                            <img src="images/apartment_04.jpg" alt="">
                                        </div>
                                        <div class="result__slider_item">
                                            <img src="images/apartment_01.jpg" alt="">
                                        </div>

                                    </div>
                                </a>
                                <a href="#" class="result__content">
                                    <div class="result__price">2 890 000 ₽</div>

                                    <!-- favorite -->
                                    <?php include('inc/like.inc.php') ?>
                                    <!-- -->


                                    <div class="result__name">
                                        <span>1-комн</span>
                                        <span>41,7 м²</span>
                                        <span>1/8 этаж</span>
                                    </div>

                                    <div class="result__place">м Медведково, <i class="far fa-clock"></i> 30 минут</div>
                                    <div class="result__address">Московская область, Мытищи, Осташковское шоссе, 22к4</div>

                                    <div class="result__info">
                                        <div class="result__phone">+7 (925) 911-45-20</div>
                                        <div class="result__author">Ирина Александрова</div>
                                    </div>

                                </a>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

        </section>


    </div>

    <!-- Modal -->
    <?php include('inc/modal.inc.php') ?>
    <!-- -->

    <!-- Scripts -->
    <?php include('inc/scripts.inc.php') ?>
    <!-- -->

    <!-- Map -->
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

    <script>

        ymaps.ready(function () {
            var myMap = new ymaps.Map('map', {
                    center: [55.751574, 37.573856],
                    zoom: 7,
                    controls: ['zoomControl']
                }, {
                    searchControlProvider: 'yandex#search'
                }),
                clusterer = new ymaps.Clusterer({
                    preset: 'islands#invertedDarkBlueClusterIcons',
                    clusterHideIconOnBalloonOpen: false,
                    geoObjectHideIconOnBalloonOpen: false
                });



            /**
             * Кластеризатор расширяет коллекцию, что позволяет использовать один обработчик
             * для обработки событий всех геообъектов.
             * Будем менять цвет иконок и кластеров при наведении.
             */
            clusterer.events
            // Можно слушать сразу несколько событий, указывая их имена в массиве.
                .add(['mouseenter', 'mouseleave'], function (e) {
                    var target = e.get('target'),
                        type = e.get('type');
                    if (typeof target.getGeoObjects != 'undefined') {
                        // Событие произошло на кластере.
                        if (type == 'mouseenter') {
                            target.options.set('preset', 'islands#invertedBlueClusterIcons');
                        } else {
                            target.options.set('preset', 'islands#invertedDarkBlueClusterIcons');
                        }
                    } else {
                        // Событие произошло на геообъекте.
                        if (type == 'mouseenter') {
                            target.options.set('preset', 'islands#blueDotIcon');
                        } else {
                            target.options.set('preset', 'islands#darkBlueDotIcon');
                        }
                    }
                })
                .add('click', function (e) {
                    var target = e.get('target');
                    if (typeof target.getGeoObjects != 'undefined') {
                        // Событие произошло на кластере.

                    } else {
                        console.log('click');
                        $('.result').addClass('open');
                    }
                });

            var getPointData = function (index) {
                },
                getPointOptions = function () {
                    return {
                        preset: 'islands#darkBlueDotIcon'
                    };
                },
                points = [
                    [55.831903, 37.411961], [55.763338, 37.565466], [55.762338, 37.595466], [55.744522, 37.616378],
                    [55.780898, 37.642889], [55.793559, 37.435983], [55.800584, 37.675638], [55.716733, 37.589988],
                    [55.775724, 37.560840], [55.822144, 37.433781], [55.874170, 37.669838], [55.716770, 37.482338],
                    [55.780850, 37.750210], [55.810906, 37.654142], [55.865386, 37.713329], [55.847121, 37.525797],
                    [55.778655, 37.710743], [55.623415, 37.717934], [55.863193, 37.737000], [55.866770, 37.760113],
                    [55.698261, 37.730838], [55.633800, 37.564769], [55.639996, 37.539400], [55.690230, 37.405853],
                    [55.775970, 37.512900], [55.775777, 37.442180], [55.811814, 37.440448], [55.751841, 37.404853],
                    [55.627303, 37.728976], [55.816515, 37.597163], [55.664352, 37.689397], [55.679195, 37.600961],
                    [55.673873, 37.658425], [55.681006, 37.605126], [55.876327, 37.431744], [55.843363, 37.778445],
                    [55.875445, 37.549348], [55.662903, 37.702087], [55.746099, 37.434113], [55.838660, 37.712326],
                    [55.774838, 37.415725], [55.871539, 37.630223], [55.657037, 37.571271], [55.691046, 37.711026],
                    [55.803972, 37.659610], [55.616448, 37.452759], [55.781329, 37.442781], [55.844708, 37.748870],
                    [55.723123, 37.406067], [55.858585, 37.484980]
                ],
                geoObjects = [];

            for(var i = 0, len = points.length; i < len; i++) {
                geoObjects[i] = new ymaps.Placemark(points[i], getPointData(i), getPointOptions());
            }

            clusterer.add(geoObjects);
            myMap.geoObjects.add(clusterer);

            myMap.setBounds(clusterer.getBounds(), {
                checkZoomRange: true
            });
        });

    </script>
    <!-- -->

    </body>

</html>

