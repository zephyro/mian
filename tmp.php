<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="objectTop hide">
                <div class="container"></div>
            </div>

            <section class="main hide">

                <div class="container">

                    <ul class="breadcrumb">
                        <li><a href="#">Недвижимость в Перми</a></li>
                        <li><a href="#">Продажа 2-комнатных квартир</a></li>
                        <li><a href="#">24-я Северная</a></li>
                    </ul>

                    <div class="object__heading">

                        <span class="object__date">сегодня, 01:41</span>, <span class="object__views"><i class="fas fa-eye"></i> 198 просмотров, 15 за сегодня</span>
                        <h1>2-комн. квартира, 53,2 м²</h1>
                        <div class="object__heading_address"><span>Омская область, Омск, Центральный, ул. 24-я Северная, 196</span> <a href="#" class="object__heading_map"></a></div>
                    </div>

                    <div class="object">
                        <div class="object__row">
                            <select class="form-select" data-placeholder="Введите адрес">
                                <option value="Россия, Москва">Россия, Москва</option>
                                <option value="Россия, Москва, Профсоюзная ул. 83">Россия, Москва, Профсоюзная ул. 83</option>
                                <option value="Россия, Москва, Тверская">Россия, Москва, Тверская</option>
                                <option value="Россия, Москва, Дорогомиловская ул. 15">Россия, Москва, Дорогомиловская ул. 15</option>
                                <option value="Россия, Москва, Ленинский проспект">Россия, Москва, Ленинский проспект</option>
                                <option value="Россия, Москва, Профсоюзная ул. 75">Россия, Москва, Профсоюзная ул. 75</option>
                                <option value="Россия, Москва, Кутузовский проспект, 21">Россия, Кутузовский проспект, 21</option>
                                <option value="Россия, Москва, Кутузовский проспект, 35">Россия, Кутузовский проспект, 35</option>
                            </select>
                            <div class="object__content">
                                <div class="object__gallery">
                                    <div class="object__gallery_wrap">
                                        <div class="fotorama">
                                            <img src="images/object_01.jpg"  alt="">
                                            <img src="images/object_02.jpg"  alt="">
                                            <img src="images/object_03.jpg"  alt="">
                                            <img src="images/object_04.jpg"  alt="">
                                            <img src="images/object_05.jpg"  alt="">
                                            <img src="images/object_06.jpg"  alt="">
                                            <img src="images/object_07.jpg"  alt="">
                                            <img src="images/object_08.jpg"  alt="">
                                            <img src="images/object_09.jpg"  alt="">
                                            <img src="images/object_10.jpg"  alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="object__side">
                                <div class="object__data">
                                    <div class="object__data_first">
                                        <div class="object__price">2 200 000 ₽</div>
                                        <div class="object__price_area">41 353 ₽/м²</div>
                                        <a href="#" class="object__subscribe"><i class="far fa-envelope"></i> <span>Следить за изменением цены</span></a>
                                        <div class="object__status">Свободная продажа, возможна ипотека</div>
                                        <a href="tel:+79609952934">+7 960 995-29-34</a>
                                        <div class="object__data_text">Пожалуйста, скажите, что нашли это объявление на ЦИАН</div>
                                        <a href="#" class="object_message">Написать сообщение</a>
                                    </div>
                                    <div class="object__data_second">
                                        <span class="object__id">ID 13014860</span>  <span class="object__author">Собственник</span>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                </div>

            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    <script>

        function init() {
            var myMap = new ymaps.Map("map", {
                center: [56.72714606799689,37.143091499999954],
                zoom: 12,
                controls: ['zoomControl']
            }, {
                searchControlProvider: ''
            });

            var myPlacemark = new ymaps.Placemark([56.72714606799689,37.143091499999954], {
                balloonContent: '',
                iconCaption: ''
            }, {
                preset: 'islands#greenDotIconWithCaption',
                iconColor: '#0095b6'
            });

            myMap.geoObjects.add(myPlacemark);

            myPlacemark.events.add('click', function (e) {
                $('.result').toggleClass('open');
            });
        }

    </script>

    </body>
</html>
