<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<head>
    <?php include('inc/head.inc.php') ?>
</head>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header.inc.php') ?>
    <!-- -->

    <section class="main">

        <div class="container">

            <ul class="breadcrumb">
                <li><a href="#">Недвижимость в Перми</a></li>
                <li><a href="#">Личный кабинет</a></li>
                <li><span>Пополнение счета</span></li>
            </ul>

            <h1>Личный кабинет</h1>

            <div class="whiteBox">

                <div class="pay">
                    <div class="pay__payment">

                        <div class="payment">

                            <div class="payment__step">
                                <div class="payment__title">Введите сумму для пополнения:</div>
                                <div class="payment__amount">
                                    <input type="text" name="paymentAmount" class="form-control" placeholder="0.00">
                                </div>
                            </div>

                            <div class="payment__step">
                                <div class="payment__title">Выберите способ пополнения счета:</div>

                                <ul class="payment__type">
                                    <li>
                                        <a href="#" class="payment__type_card">
                                            <div class="payment__type_icon">
                                                <img src="img/card.svg" class="payment-icon icon-card">
                                            </div>
                                            <div class="payment__type_title">Банковская карта</div>
                                            <div class="payment__type_text">Без комиссии</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="payment__type_card">
                                            <div class="payment__type_icon">
                                                <img src="img/sberbank_online.svg" class="payment-icon icon-sberbank">
                                            </div>
                                            <div class="payment__type_title">Сбербанк онлайн</div>
                                            <div class="payment__type_text">Без комиссии</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="payment__type_card">
                                            <div class="payment__type_icon">
                                                <img src="img/alpha_click.svg" class="payment-icon icon-alpha">
                                            </div>
                                            <div class="payment__type_title">Альфа-клик</div>
                                            <div class="payment__type_text">Без комиссии</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="payment__type_card">
                                            <div class="payment__type_icon">
                                                <img src="img/qiwi_wallet.svg" class="payment-icon icon-quwi">
                                            </div>
                                            <div class="payment__type_title">Киви Кошелек</div>
                                            <div class="payment__type_text">Без комиссии</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="payment__type_card">
                                            <div class="payment__type_icon">
                                                <img src="img/yandex_money.svg" class="payment-icon icon-yandex">
                                            </div>
                                            <div class="payment__type_title">Яндекс.Деньги</div>
                                            <div class="payment__type_text">Без комиссии</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="payment__type_card">
                                            <div class="payment__type_icon">
                                                <img src="img/mobile_phone.svg" class="payment-icon icon-mobile">
                                            </div>
                                            <div class="payment__type_title">Мобильный телефон</div>
                                            <div class="payment__type_text">Без комиссии</div>
                                        </a>
                                    </li>
                                </ul>

                            </div>

                        </div>

                    </div>
                    <div class="pay__balance">

                        <div class="balance">
                            <div class="balance__summary">
                                <div class="balance__heading">Ваш баланс</div>
                                <div class="balance__value"><span>0,00</span></div>
                                <a href="#" class="balance__report">cписания за вчера</a>
                            </div>

                            <table class="balance__info">
                                <tr>
                                    <td>Тарифный план:</td>
                                    <td><a href="#">Некоммерческий</a></td>
                                </tr>
                                <tr>
                                    <td>Тип плательщика:</td>
                                    <td>Физическое лицо</td>
                                </tr>
                                <tr>
                                    <td>Автопродление</td>
                                    <td><a href="#">Отключено</a></td>
                                </tr>
                            </table>

                        </div>

                    </div>
                </div>

            </div>

        </div>

    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Modal -->
<?php include('inc/modal.inc.php') ?>
<!-- -->



<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->



</body>
</html>
