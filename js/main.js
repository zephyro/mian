
$(".btn-modal").fancybox({
    'padding'    : 0
});

$('.header__toggle').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).toggleClass('open');
    $('.nav').toggleClass('open');
});

$('.filter__heading_toggle').on('click touchstart', function(e) {
    e.preventDefault();
    $('.filter').toggleClass('open');
});

$('.tabs-nav li a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box =   $(this).closest('.tabs');

    $(this).closest('.tabs-nav').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    box.find('.tabs-item').removeClass('active');
    box.find(tab).addClass('active');

});


$('.btn-view').click(function(e) {
    e.preventDefault();
    $(this).closest('.box-hide').toggleClass('open');
});




$('.result__close').on('click touchstart', function(e) {
    e.preventDefault();
    $('.result').toggleClass('open');
});



$(document).ready(function(){

    $('.mySelect .mySelect__btn').click(function(){
        var elem = $(this).parent();
        if(elem.hasClass("open")) {
            elem.removeClass('open');
        }
        else {
            $('body').find('.mySelect').removeClass('open');
            elem.addClass('open');
        }
        return false;
    });
    $(document).on('click touchstart', function(event) {
        if ($(event.target).closest('.mySelect').length){
            return;
        }else{
            $('.mySelect').removeClass('open');
        }
        event.stopPropagation();
    });

    $('.mySelect input[type="radio"]').change(function(e) {
        var parent = $(this).closest('.mySelect');
        parent.removeClass('open');
        var txt = $(this).val();
        parent.find('.mySelect__selected').text(txt);
    });

    $('.mySelect input[type="checkbox"]').change(function(){
        var parent = $(this).closest('.mySelect'),
            checkbox_block_arr = [],
            length = $('.mySelect label').length,
            count = 0;

        parent.find($('.mySelect label')).each(function(){
            if($(this).find('input').is(':checked')){
                checkbox_block_arr[count] = $(this).find('input').val() +", ";
                count++;
            }
        });

        parent.find($('.mySelect__selected')).html(checkbox_block_arr);
        if(!checkbox_block_arr.length){
            var empty_data = parent.find($('.mySelect__btn')).data('empty');
            parent.find($('.mySelect__selected')).html(empty_data);
        }
    });

});



$(document).ready(function(){
    /* инициализация функций */

    /* описание функций */

    $('.input_block>p').click(function(){
        $(this).parent().toggleClass('open');

        if($(this).parent().hasClass('open')){
            $(this).parent().find('.input_dropdown').slideDown();
        }else{
            $(this).parent().find('.input_dropdown').slideUp();
        }
        return false;
    });
    $(document).on('click touchstart', function(event) {
        if ($(event.target).closest('.input_block').length){
            return;
        }else{
            $('.input_block').removeClass('open');
            $('.input_block').removeClass('open');
            $('.input_dropdown').slideUp();
        }
        event.stopPropagation();
    });

    $('.checkbox_block').change(function(){
        var parent = $(this).closest('.input_block'),
            checkbox_block_arr = [],
            length = $('.input_dropdown .checkbox_block').length,
            count = 0;

        parent.find($('.input_dropdown .checkbox_block')).each(function(){
            if($(this).find('input').is(':checked')){
                checkbox_block_arr[count] = $(this).find('b').html() +", ";
                count++;
            }
        });

        parent.find($('.input_block>p span')).html(checkbox_block_arr);
        if(!checkbox_block_arr.length){
            var empty_data = parent.find($('.input_block>p')).data('empty');
            parent.find($('.input_block>p span')).html(empty_data);
        }
    });

});


$('.offer__slider').slick({
    dots: false,
    infinite: true,
    arrows: true,
    autoplay: false,
    autoplaySpeed: 5000,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"><i class="fas fa-chevron-left"></i></span>',
    nextArrow: '<span class="slide-nav next"><i class="fas fa-chevron-right"></i></span>'
});




$('.goods__slider').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    focusOnSelect: false,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 576,
            settings: {
                slidesToShow: 1
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});



$('.result__total_image').on('click touchstart', function(e) {
    e.preventDefault();
    var panel = $('.result');
    panel.toggleClass('visibleImage');
    if(panel.hasClass("visibleImage")) {

        $('.result__slider').slick({
            dots: false,
            infinite: true,
            arrows: true,
            autoplay: false,
            autoplaySpeed: 5000,
            slidesToShow: 1,
            slidesToScroll: 1,
            prevArrow: '<span class="slide-nav prev"><i class="fas fa-chevron-left"></i></span>',
            nextArrow: '<span class="slide-nav next"><i class="fas fa-chevron-right"></i></span>'
        });
    }
    else {
        $('.result__slider').slick('unslick');
    }
});

$('.filter-toggle').on('click touchstart', function(e) {
    e.preventDefault();
    $('.map__content').toggleClass('open');
});