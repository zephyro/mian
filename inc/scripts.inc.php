<script src="js/vendor/modernizr-3.5.0.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
<script src="js/vendor/jquery.select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="js/vendor/jquery.select2/js/i18n/ru.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
<script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>
<script src="js/vendor/slick/slick.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
