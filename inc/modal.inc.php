<!-- Auth modal -->
<div class="hide">
    <div class="authModal tabs" id="auth">
        <div class="authModal__heading">
            <ul class="tabs-nav">
                <li class="active"><a href="#" data-target=".tab1">ВХОД</a></li>
                <li><a href="#" data-target=".tab2">РЕГИСТРАЦИЯ</a></li>
            </ul>
        </div>
        <div class="authModal__content">
            <div class="tabs-item tab1 active">
                <div class="authModal__social">
                    <div class="social">
                        <a class="social__vk" href="#"><i class="fab fa-vk"></i></a>
                        <a class="social__twitter" href="#"><i class="fab fa-twitter"></i></a>
                        <a class="social__odnoklassniki" href="#"><i class="fab fa-odnoklassniki"></i></a>
                        <a class="social__google" href="#"><i class="fab fa-google"></i></a>
                        <a class="social__facebook" href="#"><i class="fab fa-facebook-f"></i></a>
                    </div>
                    <span class="authModal__or">или</span>
                </div>
                <div class="authModal__form">
                    <form class="form">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" placeholder="Email или Логин">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="Пароль">
                        </div>
                        <div class="form-group">
                            <ul class="form-support">
                                <li>
                                    <label class="form-checkbox">
                                        <input type="checkbox" name="ok">
                                        <span>Запомнить меня</span>
                                    </label>
                                </li>
                                <li>
                                    <a href="#">Забыли пароль?</a>
                                </li>
                            </ul>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn">Войти</button>
                        </div>

                        <div class="authModal__text">
                            При входе, вы принимаете условия
                            <br/>
                            <a href="#">Пользовательского соглашения</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="tabs-item tab2">
                <div class="authModal__social">
                    <div class="social">
                        <a class="social__vk" href="#"><i class="fab fa-vk"></i></a>
                        <a class="social__twitter" href="#"><i class="fab fa-twitter"></i></a>
                        <a class="social__odnoklassniki" href="#"><i class="fab fa-odnoklassniki"></i></a>
                        <a class="social__google" href="#"><i class="fab fa-google"></i></a>
                        <a class="social__facebook" href="#"><i class="fab fa-facebook-f"></i></a>
                    </div>
                    <span class="authModal__or">или</span>
                </div>
                <div class="authModal__form">
                    <form class="form">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" placeholder="Email или Логин">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="Пароль">
                        </div>
                        <div class="form-group">
                            <label class="form-checkbox">
                                <input type="checkbox" name="subscribe">
                                <span>Хочу получать новости</span>
                            </label>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn">Зарегистрироваться</button>
                        </div>

                        <div class="authModal__text">
                            При входе, вы принимаете условия
                            <br/>
                            <a href="#">Пользовательского соглашения</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- -->


<!-- Form modal -->
<div class="hide">
    <div class="modal" id="message">
        <div class="modal__heading">Написать сообщение!</div>
        <div class="modal__content">
            <form class="form">
                <div class="form-group">
                    <textarea class="form-control" name="message" rows="8">Заинтересовало ваше предложение, просьба прислать дополнительную информацию.</textarea>
                </div>
                <div class="form-row">
                    <div class="form-col">
                       <div class="form-group">
                           <input type="text" class="form-control" name="phone" placeholder="Номер телефона">
                       </div>
                    </div>
                    <div class="form-col">
                        <div class="form-group">
                            <input type="text" class="form-control" name="email" placeholder="email">
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-sm">Отправить</button>
            </form>
        </div>
    </div>
</div>
<!-- -->


<!-- Callback -->
<div class="hide">
    <div class="modal" id="callback">
        <div class="modal__heading">Перезвоните мне</div>
        <div class="modal__content">
            <form class="form">
                <div class="form-row">
                    <div class="form-col">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" placeholder="Имя">
                        </div>
                    </div>
                    <div class="form-col">
                        <div class="form-group">
                            <input type="text" class="form-control" name="phone" placeholder="Номер телефона">
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-sm">Отправить</button>
            </form>
        </div>
    </div>
</div>
<!-- -->