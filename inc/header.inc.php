<header class="header">

    <div class="header__top">
        <div class="container">
            <a href="#" class="header__toggle"><i class="fas fa-bars"></i></a>
            <a href="/" class="header__top_logo">mian.ru</a>
            <nav class="nav">

                <ul class="nav__primary">
                    <li><a href="#"><span>Продажа</span></a></li>
                    <li><a href="#"><span>Аренда</span></a></li>
                    <li><a href="#"><span>Новостройки</span></a></li>
                    <li><a href="#"><span>Спецпредложения</span></a></li>
                    <li class="hidden-md"><a href="#"><span>Посуточно</span></a></li>
                </ul>

                <ul class="nav__second">
                    <li> <a href="#"><span>Поиск на карте</span></a></li>
                    <li> <a href="#"><span>Участники</span></a></li>
                    <li> <a href="#"><span>Новости</span></a></li>
                    <li> <a href="#"><span>Статьи</span></a></li>
                    <li> <a href="#"><span>Правила портала</span></a></li>
                </ul>

            </nav>

            <ul class="header__link">
                <li class="hide-md hide-lg hide-xl">
                    <a href="#" class="header__ad" title="Подать объявление"><i class="fas fa-pencil-alt"></i></a>
                </li>
                <li>
                    <a href="#" class="header__favorite" title="Избранное"><i class="far fa-heart"></i><small>1</small></a>
                </li>
                <li>
                    <a href="#auth" class="header__auth btn-modal" title="Вход/Регистрация"><i class="fas fa-sign-in-alt"></i><span>Вход/Регистрация</span></a>
                </li>
            </ul>

        </div>
    </div>

    <div class="header__bottom">
        <div class="container">
            <a href="#" class="header__logo">MIAN.RU</a>
            <ul class="header__nav">
                <li><a href="#"><span>Продажа</span></a></li>
                <li><a href="#"><span>Аренда</span></a></li>
                <li><a href="#"><span>Новостройки</span></a></li>
                <li><a href="#"><span>Спецпредложения</span></a></li>
                <li class="hidden-md"><a href="#"><span>Посуточно</span></a></li>
            </ul>
            <a class="header__post" href="#"><i class="fas fa-pencil-alt"></i><span>Подать объявление</span></a>

        </div>
    </div>

</header>