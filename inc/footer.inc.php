<footer class="footer">
    <nav class="footer__nav">
        <div class="container">
            <ul>
                <li><a href="#">О портале</a></li>
                <li><a href="#">Правила пользования</a></li>
                <li><a href="#">Продажа</a></li>
                <li><a href="#">Аренда</a></li>
                <li><a href="#">Спецпердложения</a></li>
                <li><a href="#">Карта сайта</a></li>
            </ul>
        </div>
    </nav>
    <div class="footer__content">
        <div class="container">
            <div class="footer__copy">© Недвижимость mian.ru, 2008-2015</div>
            <ul class="footer__link">
                <li><a href="#">Добавить объявление</a></li>
                <li><a href="#">Обратная связь</a></li>
            </ul>
            <div class="footer__counter">
                <a href="#">
                    <img src="img/count.gif" class="img-fluid" alt="">
                </a>
            </div>
        </div>
    </div>
</footer>