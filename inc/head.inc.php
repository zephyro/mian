<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title></title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="apple-touch-icon" href="icon.png">
<!-- Place favicon.ico in the root directory -->

<link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400,500,600,700&amp;subset=cyrillic" rel="stylesheet">
<link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">
<link  href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet"> <!-- 3 KB -->
<link rel="stylesheet" href="js/vendor/jquery.select2/css/select2.min.css">
<link rel="stylesheet" href="js/vendor/slick/_slick.scss">
<link rel="stylesheet" href="css/fontawesome-all.css">
<link rel="stylesheet" href="css/main.css">
