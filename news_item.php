<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<head>
    <?php include('inc/head.inc.php') ?>
</head>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header.inc.php') ?>
    <!-- -->

    <section class="main main-white">

        <div class="container">

            <ul class="breadcrumb">
                <li><a href="#">Недвижимость в Перми</a></li>
                <li><span>Новости</span></li>
            </ul>

            <div class="main-row">
                <div class="main-left">

                    <div class="content">
                        <h1 class="content__heading">Герман Греф анонсировал новые ипотечные рекорды</h1>
                        <div class="content__date">
                            <span>22 марта 2018</span>
                            <span><i class="fas fa-eye"></i> 181</span>
                        </div>

                        <div class="content__image">
                            <img src="images/news__01.jpg" class="img-fluid">
                        </div>

                        <p>Герман Греф, занимающий пост главы российского Сбербанка, выразил уверенность, что тенденция на дальнейшее снижение ипотечной ставки не только сохранится в этом году, но и побьет новые рекорды.</p>
                        <p>По его словам, Центробанк продолжит снижение ключевой ставки, а вместе с ней будет снижаться и ипотечная, цитирует «Интерфакс». Греф отметил значительную зависимость между размерами ставки и спросом россиян – собственно, именно это соотношение и позволило главе Сбербанка сделать такие прогнозы.</p>
                        <p>Также Греф напомнил, что в 2017 год власти сделали довольно много для развития ипотечной отрасли.</p>
                        <p>Говоря о результатах, продемонстрированных Сбербанком, Греф напомнил что особо весомый спрос на жилищные займы был зафиксирован в третьем квартале 2017 года. Ипотечный портфель банка при этом вырос до 3 трлн рублей, что позволяет сделать вывод: за тот год скорость прироста удвоилась.</p>
                        <p>Напомним, это уже не первый оптимистичный прогноз, озвученный Германом Грефом. Так, в начале зимы он анонсировал схожие тенденции.</p>
                    </div>

                    <div class="comment">

                        <div class="comment__login">
                            <a href="#" class="comment__login_button btn btn-sm">Комментировать</a>
                            <div class="comment__login_social">
                                <div class="social">
                                    <a class="social__vk" href="#"><i class="fab fa-vk"></i></a>
                                    <a class="social__twitter" href="#"><i class="fab fa-twitter"></i></a>
                                    <a class="social__odnoklassniki" href="#"><i class="fab fa-odnoklassniki"></i></a>
                                    <a class="social__google" href="#"><i class="fab fa-google"></i></a>
                                    <a class="social__facebook" href="#"><i class="fab fa-facebook-f"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="comment__list">

                            <div class="commentItem">
                                <div class="commentItem__author">
                                    <div class="commentItem__author_avatar">
                                        <div class="no-photo">
                                            <i class="far fa-user"></i>
                                        </div>
                                    </div>
                                    <div class="commentItem__author_wrap">
                                        <strong>Некто</strong>
                                        <span>вчера в 10:41</span>
                                    </div>
                                </div>
                                <div class="commentItem__text">Ставка по ипотеке будет падать до нуля и даже ниже, это объективный процесс.</div>
                                <a href="#" class="commentItem__answer">Ответить</a>
                            </div>

                            <div class="commentItem">
                                <div class="commentItem__author">
                                    <div class="commentItem__author_avatar">
                                        <div class="no-photo">
                                            <i class="far fa-user"></i>
                                        </div>
                                    </div>
                                    <div class="commentItem__author_wrap">
                                        <strong>Некто</strong>
                                        <span>вчера в 10:41</span>
                                    </div>
                                </div>
                                <div class="commentItem__text">Ставка по ипотеке будет падать до нуля и даже ниже, это объективный процесс.</div>
                                <a href="#" class="commentItem__answer">Ответить</a>
                            </div>

                            <div class="commentItem">
                                <div class="commentItem__author">
                                    <div class="commentItem__author_avatar">
                                        <div class="no-photo">
                                            <i class="far fa-user"></i>
                                        </div>
                                    </div>
                                    <div class="commentItem__author_wrap">
                                        <strong>Некто</strong>
                                        <span>вчера в 10:41</span>
                                    </div>
                                </div>
                                <div class="commentItem__text">Ставка по ипотеке будет падать до нуля и даже ниже, это объективный процесс.</div>
                                <a href="#" class="commentItem__answer">Ответить</a>
                            </div>

                        </div>

                        <div class="comment__form">
                            <div class="commentItem__author">
                                <div class="commentItem__author_avatar">
                                    <div class="no-photo">
                                        <i class="far fa-user"></i>
                                    </div>
                                </div>
                                <div class="commentItem__author_wrap">
                                    <strong>Некто</strong>
                                    <span>вчера в 10:41</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="message" rows="6" placeholder="Оставьте комментарий"></textarea>
                            </div>
                            <button type="submit" class="btn btn-sm">Опубликовать</button>
                        </div>

                    </div>

                </div>
                

                <div class="main-right">

                    <aside class="sidebar">
                        <div class="sidebar__title">Новое на сайте</div>

                        <div class="sidebar__elem">
                            <a href="#" class="sidebar__elem_title">Когда начинает течь минимальный срок владения ИЖдомом в случае его реконструкции?</a>
                            <div class="sidebar__elem_text">Дарение и продажа в одном периоде Итак, при получении квартиры в подарок и ее последующей продаже двойного налогообложения не происходит....</div>
                            <div class="sidebar__elem_time">сегодня в 1:24</div>
                        </div>

                        <div class="sidebar__elem">
                            <a href="#" class="sidebar__elem_title">Когда начинает течь минимальный срок владения ИЖдомом в случае его реконструкции?</a>
                            <div class="sidebar__elem_text">Дарение и продажа в одном периоде Итак, при получении квартиры в подарок и ее последующей продаже двойного налогообложения не происходит....</div>
                            <div class="sidebar__elem_time">сегодня в 1:24</div>
                        </div>

                        <div class="sidebar__elem">
                            <a href="#" class="sidebar__elem_title">Когда начинает течь минимальный срок владения ИЖдомом в случае его реконструкции?</a>
                            <div class="sidebar__elem_text">Дарение и продажа в одном периоде Итак, при получении квартиры в подарок и ее последующей продаже двойного налогообложения не происходит....</div>
                            <div class="sidebar__elem_time">сегодня в 1:24</div>
                        </div>

                        <div class="sidebar__elem">
                            <a href="#" class="sidebar__elem_title">Когда начинает течь минимальный срок владения ИЖдомом в случае его реконструкции?</a>
                            <div class="sidebar__elem_text">Дарение и продажа в одном периоде Итак, при получении квартиры в подарок и ее последующей продаже двойного налогообложения не происходит....</div>
                            <div class="sidebar__elem_time">сегодня в 1:24</div>
                        </div>

                    </aside>

                    <div class="sideSubscribe">
                        <div class="sideSubscribe__title">Подпишитесь на рассылку</div>
                        <div class="sideSubscribe__form">
                            <input type="text" class="sideSubscribe__input" name="email" placeholder="Ваш email">
                            <button type="submit" class="sideSubscribe__btn"><i class="far fa-envelope"></i></button>
                        </div>
                    </div>

                </div>
            </div>



        </div>

    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Modal -->
<?php include('inc/modal.inc.php') ?>
<!-- -->



<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->



</body>
</html>
