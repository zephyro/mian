<!doctype html>
<html class="no-js" lang="">

<!-- Head -->
<head>
    <?php include('inc/head.inc.php') ?>
</head>
<!-- -->

<body>

<div class="page">

    <!-- Header -->
    <?php include('inc/header.inc.php') ?>
    <!-- -->

    <section class="main">

        <div class="container">

            <ul class="breadcrumb">
                <li><a href="#">Недвижимость в Перми</a></li>
                <li><span>Личный кабинет</span></li>
            </ul>

            <h1>Личный кабинет</h1>

            <div class="account-wrap">

                <div class="account-row">
                    <div class="account-column">

                        <div class="account">
                            <div class="account__heading">
                                <div class="account__heading_icon">
                                    <i class="far fa-file-alt"></i>
                                </div>
                                <div class="account__heading_name">Мои объявления</div>
                            </div>
                            <div class="account__content">
                               <ul class="account__ad">
                                   <li>
                                       <div class="account__ad_title"><a href="#">№ 47534699</a>  от 20.04.2017</div>
                                       <div class="account__ad_text"><span class="account__ad_sale">Продажа</span> Волгоградская область, Волгоград, улица Дегтярева, 5</div>
                                       <div class="account__ad_action">
                                           <a href="#">изменить</a> | <a href="#">снять с публикации</a>
                                       </div>
                                   </li>
                                   <li>
                                       <div class="account__ad_title"><a href="#">№ 47534699</a>  от 20.04.2017</div>
                                       <div class="account__ad_text"><span class="account__ad_sale">Продажа</span> Волгоградская область, Волгоград, улица Дегтярева, 5</div>
                                       <div class="account__ad_action">
                                           <a href="#">изменить</a> | <a href="#">снять с публикации</a>
                                       </div>
                                   </li>
                                   <li>
                                       <div class="account__ad_title"><a href="#">№ 47534699</a>  от 20.04.2017</div>
                                       <div class="account__ad_text"><span class="account__ad_rent">Аренда</span>Волгоградская область, Волгоград, улица Дегтярева, 5</div>
                                       <div class="account__ad_action">
                                           <a href="#">изменить</a> | <a href="#">снять с публикации</a>
                                       </div>
                                   </li>
                                   <li>
                                       <div class="account__ad_title"><a href="#">№ 47534699</a>  от 20.04.2017</div>
                                       <div class="account__ad_text"><span class="account__ad_office">Офис</span>Волгоградская область, Волгоград, улица Дегтярева, 5</div>
                                       <div class="account__ad_action">
                                           <a href="#">изменить</a> | <a href="#">снять с публикации</a>
                                       </div>
                                   </li>
                               </ul>
                            </div>
                            <div class="account___footer">
                                <a href="#" class="btn btn-sm">Показать все</a>
                            </div>
                        </div>

                        <div class="account">
                            <div class="account__heading">
                                <div class="account__heading_avatar">
                                    <div class="no-photo">
                                        <i class="far fa-user"></i>
                                    </div>
                                </div>
                                <div class="account__heading_name">Личные данные</div>
                            </div>
                            <div class="account__content">
                                <div class="account__user">
                                    <div class="account__user_name">Вихров Денис</div>
                                    <div class="account__user_contact">zephyro@yandex.ru</div>
                                    <div class="account__user_contact">тел.: +7 927-****885</div>
                                    <div class="account__user_contact">доп. тел.: +7 927-****885</div>
                                </div>
                            </div>
                            <div class="account___footer">
                                <a href="#" class="btn btn-sm">Изменить</a>
                            </div>
                        </div>

                    </div>

                    <div class="account-column">

                        <div class="account">
                            <div class="account__heading">
                                <div class="account__heading_icon">
                                    <i class="fas fa-ruble-sign"></i>
                                </div>
                                <div class="account__heading_name">Баланс</div>
                            </div>
                            <div class="account__content">
                                <div class="account__balance">0.00 р.</div>
                                <a href="#" class="account__report">Отчёт по списаниям</a>
                                <div class="account__tariff">
                                    <span>Тарифный план:</span>
                                    <strong>Некоммерческий</strong>
                                </div>
                            </div>
                            <div class="account___footer">
                                <a href="#" class="btn btn-sm">Пополнить</a>
                            </div>
                        </div>

                        <div class="account">
                            <div class="account__heading">
                                <div class="account__heading_icon">
                                    <i class="fas fa-users"></i>
                                </div>
                                <div class="account__heading_name">Связанные соцсети</div>
                            </div>
                            <div class="account__content">
                                <div class="social">
                                    <a class="social__vk" href="#"><i class="fab fa-vk"></i></a>
                                    <a class="social__twitter" href="#"><i class="fab fa-twitter"></i></a>
                                    <a class="social__odnoklassniki" href="#"><i class="fab fa-odnoklassniki"></i></a>
                                    <a class="social__google" href="#"><i class="fab fa-google"></i></a>
                                    <a class="social__facebook" href="#"><i class="fab fa-facebook-f"></i></a>
                                </div>
                            </div>

                        </div>

                        <div class="account">
                            <div class="account__heading">
                                <div class="account__heading_icon">
                                    <i class="far fa-file-alt"></i>
                                </div>
                                <div class="account__heading_name">Мои объявления</div>
                            </div>
                            <div class="account__content">
                                <div class="account__center">
                                    <div class="account__center_icon">
                                        <i class="far fa-plus-square"></i>
                                    </div>
                                    <div class="account__center_text">У вас еще нет ни одного объявления.</div>
                                    <a href="#" class="btn btn-sm">Добавить объявление</a>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!-- Footer -->
    <?php include('inc/footer.inc.php') ?>
    <!-- -->

</div>

<!-- Modal -->
<?php include('inc/modal.inc.php') ?>
<!-- -->



<!-- Scripts -->
<?php include('inc/scripts.inc.php') ?>
<!-- -->



</body>
</html>
