<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->


            <div class="first first-home">
                <div class="first__wrap">
                    <div class="container">
                        <h1 class="first__heading">Портал недвижимости в Москве</h1>

                        <div class="first__content">
                            <form class="form">
                                <ul class="first__filter">

                                    <li>
                                        <div class="mySelect first__select">
                                            <div class="mySelect__btn">
                                                <span class="mySelect__selected">Продажа</span>
                                            </div>
                                            <div class="mySelect__dropdown">
                                                <div class="mySelect__wrap">
                                                    <ul>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__deal" value="Продажа" checked>
                                                                <span>Продажа</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__deal" value="Аренда">
                                                                <span>Аренда</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__deal" value="Посуточно">
                                                                <span>Посуточно</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="mySelect first__select">
                                            <div class="mySelect__btn">
                                                <span class="mySelect__selected">Квартира</span>
                                            </div>
                                            <div class="mySelect__dropdown">
                                                <div class="mySelect__wrap">
                                                    <ul>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Квартира" checked>
                                                                <span>Квартира</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Комната">
                                                                <span>Комната</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Дом">
                                                                <span>Дом</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Коттедж">
                                                                <span>Коттедж</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Участок">
                                                                <span>Участок</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Офис">
                                                                <span>Офис</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Торговая площадь">
                                                                <span>Торговая площадь</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__object" value="Склад">
                                                                <span>Склад</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="mySelect first__select">
                                            <div class="mySelect__btn" data-empty="Число комнат">
                                                <span class="mySelect__selected">Число комнат</span>
                                            </div>
                                            <div class="mySelect__dropdown">
                                                <div class="mySelect__wrap">
                                                    <ul>
                                                        <li>
                                                            <label class="mySelect__checkbox">
                                                                <input type="checkbox" name="filter__type" value="Студия">
                                                                <span>Студия</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__checkbox">
                                                                <input type="checkbox" name="filter__type" value="1к">
                                                                <span>1-комнатная</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__checkbox">
                                                                <input type="checkbox" name="filter__type" value="2к">
                                                                <span>2-комнатная</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__checkbox">
                                                                <input type="checkbox" name="filter__type" value="3к">
                                                                <span>3-комнатная</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__checkbox">
                                                                <input type="checkbox" name="filter__type" value="4к">
                                                                <span>4-комнатная</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="formRange">
                                            <div class="formRange__start">
                                                <input type="text" name="priceStart" placeholder="от">
                                            </div>
                                            <div class="formRange__end">
                                                <input type="text" name="priceEnd" placeholder="до">
                                                <span class="formRange__legend">₽</span>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="mySelect first__select">
                                            <div class="mySelect__btn">
                                                <span class="mySelect__selected">Город, район, метро, адрес</span>
                                            </div>
                                            <div class="mySelect__dropdown">
                                                <div class="mySelect__wrap">
                                                    <ul>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__place" value="Город, район, метро, адрес">
                                                                <span>Показать всё</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__place" value="Москва, м. Беляево">
                                                                <span>Москва, м. Беляево</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__place" value="Москва, Универстетский проспект">
                                                                <span>Москва, Универстетский проспект</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__place" value="Москва, Куркино">
                                                                <span>Москва, Куркино</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__place" value="Москва, Митино">
                                                                <span>Москва, Митино</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__place" value="Москва, м. Новослободская">
                                                                <span>Москва, м. Новослободская</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__place" value="Москва, м. Теплый Стан">
                                                                <span>Москва, м. Теплый Стан</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__place" value="Москва, ЮЗАО, Коньково">
                                                                <span>Москва, ЮЗАО, Коньково</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="mySelect__radio">
                                                                <input type="radio" name="filter__place" value="Москва, ЮЗАО, м. Прсопект Вернадского">
                                                                <span>Москва, ЮЗАО, м. Прсопект Вернадского</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                    </li>

                                </ul>
                                <ul class="first__send">
                                    <li>
                                        <button type="submit" class="btn btn-orange">Найти</button>
                                    </li>
                                    <li>
                                        <button type="submit" class="btn btn-opacity"><i class="fas fa-map-marker-alt"></i> Показать на карте</button>
                                    </li>
                                </ul>
                            </form>
                        </div>

                    </div>

                </div>
            </div>

            <section class="main">
                <div class="container">

                    <!-- Предложения -->
                    <div class="serviceBlock">
                        <div class="serviceBox">
                            <div class="serviceBox__image sale"></div>
                            <div class="serviceBox__content">
                                <h5>Купить квартиру</h5>
                                <ul>
                                    <li><a href="#">1-комнатные</a><span>10 445</span></li>
                                    <li><a href="#">2-комнатные</a><span>9 236</span></li>
                                    <li><a href="#">3-комнатные</a><span>6 038</span></li>
                                    <li><a href="#">Свободная планировка</a><span>11</span></li>
                                    <li><a href="#">Квартиры-студии</a><span>3 916</span></li>
                                    <li><a href="#">Комнаты в квартире</a><span>751</span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="serviceBox">
                            <div class="serviceBox__content">
                                <h5>Снять квартиру</h5>
                                <ul>
                                    <li><a href="#">1-комнатные</a><span>452</span></li>
                                    <li><a href="#">2-комнатные</a><span>262</span></li>
                                    <li><a href="#">3-комнатные</a><span>73</span></li>
                                    <li><a href="#">Квартиры-студии</a><span>11</span></li>
                                    <li><a href="#">Комнаты в квартире</a><span>751</span></li>
                                </ul>
                            </div>
                            <div class="serviceBox__image rent"></div>
                        </div>
                        <div class="serviceBox">
                            <div class="serviceBox__image hotel"></div>
                            <div class="serviceBox__content">
                                <h5>Снять посуточно</h5>
                                <ul>
                                    <li><a href="#">Квартиры посуточно</a><span>79</span></li>
                                    <li><a href="#">Коттеджи на сутки</a><span>8</span></li>
                                    <li><a href="#">3-комнатные</a><span>6 038</span></li>
                                    <li><a href="#">Свободная планировка</a><span>11</span></li>
                                    <li><a href="#">Квартиры-студии</a><span>3 916</span></li>
                                    <li><a href="#">Комнаты в квартире</a><span>751</span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="serviceBox small">
                            <div class="serviceBox__content">
                                <h5>Новостройки</h5>
                                <ul>
                                    <li><a href="#">Каталог ЖК</a><span>140</span></li>
                                    <li><a href="#">Сданные новостройки</a><span>86</span></li>
                                    <li><a href="#">Строящиеся новостройки</a><span>75</span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="serviceBox small">
                            <div class="serviceBox__content">
                                <h5>Загородная недвижимость</h5>
                                <ul>
                                    <li><a href="#">Купить дом</a><span>1627</span></li>
                                    <li><a href="#">Купить таунхаус</a><span>44</span></li>
                                    <li><a href="#">Купить участок</a><span>1220</span></li>
                                    <li><a href="#">Снять дом надолго</a><span>8</span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="serviceBox">
                            <div class="serviceBox__content">
                                <h5>Коммерческая недвижимость</h5>
                                <ul>
                                    <li><a href="#">Купить склад</a><span>452</span></li>
                                    <li><a href="#">Купить гараж</a><span>262</span></li>
                                    <li><a href="#">Купить офис</a><span>73</span></li>
                                    <li><a href="#">Арендовать офис</a><span>11</span></li>
                                    <li><a href="#">Арендовать торговую площадь</a><span>751</span></li>
                                </ul>
                            </div>
                            <div class="serviceBox__image office"></div>
                        </div>
                        <div class="serviceBox">
                            <div class="serviceBox__map">
                                <div class="serviceBox__map_wrap">
                                    <div class="serviceBox__map_title">Поиск на карте</div>
                                    <p>Ищите новостройки рядом с работой, парком или родственниками</p>
                                    <a href="#" class="btn btn-blue-border btn-sm btn-radius">Найти на карте</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- -->

                    <!-- Статьи и новости -->
                    <div class="magazine">
                        <div class="articles">
                            <a href="#" class="articles__subtitle">
                                <strong>Статьи</strong>
                                <i class="fas fa-angle-right"></i>
                            </a>
                            <a href="#" class="articles__first" style="background-image: url('images/acticle__image_01.jpg')">
                                <span class="articles__first_title">Топ-10 самых дорогих квартир на вторичном рынке Санкт-Петербурга</span>
                                <span class="articles__first_views"><i class="fas fa-eye"></i> 40</span>
                            </a>
                            <div class="articles__container">
                                <div class="articles__item">
                                    <div class="articles__text">
                                        <a href="#" class="articles__heading">Женские грезы: острова, замки, виллы и шале</a>
                                        <p>Лучшие подарки в квадратных метрах и гектарах со всего мира</p>
                                    </div>
                                    <div class="articles__image">
                                        <a href="#">
                                            <img src="images/acticle__image_02.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="articles__item">
                                    <div class="articles__text">
                                        <a href="#" class="articles__heading">Подарок мечты к 8 марта</a>
                                        <p>По данным опросов, россияне предпочитают дарить на Международный женский день цветы…</p>
                                    </div>
                                    <div class="articles__image">
                                        <a href="#">
                                            <img src="images/acticle__image_03.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="articles__item">
                                    <div class="articles__text">
                                        <a href="#" class="articles__heading">Ипотечная квартира: как получить налоговый вычет</a>
                                        <p>Как получить имущественный вычет и когда можно претендовать на деньги от налоговой…</p>
                                    </div>
                                    <div class="articles__image">
                                        <a href="#">
                                            <img src="images/acticle__image_04.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="news">
                            <a href="#" class="news__subtitle">
                                <strong>Новости</strong>
                                <i class="fas fa-angle-right"></i>
                            </a>
                            <div class="news__item">
                                <span class="news__date">11:40</span>
                                <a class="news__title" href="#">Жители Подмосковья протестовали против мусорных полигонов</a>
                            </div>
                            <div class="news__item">
                                <span class="news__date">20:43</span>
                                <a class="news__title" href="#">Самое дешевое жилье в СЗФО — в Вологодской области</a>
                            </div>
                            <div class="news__item">
                                <span class="news__date">19:25</span>
                                <a class="news__title" href="#">Власти Новосибирска рассказали о планах развития метрополитена</a>
                            </div>
                            <div class="news__item">
                                <span class="news__date">14:28</span>
                                <a class="news__title" href="#">В Казани появится новый большой концертный зал</a>
                            </div>
                            <div class="news__item">
                                <span class="news__date">14:10</span>
                                <a class="news__title" href="#">В Питере построят 2 новых моста через Неву</a>
                            </div>
                        </div>
                    </div>
                    <!-- -->

                    <ul class="premium">
                        <li>
                            <div class="premium__icon">
                                <i class="fas fa-space-shuttle"></i>
                            </div>
                            <h3>Ускоряйте сделки</h3>
                            <p>Премиум-размещение увеличивает количество звонков в 3 раза</p>
                            <div class="premium__bottom">
                                <a href="#" class="btn btn-orange btn-sm btn-radius">Купить премиум</a>
                            </div>
                        </li>
                        <li>
                            <div class="premium__icon">
                                <i class="far fa-edit"></i>
                            </div>
                            <h3>Размещайте объявления</h3>
                            <p>Новая форма подачи позволит быстро и удобно разместить объявление</p>
                            <div class="premium__bottom">
                                <a href="#" class="btn btn-sm btn-radius">Добавить объявления</a>
                            </div>
                        </li>
                        <li>
                            <div class="premium__icon">
                                <i class="far fa-map"></i>
                            </div>
                            <h3>Ищите на карте</h3>
                            <p>Ищите объявления рядом с работой, парком или родственниками</p>
                            <div class="premium__bottom">
                                <a href="#" class="btn btn-sm btn-radius">Найти на карте</a>
                            </div>
                        </li>
                    </ul>

                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->



        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->



    </body>
</html>
